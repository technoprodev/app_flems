<?php
namespace app_flems\models;

use Yii;

/**
 * This is the model class for table "karyawan".
 *
 * @property integer $id_user
 * @property string $id_pool
 * @property string $id_sub_unit_kerja
 * @property string $nik
 *
 * @property User $user
 * @property SubUnitKerja $subUnitKerja
 * @property UnitKerja $pool
 * @property Pesanan[] $pesanans
 * @property Pesanan[] $pesanans0
 * @property Pesanan[] $pesanans1
 * @property Pesanan[] $pesanans2
 * @property Pesanan[] $pesanans3
 * @property Pesanan[] $pesanans4
 * @property Pesanan[] $pesanans5
 * @property Pesanan[] $pesanans6
 * @property Pesanan[] $pesanans7
 * @property Pesanan[] $pesanans8
 * @property Pesanan[] $pesanans9
 * @property Pesanan[] $pesanans10
 * @property TransaksiAdmin[] $transaksiAdmins
 */
class Karyawan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'karyawan';
    }

    public function rules()
    {
        return [
            //id_user
            [['id_user'], 'required'],
            [['id_user'], 'integer'],
            [['id_user'], 'unique'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],

            //id_pool
            [['id_pool'], 'integer'],
            [['id_pool'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::className(), 'targetAttribute' => ['id_pool' => 'id']],

            //id_sub_unit_kerja
            [['id_sub_unit_kerja'], 'integer'],
            [['id_sub_unit_kerja'], 'exist', 'skipOnError' => true, 'targetClass' => SubUnitKerja::className(), 'targetAttribute' => ['id_sub_unit_kerja' => 'id']],

            //nik
            [['nik'], 'required'],
            [['nik'], 'string', 'max' => 16],
            [['nik'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_user' => 'User',
            'id_pool' => 'Pool',
            'id_sub_unit_kerja' => 'Sub Unit Kerja',
            'nik' => 'Nik',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubUnitKerja()
    {
        return $this->hasOne(SubUnitKerja::className(), ['id' => 'id_sub_unit_kerja']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPool()
    {
        return $this->hasOne(UnitKerja::className(), ['id' => 'id_pool']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans()
    {
        return $this->hasMany(Pesanan::className(), ['spk_dicetak_admin' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans0()
    {
        return $this->hasMany(Pesanan::className(), ['id_penumpang' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans1()
    {
        return $this->hasMany(Pesanan::className(), ['disetujui_manager' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans2()
    {
        return $this->hasMany(Pesanan::className(), ['ditolak_manager' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans3()
    {
        return $this->hasMany(Pesanan::className(), ['disetujui_dispatcher' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans4()
    {
        return $this->hasMany(Pesanan::className(), ['disetujui_supervisor' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans5()
    {
        return $this->hasMany(Pesanan::className(), ['ditolak_supervisor' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans6()
    {
        return $this->hasMany(Pesanan::className(), ['supir_diganti_supervisor' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans7()
    {
        return $this->hasMany(Pesanan::className(), ['spk_diselesaikan_admin' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans8()
    {
        return $this->hasMany(Pesanan::className(), ['realisasi_diminta_admin' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans9()
    {
        return $this->hasMany(Pesanan::className(), ['realisasi_disetujui_supervisor' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans10()
    {
        return $this->hasMany(Pesanan::className(), ['realisasi_ditolak_supervisor' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksiAdmins()
    {
        return $this->hasMany(TransaksiAdmin::className(), ['id_admin' => 'id_user']);
    }
}
