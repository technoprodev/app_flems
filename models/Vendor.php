<?php
namespace app_flems\models;

use Yii;

/**
 * This is the model class for table "vendor".
 *
 * @property string $id
 * @property string $vendor
 * @property string $alamat
 * @property string $bidang_bisnis
 * @property string $nomor_pks
 * @property string $waktu_mulai_pks
 * @property string $waktu_selesai_pks
 * @property string $nomor_telpon
 * @property string $periode_bayar
 * @property string $pic1_nama
 * @property string $pic1_telpon
 * @property string $pic2_nama
 * @property string $pic2_telpon
 * @property string $pic3_nama
 * @property string $pic3_telpon
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Mobil[] $mobils
 * @property Supir[] $supirs
 */
class Vendor extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'vendor';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id

            //vendor
            [['vendor'], 'required'],
            [['vendor'], 'string', 'max' => 256],

            //alamat
            [['alamat'], 'string', 'max' => 256],

            //bidang_bisnis
            [['bidang_bisnis'], 'string', 'max' => 256],

            //nomor_pks
            [['nomor_pks'], 'string', 'max' => 256],

            //waktu_mulai_pks
            [['waktu_mulai_pks'], 'safe'],

            //waktu_selesai_pks
            [['waktu_selesai_pks'], 'safe'],

            //nomor_telpon
            [['nomor_telpon'], 'string', 'max' => 16],

            //periode_bayar
            [['periode_bayar'], 'string', 'max' => 128],

            //pic1_nama
            [['pic1_nama'], 'string', 'max' => 256],

            //pic1_telpon
            [['pic1_telpon'], 'string', 'max' => 16],

            //pic2_nama
            [['pic2_nama'], 'string', 'max' => 256],

            //pic2_telpon
            [['pic2_telpon'], 'string', 'max' => 16],

            //pic3_nama
            [['pic3_nama'], 'string', 'max' => 256],

            //pic3_telpon
            [['pic3_telpon'], 'string', 'max' => 16],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendor' => 'Vendor',
            'alamat' => 'Alamat',
            'bidang_bisnis' => 'Bidang Bisnis',
            'nomor_pks' => 'Nomor Pks',
            'waktu_mulai_pks' => 'Waktu Mulai Pks',
            'waktu_selesai_pks' => 'Waktu Selesai Pks',
            'nomor_telpon' => 'Nomor Telpon',
            'periode_bayar' => 'Periode Bayar',
            'pic1_nama' => 'Pic1 Nama',
            'pic1_telpon' => 'Pic1 Telpon',
            'pic2_nama' => 'Pic2 Nama',
            'pic2_telpon' => 'Pic2 Telpon',
            'pic3_nama' => 'Pic3 Nama',
            'pic3_telpon' => 'Pic3 Telpon',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMobils()
    {
        return $this->hasMany(Mobil::className(), ['id_vendor' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupirs()
    {
        return $this->hasMany(Supir::className(), ['id_vendor' => 'id']);
    }
}
