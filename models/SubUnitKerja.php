<?php
namespace app_flems\models;

use Yii;

/**
 * This is the model class for table "sub_unit_kerja".
 *
 * @property string $id
 * @property string $id_unit_kerja
 * @property string $sub_unit_kerja
 * @property string $created_at
 * @property string $updated_at
 *
 * @property UnitKerja $unitKerja
 */
class SubUnitKerja extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'sub_unit_kerja';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id

            //id_unit_kerja
            [['id_unit_kerja'], 'required'],
            [['id_unit_kerja'], 'integer'],
            [['id_unit_kerja'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::className(), 'targetAttribute' => ['id_unit_kerja' => 'id']],

            //sub_unit_kerja
            [['sub_unit_kerja'], 'required'],
            [['sub_unit_kerja'], 'string', 'max' => 256],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_unit_kerja' => 'Unit Kerja',
            'sub_unit_kerja' => 'Sub Unit Kerja',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::className(), ['id' => 'id_unit_kerja']);
    }
}
