<?php
namespace app_flems\models;

use Yii;

/**
 * This is the model class for table "supir".
 *
 * @property integer $id_user
 * @property string $nik
 * @property string $tipe
 * @property string $id_mobil
 * @property string $id_pool
 * @property string $id_sub_unit_kerja
 * @property string $id_vendor
 * @property string $nomor_ktp
 * @property string $tipe_sim
 * @property string $nomor_sim
 * @property string $alamat
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $jenis_kelamin
 * @property string $pendidikan_terakhir
 * @property string $golongan_darah
 * @property string $status
 * @property string $agama
 * @property string $kewarganegaraan
 * @property string $pelatihan
 * @property string $nomor_kontrak
 * @property string $tanggal_kontrak
 * @property string $sanksi_keterangan
 * @property string $sanksi_sp1
 * @property string $sanksi_sp2
 * @property string $sanksi_sp3
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Pesanan[] $pesanans
 * @property User $user
 * @property Mobil $mobil
 * @property Vendor $vendor
 * @property UnitKerja $pool
 * @property SubUnitKerja $subUnitKerja
 */
class Supir extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'supir';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id_user
            [['id_user'], 'required'],
            [['id_user'], 'integer'],
            [['id_user'], 'unique'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],

            //nik
            [['nik'], 'required'],
            [['nik'], 'string', 'max' => 16],

            //tipe
            [['tipe'], 'required'],
            [['tipe'], 'string'],

            //id_mobil
            [['id_mobil'], 'integer'],
            [['id_mobil'], 'exist', 'skipOnError' => true, 'targetClass' => Mobil::className(), 'targetAttribute' => ['id_mobil' => 'id']],

            //id_pool
            [['id_pool'], 'integer'],
            [['id_pool'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::className(), 'targetAttribute' => ['id_pool' => 'id']],

            //id_sub_unit_kerja
            [['id_sub_unit_kerja'], 'integer'],
            [['id_sub_unit_kerja'], 'exist', 'skipOnError' => true, 'targetClass' => SubUnitKerja::className(), 'targetAttribute' => ['id_sub_unit_kerja' => 'id']],

            //id_vendor
            [['id_vendor'], 'required'],
            [['id_vendor'], 'integer'],
            [['id_vendor'], 'exist', 'skipOnError' => true, 'targetClass' => Vendor::className(), 'targetAttribute' => ['id_vendor' => 'id']],

            //nomor_ktp
            [['nomor_ktp'], 'string', 'max' => 64],

            //tipe_sim
            [['tipe_sim'], 'string'],

            //nomor_sim
            [['nomor_sim'], 'string', 'max' => 64],

            //alamat
            [['alamat'], 'string'],

            //tempat_lahir
            [['tempat_lahir'], 'string', 'max' => 128],

            //tanggal_lahir
            [['tanggal_lahir'], 'safe'],

            //jenis_kelamin
            [['jenis_kelamin'], 'string'],

            //pendidikan_terakhir
            [['pendidikan_terakhir'], 'string', 'max' => 256],

            //golongan_darah
            [['golongan_darah'], 'string'],

            //status
            [['status'], 'string'],

            //agama
            [['agama'], 'string'],

            //kewarganegaraan
            [['kewarganegaraan'], 'string', 'max' => 64],

            //pelatihan
            [['pelatihan'], 'string'],

            //nomor_kontrak
            [['nomor_kontrak'], 'string', 'max' => 32],

            //tanggal_kontrak
            [['tanggal_kontrak'], 'safe'],

            //sanksi_keterangan
            [['sanksi_keterangan'], 'string'],

            //sanksi_sp1
            [['sanksi_sp1'], 'string', 'max' => 64],

            //sanksi_sp2
            [['sanksi_sp2'], 'string', 'max' => 64],

            //sanksi_sp3
            [['sanksi_sp3'], 'string', 'max' => 64],

            //created_at
            // [['created_at'], 'required'],
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_user' => 'User',
            'nik' => 'Nik',
            'tipe' => 'Tipe',
            'id_mobil' => 'Mobil',
            'id_pool' => 'Pool',
            'id_sub_unit_kerja' => 'Sub Unit Kerja',
            'id_vendor' => 'Vendor',
            'nomor_ktp' => 'Nomor Ktp',
            'tipe_sim' => 'Tipe Sim',
            'nomor_sim' => 'Nomor Sim',
            'alamat' => 'Alamat',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'pendidikan_terakhir' => 'Pendidikan Terakhir',
            'golongan_darah' => 'Golongan Darah',
            'status' => 'Status',
            'agama' => 'Agama',
            'kewarganegaraan' => 'Kewarganegaraan',
            'pelatihan' => 'Pelatihan',
            'nomor_kontrak' => 'Nomor Kontrak',
            'tanggal_kontrak' => 'Tanggal Kontrak',
            'sanksi_keterangan' => 'Sanksi Keterangan',
            'sanksi_sp1' => 'Sanksi Sp1',
            'sanksi_sp2' => 'Sanksi Sp2',
            'sanksi_sp3' => 'Sanksi Sp3',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans()
    {
        return $this->hasMany(Pesanan::className(), ['id_supir' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMobil()
    {
        return $this->hasOne(Mobil::className(), ['id' => 'id_mobil']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(Vendor::className(), ['id' => 'id_vendor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPool()
    {
        return $this->hasOne(UnitKerja::className(), ['id' => 'id_pool']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubUnitKerja()
    {
        return $this->hasOne(SubUnitKerja::className(), ['id' => 'id_sub_unit_kerja']);
    }
}
