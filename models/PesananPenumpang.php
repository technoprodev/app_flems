<?php
namespace app_flems\models;

use Yii;

/**
 * This is the model class for table "pesanan_penumpang".
 *
 * @property integer $id
 * @property string $id_pesanan
 * @property integer $id_penumpang
 *
 * @property Pesanan $pesanan
 * @property User $penumpang
 */
class PesananPenumpang extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;

    public static function tableName()
    {
        return 'pesanan_penumpang';
    }

    public function rules()
    {
        return [
            //id

            //id_pesanan
            [['id_pesanan'], 'required'],
            [['id_pesanan'], 'integer'],
            [['id_pesanan'], 'exist', 'skipOnError' => true, 'targetClass' => Pesanan::className(), 'targetAttribute' => ['id_pesanan' => 'id']],

            //id_penumpang
            [['id_penumpang'], 'required'],
            [['id_penumpang'], 'integer'],
            [['id_penumpang'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_penumpang' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pesanan' => 'Pesanan',
            'id_penumpang' => 'Penumpang',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanan()
    {
        return $this->hasOne(Pesanan::className(), ['id' => 'id_pesanan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenumpang()
    {
        return $this->hasOne(User::className(), ['id' => 'id_penumpang']);
    }
}
