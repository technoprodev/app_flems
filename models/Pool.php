<?php
namespace app_flems\models;

use Yii;

/**
 * This is the model class for table "pool".
 *
 * @property integer $id
 * @property string $pool
 *
 * @property Driver[] $drivers
 * @property UnitKerja[] $unitKerjas
 */
class Pool extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'pool';
    }

    public function rules()
    {
        return [
            //id

            //pool
            [['pool'], 'required'],
            [['pool'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pool' => 'Pool',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Driver::className(), ['id_pool' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerjas()
    {
        return $this->hasMany(UnitKerja::className(), ['id_pool' => 'id']);
    }
}
