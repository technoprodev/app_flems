<?php
namespace app_flems\models;

use Yii;

/**
 * This is the model class for table "mobil".
 *
 * @property string $id
 * @property string $nomor_polisi
 * @property string $id_vendor
 * @property string $dedicate
 * @property string $merk
 * @property string $tipe
 * @property string $warna
 * @property string $tahun
 * @property string $tipe_bbm
 * @property string $odometer_unit_comes
 * @property string $periode_sewa
 * @property string $tanggal_mulai_sewa
 * @property string $tanggal_akhir_sewa
 * @property integer $harga_per_bulan
 * @property string $periode_service
 * @property string $tanggal_pajak
 * @property string $masa_stnk
 * @property string $nama_pemilik
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Vendor $vendor
 * @property Supir[] $supirs
 */
class Mobil extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'mobil';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id

            //nomor_polisi
            [['nomor_polisi'], 'required'],
            [['nomor_polisi'], 'string', 'max' => 16],

            //id_vendor
            [['id_vendor'], 'required'],
            [['id_vendor'], 'integer'],
            [['id_vendor'], 'exist', 'skipOnError' => true, 'targetClass' => Vendor::className(), 'targetAttribute' => ['id_vendor' => 'id']],

            //dedicate
            [['dedicate'], 'string'],

            //merk
            [['merk'], 'string', 'max' => 256],

            //tipe
            [['tipe'], 'string', 'max' => 256],

            //warna
            [['warna'], 'string', 'max' => 256],

            //tahun
            [['tahun'], 'safe'],

            //tipe_bbm
            [['tipe_bbm'], 'string', 'max' => 256],

            //odometer_unit_comes
            [['odometer_unit_comes'], 'string', 'max' => 256],

            //periode_sewa
            [['periode_sewa'], 'string'],

            //tanggal_mulai_sewa
            [['tanggal_mulai_sewa'], 'safe'],

            //tanggal_akhir_sewa
            [['tanggal_akhir_sewa'], 'safe'],

            //harga_per_bulan
            [['harga_per_bulan'], 'integer'],

            //periode_service
            [['periode_service'], 'string'],

            //tanggal_pajak
            [['tanggal_pajak'], 'safe'],

            //masa_stnk
            [['masa_stnk'], 'safe'],

            //nama_pemilik
            [['nama_pemilik'], 'string', 'max' => 256],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomor_polisi' => 'Nomor Polisi',
            'id_vendor' => 'Id Vendor',
            'dedicate' => 'Dedicate',
            'merk' => 'Merk',
            'tipe' => 'Tipe',
            'warna' => 'Warna',
            'tahun' => 'Tahun',
            'tipe_bbm' => 'Tipe Bbm',
            'odometer_unit_comes' => 'Odometer Unit Comes',
            'periode_sewa' => 'Periode Sewa',
            'tanggal_mulai_sewa' => 'Tanggal Mulai Sewa',
            'tanggal_akhir_sewa' => 'Tanggal Akhir Sewa',
            'harga_per_bulan' => 'Harga Per Bulan',
            'periode_service' => 'Periode Service',
            'tanggal_pajak' => 'Tanggal Pajak',
            'masa_stnk' => 'Masa Stnk',
            'nama_pemilik' => 'Nama Pemilik',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(Vendor::className(), ['id' => 'id_vendor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupirs()
    {
        return $this->hasMany(Supir::className(), ['id_mobil' => 'id']);
    }
}
