<?php
namespace app_flems\models;

use Yii;

/**
 * This is the model class for table "transaksi_supir".
 *
 * @property integer $id
 * @property integer $id_supir
 * @property integer $id_admin
 * @property string $waktu
 * @property double $debit
 * @property double $kredit
 * @property double $saldo
 * @property string $keterangan
 * @property string $id_pesanan
 *
 * @property Supir $supir
 * @property Karyawan $admin
 * @property Pesanan $pesanan
 */
class TransaksiSupir extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'transaksi_supir';
    }

    public function rules()
    {
        return [
            //id

            //id_supir
            [['id_supir'], 'required'],
            [['id_supir'], 'integer'],
            [['id_supir'], 'exist', 'skipOnError' => true, 'targetClass' => Supir::className(), 'targetAttribute' => ['id_supir' => 'id_user']],

            //id_admin
            [['id_admin'], 'required'],
            [['id_admin'], 'integer'],
            [['id_admin'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['id_admin' => 'id_user']],

            //waktu
            [['waktu'], 'safe'],

            //debit
            [['debit'], 'number'],

            //kredit
            [['kredit'], 'number'],

            //saldo
            [['saldo'], 'required'],
            [['saldo'], 'number'],

            //keterangan
            [['keterangan'], 'required'],
            [['keterangan'], 'string'],

            //id_pesanan
            [['id_pesanan'], 'integer'],
            [['id_pesanan'], 'exist', 'skipOnError' => true, 'targetClass' => Pesanan::className(), 'targetAttribute' => ['id_pesanan' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_supir' => 'Id Supir',
            'id_admin' => 'Id Admin',
            'waktu' => 'Waktu',
            'debit' => 'Debit',
            'kredit' => 'Kredit',
            'saldo' => 'Saldo',
            'keterangan' => 'Keterangan',
            'id_pesanan' => 'Id Pesanan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupir()
    {
        return $this->hasOne(Supir::className(), ['id_user' => 'id_supir']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'id_admin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanan()
    {
        return $this->hasOne(Pesanan::className(), ['id' => 'id_pesanan']);
    }
}
