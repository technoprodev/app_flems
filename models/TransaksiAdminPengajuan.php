<?php
namespace app_flems\models;

use Yii;

/**
 * This is the model class for table "transaksi_admin_pengajuan".
 *
 * @property integer $id
 * @property string $status_pengajuan
 * @property string $id_pool
 * @property integer $supervisor_yang_menyetujui
 * @property integer $supervisor_yang_menolak
 * @property integer $admin_yang_mengajukan
 * @property double $total_yang_diajukan
 * @property string $waktu_pengajuan
 * @property string $tipe
 *
 * @property UnitKerja $pool
 * @property Karyawan $supervisorYangMenyetujui
 * @property Karyawan $supervisorYangMenolak
 * @property Karyawan $adminYangMengajukan
 */
class TransaksiAdminPengajuan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'transaksi_admin_pengajuan';
    }

    public function rules()
    {
        return [
            //id

            //status_pengajuan
            [['status_pengajuan'], 'required'],
            [['status_pengajuan'], 'string'],

            //id_pool
            [['id_pool'], 'required'],
            [['id_pool'], 'integer'],
            [['id_pool'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::className(), 'targetAttribute' => ['id_pool' => 'id']],

            //supervisor_yang_menyetujui
            [['supervisor_yang_menyetujui'], 'integer'],
            [['supervisor_yang_menyetujui'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['supervisor_yang_menyetujui' => 'id_user']],

            //supervisor_yang_menolak
            [['supervisor_yang_menolak'], 'integer'],
            [['supervisor_yang_menolak'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['supervisor_yang_menolak' => 'id_user']],

            //admin_yang_mengajukan
            [['admin_yang_mengajukan'], 'required'],
            [['admin_yang_mengajukan'], 'integer'],
            [['admin_yang_mengajukan'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['admin_yang_mengajukan' => 'id_user']],

            //total_yang_diajukan
            [['total_yang_diajukan'], 'required'],
            [['total_yang_diajukan'], 'number'],

            //waktu_pengajuan
            [['waktu_pengajuan'], 'safe'],

            //tipe
            [['tipe'], 'required'],
            [['tipe'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_pengajuan' => 'Status Pengajuan',
            'id_pool' => 'Id Pool',
            'supervisor_yang_menyetujui' => 'Supervisor Yang Menyetujui',
            'supervisor_yang_menolak' => 'Supervisor Yang Menolak',
            'admin_yang_mengajukan' => 'Admin Yang Mengajukan',
            'total_yang_diajukan' => 'Total Yang Diajukan',
            'waktu_pengajuan' => 'Waktu Pengajuan',
            'tipe' => 'Tipe',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPool()
    {
        return $this->hasOne(UnitKerja::className(), ['id' => 'id_pool']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupervisorYangMenyetujui()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'supervisor_yang_menyetujui']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupervisorYangMenolak()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'supervisor_yang_menolak']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminYangMengajukan()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'admin_yang_mengajukan']);
    }
}
