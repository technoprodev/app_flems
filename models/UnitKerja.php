<?php
namespace app_flems\models;

use Yii;

/**
 * This is the model class for table "unit_kerja".
 *
 * @property string $id
 * @property string $unit_kerja
 * @property string $saldo
 * @property double $keberangkatan_latitude
 * @property double $keberangkatan_longitude
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Karyawan[] $karyawans
 * @property Pesanan[] $pesanans
 * @property SubUnitKerja[] $subUnitKerjas
 * @property Supir[] $supirs
 */
class UnitKerja extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'unit_kerja';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id

            //unit_kerja
            [['unit_kerja'], 'required'],
            [['unit_kerja'], 'string', 'max' => 256],

            //saldo
            [['saldo'], 'number'],

            //keberangkatan_latitude
            [['keberangkatan_latitude'], 'required'],
            [['keberangkatan_latitude'], 'number'],

            //keberangkatan_longitude
            [['keberangkatan_longitude'], 'required'],
            [['keberangkatan_longitude'], 'number'],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unit_kerja' => 'Unit Kerja',
            'saldo' => 'Saldo',
            'keberangkatan_latitude' => 'Keberangkatan Latitude',
            'keberangkatan_longitude' => 'Keberangkatan Longitude',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKaryawans()
    {
        return $this->hasMany(Karyawan::className(), ['id_unit_kerja' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans()
    {
        return $this->hasMany(Pesanan::className(), ['id_unit_kerja' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubUnitKerjas()
    {
        return $this->hasMany(SubUnitKerja::className(), ['id_unit_kerja' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupirs()
    {
        return $this->hasMany(Supir::className(), ['id_unit_kerja' => 'id']);
    }
}
