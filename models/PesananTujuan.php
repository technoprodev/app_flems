<?php
namespace app_flems\models;

use Yii;

/**
 * This is the model class for table "pesanan_tujuan".
 *
 * @property integer $id
 * @property string $id_pesanan
 * @property string $tujuan
 * @property double $tujuan_latitude
 * @property double $tujuan_longitude
 *
 * @property Pesanan $pesanan
 */
class PesananTujuan extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;

    public static function tableName()
    {
        return 'pesanan_tujuan';
    }

    public function rules()
    {
        return [
            //id

            //id_pesanan
            [['id_pesanan'], 'required'],
            [['id_pesanan'], 'integer'],
            [['id_pesanan'], 'exist', 'skipOnError' => true, 'targetClass' => Pesanan::className(), 'targetAttribute' => ['id_pesanan' => 'id']],

            //tujuan
            [['tujuan'], 'required'],
            [['tujuan'], 'string'],

            //tujuan_latitude
            [['tujuan_latitude'], 'required'],
            [['tujuan_latitude'], 'number'],

            //tujuan_longitude
            [['tujuan_longitude'], 'required'],
            [['tujuan_longitude'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pesanan' => 'Pesanan',
            'tujuan' => 'Tujuan',
            'tujuan_latitude' => 'Tujuan Latitude',
            'tujuan_longitude' => 'Tujuan Longitude',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanan()
    {
        return $this->hasOne(Pesanan::className(), ['id' => 'id_pesanan']);
    }
}
