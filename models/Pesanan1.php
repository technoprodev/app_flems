<?php
namespace app_flems\models;

use Yii;

/**
 * This is the model class for table "pesanan".
 *
 * @property string $id
 * @property string $status
 * @property string $status_realisasi
 * @property integer $disetujui_manager
 * @property integer $ditolak_manager
 * @property integer $disetujui_dispatcher
 * @property integer $disetujui_supervisor
 * @property integer $ditolak_supervisor
 * @property integer $supir_diganti_supervisor
 * @property integer $spk_dicetak_admin
 * @property integer $spk_diselesaikan_admin
 * @property integer $realisasi_diminta_admin
 * @property integer $realisasi_disetujui_supervisor
 * @property integer $realisasi_ditolak_supervisor
 * @property integer $supir_konfirmasi
 * @property string $waktu_checkin
 * @property string $waktu_checkout
 * @property string $waktu_spk_selesai
 * @property string $tipe
 * @property string $tipe_penumpang
 * @property integer $id_penumpang
 * @property string $keperluan
 * @property integer $id_supir
 * @property string $id_sub_unit_kerja
 * @property string $waktu_keberangkatan
 * @property string $waktu_kembali
 * @property string $keberangkatan_dari
 * @property string $keberangkatan
 * @property double $keberangkatan_latitude
 * @property double $keberangkatan_longitude
 * @property string $tujuan
 * @property double $tujuan_latitude
 * @property double $tujuan_longitude
 * @property string $created_at
 * @property string $updated_at
 * @property string $estimasi_jarak
 * @property string $estimasi_biaya
 * @property string $anggaran
 * @property string $bbm
 * @property string $tol
 * @property string $parkir
 * @property string $revisi_bbm
 * @property string $revisi_tol
 * @property string $revisi_parkir
 * @property integer $spj
 * @property integer $otp_checkin
 * @property integer $otp_checkout
 *
 * @property UnitKerja $unitKerja
 * @property Karyawan $spkDicetakAdmin
 * @property Karyawan $penumpang
 * @property Karyawan $disetujuiManager
 * @property Karyawan $ditolakManager
 * @property Karyawan $disetujuiDispatcher
 * @property Karyawan $disetujuiSupervisor
 * @property Karyawan $ditolakSupervisor
 * @property Karyawan $supirDigantiSupervisor
 * @property Supir $supir
 * @property Karyawan $spkDiselesaikanAdmin
 * @property Karyawan $realisasiDimintaAdmin
 * @property Karyawan $realisasiDisetujuiSupervisor
 * @property Karyawan $realisasiDitolakSupervisor
 * @property PesananBarang[] $pesananBarangs
 * @property PesananPenumpang[] $pesananPenumpangs
 * @property PesananTujuan[] $pesananTujuans
 */
class Pesanan extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_otp_checkin;
    public $virtual_otp_checkout;
    
    public static function tableName()
    {
        return 'pesanan';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id

            //status
            [['status'], 'required'],
            [['status'], 'string'],

            //status_realisasi
            [['status_realisasi'], 'string'],

            //disetujui_manager
            [['disetujui_manager'], 'integer'],
            [['disetujui_manager'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['disetujui_manager' => 'id_user']],

            //ditolak_manager
            [['ditolak_manager'], 'integer'],
            [['ditolak_manager'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['ditolak_manager' => 'id_user']],

            //disetujui_dispatcher
            [['disetujui_dispatcher'], 'integer'],
            [['disetujui_dispatcher'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['disetujui_dispatcher' => 'id_user']],

            //disetujui_supervisor
            [['disetujui_supervisor'], 'integer'],
            [['disetujui_supervisor'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['disetujui_supervisor' => 'id_user']],

            //ditolak_supervisor
            [['ditolak_supervisor'], 'integer'],
            [['ditolak_supervisor'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['ditolak_supervisor' => 'id_user']],

            //supir_diganti_supervisor
            [['supir_diganti_supervisor'], 'integer'],
            [['supir_diganti_supervisor'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['supir_diganti_supervisor' => 'id_user']],

            //spk_dicetak_admin
            [['spk_dicetak_admin'], 'integer'],
            [['spk_dicetak_admin'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['spk_dicetak_admin' => 'id_user']],

            //spk_diselesaikan_admin
            [['spk_diselesaikan_admin'], 'integer'],
            [['spk_diselesaikan_admin'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['spk_diselesaikan_admin' => 'id_user']],

            //realisasi_diminta_admin
            [['realisasi_diminta_admin'], 'integer'],
            [['realisasi_diminta_admin'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['realisasi_diminta_admin' => 'id_user']],

            //realisasi_disetujui_supervisor
            [['realisasi_disetujui_supervisor'], 'integer'],
            [['realisasi_disetujui_supervisor'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['realisasi_disetujui_supervisor' => 'id_user']],

            //realisasi_ditolak_supervisor
            [['realisasi_ditolak_supervisor'], 'integer'],
            [['realisasi_ditolak_supervisor'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['realisasi_ditolak_supervisor' => 'id_user']],

            //supir_konfirmasi
            [['supir_konfirmasi'], 'integer'],

            //waktu_checkin
            [['waktu_checkin'], 'safe'],

            //waktu_checkout
            [['waktu_checkout'], 'safe'],

            //waktu_spk_selesai
            [['waktu_spk_selesai'], 'safe'],

            //tipe
            [['tipe'], 'required'],
            [['tipe'], 'string'],

            //tipe_penumpang
            [['tipe_penumpang'], 'required'],
            [['tipe_penumpang'], 'string'],

            //id_penumpang
            [['id_penumpang'], 'required'],
            [['id_penumpang'], 'integer'],
            [['id_penumpang'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['id_penumpang' => 'id_user']],

            //keperluan
            [['keperluan'], 'string', 'max' => 256],

            //id_supir
            [['id_supir'], 'integer'],
            [['id_supir'], 'exist', 'skipOnError' => true, 'targetClass' => Supir::className(), 'targetAttribute' => ['id_supir' => 'id_user']],

            //id_sub_unit_kerja
            [['id_sub_unit_kerja'], 'required'],
            [['id_sub_unit_kerja'], 'integer'],
            [['id_sub_unit_kerja'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::className(), 'targetAttribute' => ['id_sub_unit_kerja' => 'id']],

            //waktu_keberangkatan
            [['waktu_keberangkatan'], 'required'],
            [['waktu_keberangkatan'], 'safe'],

            //waktu_kembali
            [['waktu_kembali'], 'required'],
            [['waktu_kembali'], 'safe'],

            //keberangkatan_dari
            [['keberangkatan_dari'], 'required'],
            [['keberangkatan_dari'], 'string'],

            //keberangkatan
            [['keberangkatan'], 'required'],
            [['keberangkatan'], 'string', 'max' => 256],

            //keberangkatan_latitude
            [['keberangkatan_latitude'], 'required'],
            [['keberangkatan_latitude'], 'number'],

            //keberangkatan_longitude
            [['keberangkatan_longitude'], 'required'],
            [['keberangkatan_longitude'], 'number'],

            //tujuan
            [['tujuan'], 'required'],
            [['tujuan'], 'string', 'max' => 256],

            //tujuan_latitude
            [['tujuan_latitude'], 'required'],
            [['tujuan_latitude'], 'number'],

            //tujuan_longitude
            [['tujuan_longitude'], 'required'],
            [['tujuan_longitude'], 'number'],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],

            //estimasi_jarak
            [['estimasi_jarak'], 'number'],

            //estimasi_biaya
            [['estimasi_biaya'], 'number'],

            //anggaran
            [['anggaran'], 'number'],

            //bbm
            [['bbm'], 'number'],

            //tol
            [['tol'], 'number'],

            //parkir
            [['parkir'], 'number'],

            //revisi_bbm
            [['revisi_bbm'], 'number'],

            //revisi_tol
            [['revisi_tol'], 'number'],

            //revisi_parkir
            [['revisi_parkir'], 'number'],

            //spj
            [['spj'], 'integer'],

            //otp_checkin
            [['otp_checkin'], 'integer'],

            //otp_checkout
            [['otp_checkout'], 'integer'],

            //virtual_otp_checkin
            [['virtual_otp_checkin'], 'integer'],
            [['virtual_otp_checkin'], 'compare', 'compareAttribute'=>'otp_checkin', 'message'=>'OTP Checkin is wrong', 'enableClientValidation' => false],

            //virtual_otp_checkout
            [['virtual_otp_checkout'], 'integer'],
            [['virtual_otp_checkout'], 'compare', 'compareAttribute'=>'otp_checkout', 'message'=>'OTP Checkout is wrong', 'enableClientValidation' => false],

            [['anggaran'], 'required', 'on' => 'dispatcher'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'status_realisasi' => 'Status Realisasi',
            'disetujui_manager' => 'Disetujui Manager',
            'ditolak_manager' => 'Ditolak Manager',
            'disetujui_dispatcher' => 'Dialokasikan Dispatcher',
            'disetujui_supervisor' => 'Disetujui Supervisor',
            'ditolak_supervisor' => 'Ditolak Supervisor',
            'supir_diganti_supervisor' => 'Supir Diganti Supervisor',
            'spk_dicetak_admin' => 'Spk Dicetak Admin',
            'spk_diselesaikan_admin' => 'Spk Diselesaikan Admin',
            'realisasi_diminta_admin' => 'Realisasi Diminta Admin',
            'realisasi_disetujui_supervisor' => 'Realisasi Disetujui Supervisor',
            'realisasi_ditolak_supervisor' => 'Realisasi Ditolak Supervisor',
            'supir_konfirmasi' => 'Supir Konfirmasi',
            'waktu_checkin' => 'Waktu Checkin',
            'waktu_checkout' => 'Waktu Checkout',
            'waktu_spk_selesai' => 'Waktu Spk Selesai',
            'tipe' => 'Tipe',
            'tipe_penumpang' => 'Tipe Perjalanan',
            'id_penumpang' => 'Penumpang',
            'keperluan' => 'Keperluan',
            'id_supir' => 'Supir',
            'id_sub_unit_kerja' => 'Unit Kerja',
            'waktu_keberangkatan' => 'Waktu Keberangkatan',
            'waktu_kembali' => 'Waktu Kembali',
            'keberangkatan_dari' => 'Keberangkatan Dari',
            'keberangkatan' => 'Keberangkatan',
            'keberangkatan_latitude' => 'Keberangkatan Latitude',
            'keberangkatan_longitude' => 'Keberangkatan Longitude',
            'tujuan' => 'Tujuan',
            'tujuan_latitude' => 'Tujuan Latitude',
            'tujuan_longitude' => 'Tujuan Longitude',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'estimasi_jarak' => 'Estimasi Jarak',
            'estimasi_biaya' => 'Estimasi Biaya',
            'anggaran' => 'Anggaran',
            'bbm' => 'Bbm',
            'tol' => 'Tol',
            'parkir' => 'Parkir',
            'revisi_bbm' => 'Bbm',
            'revisi_tol' => 'Tol',
            'revisi_parkir' => 'Parkir',
            'spj' => 'Spj',
            'otp_checkin' => 'Otp Checkin',
            'otp_checkout' => 'Otp Checkout',
            'virtual_otp_checkin' => 'Otp Checkin',
            'virtual_otp_checkout' => 'Otp Checkout',
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['otp_checkout']);
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubUnitKerja()
    {
        return $this->hasOne(SubUnitKerja::className(), ['id' => 'id_sub_unit_kerja']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpkDicetakAdmin()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'spk_dicetak_admin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenumpang()
    {
        return $this->hasOne(User::className(), ['id' => 'id_penumpang'])->with('karyawan');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisetujuiManager()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'disetujui_manager']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDitolakManager()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'ditolak_manager']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisetujuiDispatcher()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'disetujui_dispatcher']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisetujuiSupervisor()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'disetujui_supervisor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDitolakSupervisor()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'ditolak_supervisor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupirDigantiSupervisor()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'supir_diganti_supervisor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupir()
    {
        return $this->hasOne(User::className(), ['id' => 'id_supir'])->select(['user.*', 'supir.*'])
            ->leftJoin('supir', '`supir`.`id_user` = `user`.`id`');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpkDiselesaikanAdmin()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'spk_diselesaikan_admin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealisasiDimintaAdmin()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'realisasi_diminta_admin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealisasiDisetujuiSupervisor()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'realisasi_disetujui_supervisor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealisasiDitolakSupervisor()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'realisasi_ditolak_supervisor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesananBarangs()
    {
        return $this->hasMany(PesananBarang::className(), ['id_pesanan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesananPenumpangs()
    {
        return $this->hasMany(User::className(), ['id' => 'id_penumpang'])->viaTable('pesanan_penumpang', ['id_pesanan' => 'id'])->with('karyawan');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesananTujuans()
    {
        return $this->hasMany(PesananTujuan::className(), ['id_pesanan' => 'id']);
    }
}
