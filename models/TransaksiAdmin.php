<?php
namespace app_flems\models;

use Yii;

/**
 * This is the model class for table "transaksi_admin".
 *
 * @property integer $id
 * @property integer $id_admin
 * @property string $tipe
 * @property string $waktu
 * @property double $debit
 * @property double $kredit
 * @property double $saldo
 * @property string $keterangan
 * @property integer $disetujui_supervisor
 * @property string $id_pesanan
 *
 * @property Karyawan $admin
 * @property User $disetujuiSupervisor
 * @property Pesanan $pesanan
 */
class TransaksiAdmin extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'transaksi_admin';
    }

    public function rules()
    {
        return [
            //id

            //id_admin
            [['id_admin'], 'required'],
            [['id_admin'], 'integer'],
            [['id_admin'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['id_admin' => 'id_user']],

            //tipe
            [['tipe'], 'required'],
            [['tipe'], 'string'],

            //waktu
            // [['waktu'], 'required'],
            [['waktu'], 'safe'],

            //debit
            [['debit'], 'number'],

            //kredit
            [['kredit'], 'number'],

            //saldo
            [['saldo'], 'required'],
            [['saldo'], 'number'],

            //keterangan
            [['keterangan'], 'required'],
            [['keterangan'], 'string'],

            //disetujui_supervisor
            [['disetujui_supervisor'], 'integer'],
            [['disetujui_supervisor'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['disetujui_supervisor' => 'id']],

            //id_pesanan
            [['id_pesanan'], 'integer'],
            [['id_pesanan'], 'exist', 'skipOnError' => true, 'targetClass' => Pesanan::className(), 'targetAttribute' => ['id_pesanan' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_admin' => 'Id Admin',
            'tipe' => 'Tipe',
            'waktu' => 'Waktu',
            'debit' => 'Debit',
            'kredit' => 'Kredit',
            'saldo' => 'Saldo',
            'keterangan' => 'Keterangan',
            'disetujui_supervisor' => 'Disetujui Supervisor',
            'id_pesanan' => 'Id Pesanan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'id_admin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisetujuiSupervisor()
    {
        return $this->hasOne(User::className(), ['id' => 'disetujui_supervisor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesanan()
    {
        return $this->hasOne(Pesanan::className(), ['id' => 'id_pesanan']);
    }
}
