<?php
namespace app_flems\models;

use Yii;

/**
 * This is connector for Yii::$app->user with model User
 */
class User extends \technosmart\models\User
{
    public function fields()
    {
        $fields = parent::fields();
        unset($fields['password_hash']);
        return $fields;
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            //username
            [['username'], 'required'],
            
            //email
            [['email'], 'required'],
        ]);
    }

    /*public static function findByLogin($login)
    {
        return static::find()
            ->join('INNER JOIN', 'user_extend ue', 'ue.id = user.id')
            ->where('username = :login or email = :login', [':login' => $login])
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->one();
    }*/

    public function getKaryawan()
    {
        return $this->hasOne(Karyawan::className(), ['id_user' => 'id']);
    }

    public function getSupir()
    {
        return $this->hasOne(Supir::className(), ['id_user' => 'id']);
    }
}