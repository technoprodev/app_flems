<?php
$params = [
    'app.name' => 'Arkeo',
    'app.description' => 'Armada Kendaraan Operasional',
    'app.keywords' => 'arkeo, fandy, pradana',
    'app.owner' => 'Technopro',
    'app.author' => 'Technopro',
    'enable.permission-checking' => true,
    'enable.permission-init' => false,
    'user.passwordResetTokenExpire' => 3600,
    'email.noreply' => 'arkeo@gmail.com',
    'emailName.noreply' => 'Arkeo',
];

$config = [
    'id' => 'app_flems',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_flems\controllers',
    'bootstrap' => [
        'log',
        /*function() {
            Yii::$app->db->createCommand()->update('pesanan', ['status' => 'Dibatalkan Otomatis'], '(status = "Diminta" or status = "Disetujui Manager" or status = "Dialokasikan Dispatcher" or status = "Disetujui Supervisor") and waktu_keberangkatan > now()')->execute();
            // Yii::$app->db->createCommand()->update('pesanan', ['status' => 'Checkout'], '(status = "SPK Telah Siap") and waktu_keberangkatan > now()')->execute();
        },*/
    ],
    'modules' => [
        'location' => [
            'class' => 'technosmart\modules\dev\Module',
        ]
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app_flems',
        ],
        'user' => [
            'identityClass' => 'app_flems\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_flems', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'session-app_flems',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

if (php_sapi_name() != 'cli' && YII_ENV == 'dev') {
    $config['modules']['dev'] = [
        'class' => 'technosmart\modules\dev\Module',
    ];
}

return $config;