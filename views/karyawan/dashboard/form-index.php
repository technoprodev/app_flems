<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

// technosmart\assets_manager\ChoicesAsset::register($this);
technosmart\assets_manager\FlatpickrAsset::register($this);
technosmart\assets_manager\ChartAsset::register($this);

//
$pesananPenumpangs = [];
if (isset($model['pesanan_penumpang']))
    foreach ($model['pesanan_penumpang'] as $key => $pesananPenumpang)
        $pesananPenumpangs[] = $pesananPenumpang->attributes;

$this->registerJs(
    'vm.$data.pesanan.pesananPenumpangs = vm.$data.pesanan.pesananPenumpangs.concat(' . json_encode($pesananPenumpangs) . ');' .
    'technoart.footerFixed();',
    3
);

//
$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['pesanan']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['pesanan'], ['class' => '']);
}

if (isset($model['pesanan_penumpang'])) foreach ($model['pesanan_penumpang'] as $key => $pesananPenumpang) {
    if ($pesananPenumpang->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($pesananPenumpang, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}

$this->registerJsFile('@web/app/karyawan/dashboard/form-index1.js', ['depends' => 'technosmart\assets_manager\VueAsset']);
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyBhx0Dgxc0tv-B2Dn0oCAc4bfKV32oGDyM&libraries=places', ['position' => 1]);
$this->registerJsFile('@web/app/karyawan/dashboard/form-index.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>
<style type="text/css">
    .flatpickr-time input {
        background-color: #3376b8;
        color: #fff;
    }
</style>

<div class="box box-break-md box-space-md box-gutter">
    <div class="box-7 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Permintaan Kendaraan</h2>
        
        <ul class="nav nav-tabs f-bold">
            <li class="active"><a href="#tab-penumpang" data-toggle="tab">Penumpang</a></li>
            <li class="activ3"><a href="#tab-bis" data-toggle="tab">Bis</a></li>
            <li class="activ3"><a href="#tab-shuttle" data-toggle="tab">Shuttle</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane padding-20 fade active in" id="tab-penumpang">
                <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>

                    <?php if ($error) : ?>
                        <div class="alert alert-danger">
                            <?= $errorMessage ?>
                        </div>
                    <?php endif; ?>

                
                    <?= $form->field($model['pesanan'], 'tipe')->hiddenInput(['value'=> 'Penumpang'])->label(false); ?>

                    <div class="padding-x-15 padding-top-15 margin-bottom-15 border-azure bg-light-azure" data-toggle="tooltip" data-placement="bottom" title="Dimohon untuk memilih jenis perjalanan terlebih dahulu">
                    <?= $form->field($model['pesanan'], 'tipe_penumpang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-2 padding-x-0 text-right m-text-left">
                            <label class="form-label fw-bold">Jenis Perjalanan</label>
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <?= Html::activeRadioList($model['pesanan'], 'tipe_penumpang', $model['pesanan']->getEnum('tipe_penumpang'), ['class' => 'form-radio', 'unselect' => null,
                                'item' => function($index, $label, $name, $checked, $value){
                                    $checked = $checked ? 'checked' : '';
                                    $disabled = in_array($value, []) ? 'disabled' : '';
                                    return "<label><input type='radio' name='$name' value='$value' $checked $disabled v-model='pesanan.tipe_penumpang' v-on:click='technoart.footerFixed();'><i></i>$label</label>";
                                }]); ?>
                            <?= Html::error($model['pesanan'], 'tipe_penumpang', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['pesanan'], 'tipe_penumpang')->end(); ?>
                    </div>

                    <div v-show="pesanan.tipe_penumpang != ''">

                    <?= $form->field($model['pesanan'], 'waktu_keberangkatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                            <?= Html::activeLabel($model['pesanan'], 'waktu_keberangkatan', ['class' => 'form-label fw-bold']); ?>
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <?= Html::activeTextInput($model['pesanan'], 'waktu_keberangkatan', ['class' => 'form-text input-flatpickr-time-custom', 'v-model' => 'pesanan.waktu_keberangkatan']) ?>
                            <?= Html::error($model['pesanan'], 'waktu_keberangkatan', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['pesanan'], 'waktu_keberangkatan')->end(); ?>

                    <?= $form->field($model['pesanan'], 'waktu_kembali', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                            <?= Html::activeLabel($model['pesanan'], 'waktu_kembali', ['class' => 'form-label fw-bold']); ?>
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <?= Html::activeTextInput($model['pesanan'], 'waktu_kembali', ['class' => 'form-text input-flatpickr-time-custom']) ?>
                            <?= Html::error($model['pesanan'], 'waktu_kembali', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['pesanan'], 'waktu_kembali')->end(); ?>

                    <?= $form->field($model['pesanan'], 'tujuan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                            <?= Html::activeLabel($model['pesanan'], 'tujuan', ['class' => 'form-label fw-bold']); ?>
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <?= Html::activeHiddenInput($model['pesanan'], 'tujuan', ['v-model' => 'pesanan.tujuan']) ?>
                            <?= Html::activeHiddenInput($model['pesanan'], 'tujuan_latitude', ['v-model' => 'pesanan.tujuan_latitude']) ?>
                            <?= Html::activeHiddenInput($model['pesanan'], 'tujuan_longitude', ['v-model' => 'pesanan.tujuan_longitude']) ?>

                            <div v-show="pesanan.tujuan">
                                <div class="box box-break-sm">
                                    <div class="box-11 padding-left-0">
                                        <div class="padding-y-5 margin-bottom-5">{{pesanan.tujuan}}</div>
                                    </div>
                                    <div class="box-1 padding-0 text-right">
                                        <div v-on:click="removeTujuan()" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                                    </div>
                                </div>
                            </div>

                            <template v-if="typeof pesanan.pesananTujuans == 'object' && pesanan.tipe_penumpang == 'Circle Trip'">
                                <template v-for="(value, key, index) in pesanan.pesananTujuans">
                                    <div v-show="!(value.id < 0)">
                                        <div class="box box-break-sm">
                                            <div class="box-11 padding-left-0">
                                                <input type="hidden" v-bind:id="'pesananpenumpang-' + key + '-id'" v-bind:name="'PesananTujuan[' + key + '][id]'" class="form-text" type="text" v-model="pesanan.pesananTujuans[key].id">
                                                <input type="hidden" v-bind:id="'pesananpenumpang-' + key + '-tujuan'" v-bind:name="'PesananTujuan[' + key + '][tujuan]'" class="form-text" type="text" v-model="pesanan.pesananTujuans[key].tujuan">
                                                <input type="hidden" v-bind:id="'pesananpenumpang-' + key + '-tujuan_latitude'" v-bind:name="'PesananTujuan[' + key + '][tujuan_latitude]'" class="form-text" type="text" v-model="pesanan.pesananTujuans[key].tujuan_latitude">
                                                <input type="hidden" v-bind:id="'pesananpenumpang-' + key + '-tujuan_longitude'" v-bind:name="'PesananTujuan[' + key + '][tujuan_longitude]'" class="form-text" type="text" v-model="pesanan.pesananTujuans[key].tujuan_longitude">
                                                <div class="padding-y-5 margin-bottom-5">{{pesanan.pesananTujuans[key].tujuan}}</div>
                                            </div>
                                            <div class="box-1 padding-0 text-right">
                                                <div v-on:click="removePesananTujuan(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </template>
                            </template>

                            <div>
                                <div v-show="pesanan.tipe_penumpang == 'Circle Trip' || !pesanan.tujuan">
                                    <input type="text" id="map1-input" class="form-text">
                                </div>
                                <div id="map1" style="height: 320px;" class="margin-y-5" v-show="pesanan.tipe_penumpang == 'Circle Trip' || !pesanan.tujuan"></div>
                            </div>

                            <div class="form-info"></div>
                        </div>
                    <?= $form->field($model['pesanan'], 'tujuan')->end(); ?>

                    <?= $form->field($model['pesanan'], 'penumpang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                            <?= Html::activeLabel($model['pesanan'], 'penumpang', ['class' => 'form-label fw-bold']); ?>
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <div class="padding-y-5">
                                <?= Yii::$app->user->identity->name ?>
                                <?= Html::error($model['pesanan'], 'penumpang', ['class' => 'form-info']); ?>
                            </div>

                            <?php if (isset($model['pesanan_penumpang'])) foreach ($model['pesanan_penumpang'] as $key => $value): ?>
                                <?= $form->field($model['pesanan_penumpang'][$key], "[$key]id_penumpang")->begin(); ?>
                                <?= $form->field($model['pesanan_penumpang'][$key], "[$key]id_penumpang")->end(); ?>
                            <?php endforeach; ?>

                            <template v-if="typeof pesanan.pesananPenumpangs == 'object'">
                                <template v-for="(value, key, index) in pesanan.pesananPenumpangs">
                                    <div v-show="!(value.id < 0)">
                                        <div class="box box-break-sm">
                                            <div class="box-11 padding-left-0">
                                                <input type="hidden" v-bind:id="'pesananpenumpang-' + key + '-id'" v-bind:name="'PesananPenumpang[' + key + '][id]'" class="form-text" type="text" v-model="pesanan.pesananPenumpangs[key].id">
                                                <div v-bind:class="'form-wrapper field-orderpenumpang-' + key + '-id_penumpang'">
                                                    <select v-bind:id="'pesananpenumpang-' + key + '-id_penumpang'" v-bind:name="'PesananPenumpang[' + key + '][id_penumpang]'" class="form-dropdown input-choices" v-model="pesanan.pesananPenumpangs[key].id_penumpang">
                                                        <option value="">Pilih Penumpang</option>
                                                        <?php foreach (ArrayHelper::map(
                                                                \Yii::$app->db->createCommand(
                                                                    "SELECT u.id, CONCAT(u.name, ' - ', k.nik) AS name FROM user u
                                                                    JOIN karyawan k ON k.id_user = u.id
                                                                    WHERE u.id <> :id
                                                                    ", ['id' => Yii::$app->user->identity->id]
                                                                )->queryAll(),
                                                                'id',
                                                                'name'
                                                            ) as $key => $value) : ?>
                                                            <option value="<?= $key ?>"><?= $value ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <div class="help-block"></div>
                                                </div>
                                            </div>
                                            <div class="box-1 padding-0 text-right">
                                                <div v-on:click="removePesananPenumpang(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </template>
                            </template>

                            <div class="padding-y-5" v-if="pesanan.pesananPenumpangs.length <= 4">
                                <a v-on:click="addPesananPenumpang" class="button button-sm text-azure border-azure hover-bg-light-azure">Tambah Penumpang</a>
                            </div>
                        </div>
                    <?= $form->field($model['pesanan'], 'penumpang')->end(); ?>

                    <?= $form->field($model['pesanan'], 'keperluan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                            <?= Html::activeLabel($model['pesanan'], 'keperluan', ['class' => 'form-label fw-bold']); ?>
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <?= Html::activeTextInput($model['pesanan'], 'keperluan', ['class' => 'form-text', 'maxlength' => true]) ?>
                            <?= Html::error($model['pesanan'], 'keperluan', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['pesanan'], 'keperluan')->end(); ?>

                    <?= $form->field($model['pesanan'], 'disetujui_manager', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                            <?= Html::activeLabel($model['pesanan'], 'disetujui_manager', ['class' => 'form-label fw-bold', 'label' => 'Disetujui Oleh']); ?>
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <?= Html::activeDropDownList($model['pesanan'], 'disetujui_manager',
                                ArrayHelper::map(
                                    \Yii::$app->db->createCommand(
                                        "
                                            SELECT u.id, CONCAT(u.name, ' - ', k.nik) AS name FROM karyawan k
                                            LEFT JOIN auth_assignment aa ON aa.user_id = k.id_user
                                            LEFT JOIN user u ON u.id = k.id_user
                                            WHERE id_sub_unit_kerja = :idSubUnitKerja
                                            AND aa.item_name = 'Manager'
                                            AND u.id <> :id
                                        ", [
                                            'idSubUnitKerja' => Yii::$app->user->identity->karyawan->id_sub_unit_kerja,
                                            'id' => Yii::$app->user->identity->id,
                                        ]
                                    )->queryAll(), 'id', 'name'
                                ), ['prompt' => 'Choose one please', 'class' => 'form-dropdown']);
                            ?>
                            <?= Html::error($model['pesanan'], 'disetujui_manager', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['pesanan'], 'disetujui_manager')->end(); ?>
                    

                    <hr class="margin-y-15">

                    <?php if ($error) : ?>
                        <div class="alert alert-danger">
                            <?= $errorMessage ?>
                        </div>
                    <?php endif; ?>

                    <div class="box box-break-sm margin-bottom-15">
                        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <?= Html::submitButton('Submit', ['class' => 'button button-sm bg-azure border-azure hover-bg-light-azure']) ?>
                            <?= Html::resetButton('Reset', ['class' => 'button button-sm bg-lightest text-azure border-azure hover-bg-light-azure']); ?>
                        </div>
                    </div>

                    </div>
                    
                <?php ActiveForm::end(); ?>

            </div>
            <div class="tab-pane padding-20 fade" id="tab-bis">
            </div>
            <div class="tab-pane padding-20 fade" id="tab-shuttle">
                <div id="app-shuttle">
                    <div class="form-wrapper box box-break-sm">
                        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                            <div class="fw-bold">From</div>
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <select class="form-text">
                                <option>Bandung</option>
                                <option>Jakarta</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-wrapper box box-break-sm">
                        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                            <div class="fw-bold">To</div>
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <select class="form-text">
                                <option>Jakarta</option>
                                <option>Bandung</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-wrapper box box-break-sm">
                        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                            <div class="fw-bold">Departure</div>
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <input type="text" name="" class="form-text input-flatpickr-time">
                        </div>
                    </div>

                    <div class="form-wrapper box box-break-sm">
                        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                            <div class="fw-bold">Waktu</div>
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <div class="padding-y-5">
                                <div class="radio">
                                    <label>
                                        <input id="radio-inline-1" name="radio-inline" value="1" checked="" type="radio">
                                        06.30 <span class="text-azure">(Sisa kursi: 7)</span>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="radio-inline-2" name="radio-inline" value="2" type="radio">
                                        07.00 <span class="text-azure">(Sisa kursi: 3)</span>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="radio-inline-3" name="radio-inline" value="3" type="radio" disabled="">
                                        07.30 <span class="text-red">(Sisa kursi: 0)</span>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="radio-inline-2" name="radio-inline" value="2" type="radio" disabled="">
                                        08.00 <span class="text-red">(Sisa kursi: 0)</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-wrapper box box-break-sm">
                        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <div class="padding-y-5">
                                <div class="checkbox">
                                    <label>
                                        <input name="checkbox-inline" value="1" checked="" type="checkbox" v-model="tempReturn">
                                        Return
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <template v-if="tempReturn == 1">
                        <div class="form-wrapper box box-break-sm">
                            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                                <div class="fw-bold">Return</div>
                            </div>
                            <div class="box-10 m-padding-x-0">
                                <input type="text" name="" class="form-text input-flatpickr-time">
                            </div>
                        </div>
                        <div class="form-wrapper box box-break-sm">
                            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                                <div class="fw-bold">Waktu</div>
                            </div>
                            <div class="box-10 m-padding-x-0">
                                <div class="padding-y-5">
                                    <div class="radio">
                                        <label>
                                            <input id="radio-inline-1" name="radio-inline" value="1" checked="" type="radio">
                                            16.30 <span class="text-azure">(Sisa kursi: 7)</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input id="radio-inline-2" name="radio-inline" value="2" type="radio">
                                            17.00 <span class="text-red">(Sisa kursi: 0)</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input id="radio-inline-3" name="radio-inline" value="3" type="radio" disabled="">
                                            17.30 <span class="text-azure">(Sisa kursi: 3)</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input id="radio-inline-2" name="radio-inline" value="2" type="radio" disabled="">
                                            18.00 <span class="text-red">(Sisa kursi: 0)</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </template>

                    <div class="box box-break-sm margin-bottom-15">
                        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                        </div>
                        <div class="box-10 m-padding-x-0">
                            <?= Html::submitButton('Submit', ['class' => 'button button-sm bg-azure border-azure hover-bg-light-azure']) ?>
                            <?= Html::resetButton('Reset', ['class' => 'button button-sm bg-lightest text-azure border-azure hover-bg-light-azure']); ?> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-5 bg-lightest shadow padding-30 rounded-md" style="max-height: 900px; overflow-y: scroll;">
        <ul class="nav nav-tabs f-bold">
            <li class="active"><a href="#tab-dalam_proses" data-toggle="tab">Dalam Proses</a></li>
            <li class="activ3"><a href="#tab-selesai" data-toggle="tab">Selesai</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane padding-20 fade active in" id="tab-dalam_proses">
                <table class="datatables-dalam-proses table table-no-line">
                </table>

                <?php foreach ([]/*$dalamProses*/ as $key => $pesanan) : ?>
                <div class="border rounded-xs padding-20 margin-bottom-15">                
                    <h5 class="margin-top-0">
                        <span class="text-azure margin-right-10">#<?= $pesanan->id ?></span>
                        <span class="fs-16 f-normal"><?= $pesanan->tipe ?> - </span>
                        <span class="fs-16 f-bold"><?= $pesanan->status ?></span>
                    </h5>

                    <div class="box box-break-sm">
                        <div class="box-12 padding-0 margin-bottom-10">
                            <div class=""><?= $pesanan->tujuan ? $pesanan->tujuan : '(kosong)' ?></div>
                        </div>

                        <div class="box-6 padding-0 margin-bottom-15">
                            <div class=""><code class="margin-0 bg-light-spring border-light-spring">OTP In :</code> <?= $pesanan->otp_checkin ? $pesanan->otp_checkin : '(kosong)' ?></div>
                            <div class=""><code class="margin-0 bg-light-spring border-light-spring">OTP Out:</code> <?= $pesanan->otp_checkout ? $pesanan->otp_checkout : '(kosong)' ?></div>
                        </div>
                    
                        <div class="box-6 padding-0 margin-bottom-15">
                            <div class=""><code class="margin-0 bg-light-azure border-light-azure">Start:</code> <?= DateTime::createFromFormat('d/m/Y H:i:s', $pesanan->waktu_keberangkatan)->format("d M, h:i") ?></div>
                            <div class=""><code class="margin-0 bg-light-azure border-light-azure">End  :</code> <?= DateTime::createFromFormat('d/m/Y H:i:s', $pesanan->waktu_kembali)->format("d M, h:i") ?></div>
                        </div>

                    <?php foreach ($pesanan->pesananPenumpangs as $key => $pesananPenumpang) : ?>

                        <!-- <div class="box-1 padding-left-0">
                            <div class="fw-bold">Penumpang <?= $key+2 ?></div>
                            <div class=""><?= $pesananPenumpang->id ? $pesananPenumpang->name : '(kosong)' ?></div>
                        </div> -->
                    
                    <?php endforeach; ?>

                        <div class="box-12 padding-0">
                            <a href="<?= Yii::$app->urlManager->createUrl(['karyawan/dashboard/detail', 'id' => $pesanan->id]) ?>" class="margin-right-10" modal-md="" modal-title="#<?= $pesanan->id ?>"><img src="https://image.flaticon.com/icons/svg/179/179614.svg" height="30" width="auto"></a>
                            <a class=""><img src="https://image.flaticon.com/icons/svg/179/179429.svg" height="30" width="auto"></a>
                        </div>

                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="tab-pane padding-20 fade" id="tab-selesai">
                <table class="datatables-selesai table table-no-line">
                    <thead>
                        <tr class="text-dark">
                            <th>a</th>
                        </tr>
                    </thead>
                </table>

                <?php foreach ([]/*$selesai*/ as $key => $pesanan) : ?>
                <div class="border rounded-xs padding-20 margin-bottom-15">                
                    <h5 class="margin-top-0">
                        <span class="text-azure margin-right-10">#<?= $pesanan->id ?></span>
                        <span class="fs-16 f-normal"><?= $pesanan->tipe ?> - </span>
                        <span class="fs-16 f-bold"><?= $pesanan->status ?></span>
                    </h5>

                    <div class="box box-break-sm">
                        <div class="box-12 padding-0 margin-bottom-10">
                            <div class=""><?= $pesanan->tujuan ? $pesanan->tujuan : '(kosong)' ?></div>
                        </div>

                        <div class="box-6 padding-0 margin-bottom-15">
                            <div class=""><code class="margin-0 bg-light-spring border-light-spring">OTP In :</code> <?= $pesanan->otp_checkin ? $pesanan->otp_checkin : '(kosong)' ?></div>
                            <div class=""><code class="margin-0 bg-light-spring border-light-spring">OTP Out:</code> <?= $pesanan->otp_checkout ? $pesanan->otp_checkout : '(kosong)' ?></div>
                        </div>
                    
                        <div class="box-6 padding-0 margin-bottom-15">
                            <div class=""><code class="margin-0 bg-light-azure border-light-azure">Start:</code> <?= DateTime::createFromFormat('d/m/Y H:i:s', $pesanan->waktu_keberangkatan)->format("d M, h:i") ?></div>
                            <div class=""><code class="margin-0 bg-light-azure border-light-azure">End  :</code> <?= DateTime::createFromFormat('d/m/Y H:i:s', $pesanan->waktu_kembali)->format("d M, h:i") ?></div>
                        </div>

                    <?php foreach ($pesanan->pesananPenumpangs as $key => $pesananPenumpang) : ?>

                        <!-- <div class="box-1 padding-left-0">
                            <div class="fw-bold">Penumpang <?= $key+2 ?></div>
                            <div class=""><?= $pesananPenumpang->id ? $pesananPenumpang->name : '(kosong)' ?></div>
                        </div> -->
                    
                    <?php endforeach; ?>

                        <div class="box-12 padding-0">
                            <a href="<?= Yii::$app->urlManager->createUrl(['karyawan/dashboard/detail', 'id' => $pesanan->id]) ?>" class="margin-right-10" modal-md="" modal-title="#<?= $pesanan->id ?>"><img src="https://image.flaticon.com/icons/svg/179/179614.svg" height="30" width="auto"></a>
                            <a class=""><img src="https://image.flaticon.com/icons/svg/179/179429.svg" height="30" width="auto"></a>
                        </div>

                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!-- <div class="box-6 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Total Pesanan</h2>
        <canvas id="semua" width="400px" height="400px"></canvas>
    </div> -->
</div>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhx0Dgxc0tv-B2Dn0oCAc4bfKV32oGDyM&libraries=places" async defer></script> -->
