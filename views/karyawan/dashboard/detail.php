<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Pesanan</h2>
<?php endif; ?>
        <div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Status</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->status ? $model['pesanan']->status : '(kosong)' ?>
                    </div>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Tipe Pesanan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->tipe ? $model['pesanan']->tipe : '(kosong)' ?>
                    </div>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Waktu Keberangkatan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->waktu_keberangkatan ? DateTime::createFromFormat('d/m/Y H:i:s', $model['pesanan']->waktu_keberangkatan)->format("d M, h:i") : '(kosong)' ?>
                    </div>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Waktu Kembali</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->waktu_kembali ? DateTime::createFromFormat('d/m/Y H:i:s', $model['pesanan']->waktu_kembali)->format("d M, h:i") : '(kosong)' ?>
                    </div>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Tujuan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->tujuan ? $model['pesanan']->tujuan : '(kosong)' ?>
                    </div>
                    
                    <?php foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan) : ?>
                    
                    <div class="padding-y-5">
                        <?= $pesananTujuan->id ? $pesananTujuan->tujuan : '(kosong)' ?>
                    </div>
                    
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Penumpang</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->penumpang->name ?>
                    </div>
                    
                    <?php foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang) : ?>
                    
                    <div class="padding-y-5">
                        <?= $pesananPenumpang->id ? $pesananPenumpang->name : '(kosong)' ?>
                    </div>
                    
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Keperluan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <div class="border-none padding-0"><?= $model['pesanan']->keperluan ? $model['pesanan']->keperluan : '(kosong)' ?></div>
                    </div>
                </div>
            </div>
        </div>
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
