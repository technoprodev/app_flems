<?php

use app_flems\assets_manager\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();

if (Yii::$app->session->hasFlash('success'))
    $this->registerJs(
        'fn.alert("Success", "' . Yii::$app->session->getFlash('success') . '", "success");',
        3
    );
if (Yii::$app->session->hasFlash('info'))
    $this->registerJs(
        'fn.alert("Info", "' . Yii::$app->session->getFlash('info') . '", "info");',
        3
    );
if (Yii::$app->session->hasFlash('warning'))
    $this->registerJs(
        'fn.alert("Warning", "' . Yii::$app->session->getFlash('warning') . '", "warning");',
        3
    );
if (Yii::$app->session->hasFlash('error'))
    $this->registerJs(
        'fn.alert("Error", "' . Yii::$app->session->getFlash('error') . '", "danger");',
        3
    );
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="<?= Yii::$app->params['app.description'] ?>">
        <meta name="keywords" content="<?= Yii::$app->params['app.keywords'] ?>">
        <meta name="author" content="<?= Yii::$app->params['app.author'] ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <link rel="shortcut icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <div class="wrapper">
            <div class="header bg-azure hidden-sm-less">
                <div class="header-left padding-top-10 text-center darker-20">
                    <a href="<?= Yii::$app->urlManager->createUrl("site/index") ?>" class="a-nocolor">
                        <span class="f-italic fs-21 fw-bold"><?= Yii::$app->params['app.name'] ?></span>
                    </a>
                </div>
                <div class="header-right padding-x-30 m-padding-x-10">
                    <div class="pull-left padding-y-10 fs-21 fw-bold text-lightest"><?= Yii::$app->params['app.description'] ?></div>
                    
                    <?php if (!Yii::$app->user->isGuest) : ?>
                        <div class="pull-right padding-top-10">
                            <a class="button button-xs border-lightest text-lightest" data-method="post" href="<?= Yii::$app->urlManager->createUrl("site/logout") ?>">Logout</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="header bg-azure visible-sm-less">
                <div class="padding-x-30 m-padding-x-10">
                    <div class="pull-left padding-y-10 fs-21 fw-bold text-lightest"><?= Yii::$app->params['app.description'] ?></div>
                    
                    <?php if (!Yii::$app->user->isGuest) : ?>
                        <div class="pull-right padding-top-10">
                            <a class="button button-xs border-lightest text-lightest" data-method="post" href="<?= Yii::$app->urlManager->createUrl("site/logout") ?>">Logout</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="body has-sidebar-left bg-light">
                <aside class="sidebar-left bg-lightest shadow-top-right">
                    <div class="margin-top-15"></div>
                    
                    <div class="bg-azure margin-x-15 padding-20 rounded-sm text-center">
                        <div class="">
                            <i class="fa fa-user fs-30 circle-icon border-lightest bg-lightest text-azure"></i>
                        </div>
                        <div class="margin-top-10"></div>
                        <div>Selamat Datang</div>
                        <div class="fs-18 fw-bold text-wrap"><?= Yii::$app->user->identity->name ?></div>
                        <!-- <hr>
                        <?php $roles = (function($roles){foreach ($roles as $key => $value) $roles[$key] = $value->description; return $roles;})(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())) ?>
                        <div>Role Anda:</div>
                        <div class="fs-14 fw-bold text-wrap"><?= implode(', ', $roles) ?></div>  -->
                    </div>

                    <div class="margin-top-20"></div>

                    <?php $menu['sidebar'] = [
                        [
                            'label' => 'Permintaan Kendaraan',
                            'url' => ['karyawan/dashboard/index'],
                            'visible' => Menu::menuVisible(true, 'karyawan/dashboard', 'index'),
                        ],
                        [
                            'label' => 'Persetujuan Manager',
                            'url' => ['manager/dashboard/index'],
                            'visible' => Menu::menuVisible(true, 'manager/dashboard', 'index'),
                        ],
                        [
                            'label' => 'Alokasi Kendaraan',
                            'url' => ['dispatcher/dashboard/index'],
                            'visible' => Menu::menuVisible(true, 'dispatcher/dashboard', 'index'),
                        ],
                        [
                            'label' => 'Data Supir',
                            'url' => ['dispatcher/supir/index'],
                            'visible' => Menu::menuVisible(true, 'dispatcher/supir', 'index'),
                        ],
                        [
                            'label' => 'Pairing',
                            'url' => ['dispatcher/pairing/index'],
                            'visible' => Menu::menuVisible(true, 'dispatcher/pairing', 'index'),
                        ],
                        [
                            'label' => 'Alokasi Biaya',
                            'url' => ['supervisor/dashboard/index'],
                            'visible' => Menu::menuVisible(true, 'supervisor/dashboard', 'index'),
                        ],
                        [
                            'label' => 'Laporan Supervisor',
                            'url' => null,
                            'visible' => 1,
                            'items' => [
                                [
                                    'label' => 'Laporan Permintaan',
                                    'url' => ['supervisor/laporan/permintaan'],
                                    'visible' => Menu::menuVisible(true, 'supervisor/laporan', 'permintaan'),
                                ],
                                [
                                    'label' => 'Laporan Pemakaian',
                                    'url' => ['supervisor/laporan/pemakaian'],
                                    'visible' => Menu::menuVisible(true, 'supervisor/laporan', 'pemakaian'),
                                ],
                                [
                                    'label' => 'Laporan Biaya',
                                    'url' => ['supervisor/laporan/biaya'],
                                    'visible' => Menu::menuVisible(true, 'supervisor/laporan', 'biaya'),
                                ],
                                [
                                    'label' => 'Laporan Performa Supir',
                                    'url' => ['supervisor/laporan/performa-supir'],
                                    'visible' => Menu::menuVisible(true, 'supervisor/laporan', 'performa-supir'),
                                ],
                                [
                                    'label' => 'Laporan Spk Supir',
                                    'url' => ['supervisor/laporan/spk-supir'],
                                    'visible' => Menu::menuVisible(true, 'supervisor/laporan', 'spk-supir'),
                                ],
                                [
                                    'label' => 'Laporan Pengeluaran Supir',
                                    'url' => ['supervisor/laporan/pengeluaran-supir'],
                                    'visible' => Menu::menuVisible(true, 'supervisor/laporan', 'pengeluaran-supir'),
                                ],
                                [
                                    'label' => 'Laporan Petty Cash',
                                    'url' => ['supervisor/laporan/petty-cash'],
                                    'visible' => Menu::menuVisible(true, 'supervisor/laporan', 'petty-cash'),
                                ],
                            ],
                        ],
                        [
                            'label' => 'Data Supir',
                            'url' => ['supervisor/supir/index'],
                            'visible' => Menu::menuVisible(true, 'supervisor/supir', 'index'),
                        ],
                        [
                            'label' => 'Pairing',
                            'url' => ['supervisor/pairing/index'],
                            'visible' => Menu::menuVisible(true, 'supervisor/pairing', 'index'),
                        ],
                        [
                            'label' => 'Dashboard Admin Pool',
                            'url' => ['admin_pool/dashboard/index'],
                            'visible' => Menu::menuVisible(true, 'admin_pool/dashboard', 'index'),
                        ],
                        [
                            'label' => 'Petty Cash',
                            'url' => ['admin_pool/petty-cash/index'],
                            'visible' => Menu::menuVisible(true, 'admin_pool/petty-cash', 'index'),
                        ],
                        [
                            'label' => 'Dashboard Supir',
                            'url' => ['supir/dashboard/index'],
                            'visible' => Menu::menuVisible(true, 'supir/dashboard', 'index'),
                        ],
                        [
                            'label' => 'Transaksi Supir',
                            'url' => ['supir/transaksi/index'],
                            'visible' => Menu::menuVisible(true, 'supir/transaksi', 'index'),
                        ],
                        [
                            'label' => 'Laporan Admin Pusat',
                            'url' => null,
                            'visible' => 1,
                            'items' => [
                                [
                                    'label' => 'Laporan Permintaan',
                                    'url' => ['admin_pusat/laporan/permintaan'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/laporan', 'permintaan'),
                                ],
                                [
                                    'label' => 'Laporan Pemakaian',
                                    'url' => ['admin_pusat/laporan/pemakaian'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/laporan', 'pemakaian'),
                                ],
                                [
                                    'label' => 'Laporan Biaya',
                                    'url' => ['admin_pusat/laporan/biaya'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/laporan', 'biaya'),
                                ],
                                [
                                    'label' => 'Laporan Performa Supir',
                                    'url' => ['admin_pusat/laporan/performa-supir'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/laporan', 'performa-supir'),
                                ],
                                [
                                    'label' => 'Laporan Spk Supir',
                                    'url' => ['admin_pusat/laporan/spk-supir'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/laporan', 'spk-supir'),
                                ],
                                [
                                    'label' => 'Laporan Pengeluaran Supir',
                                    'url' => ['admin_pusat/laporan/pengeluaran-supir'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/laporan', 'pengeluaran-supir'),
                                ],
                                [
                                    'label' => 'Laporan Petty Cash',
                                    'url' => ['admin_pusat/laporan/petty-cash'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/laporan', 'petty-cash'),
                                ],
                            ],
                        ],
                        [
                            'label' => 'Ubah Data',
                            'url' => null,
                            'visible' => Menu::menuVisible(true, 'admin_pusat/dashboard', 'index'),
                            'items' => [
                                [
                                    'label' => 'Data Karyawan',
                                    'url' => ['admin_pusat/karyawan/index'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/karyawan', 'index'),
                                ],
                                [
                                    'label' => 'Data Supir',
                                    'url' => ['admin_pusat/supir/index'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/supir', 'index'),
                                ],
                                [
                                    'label' => 'Data Mobil',
                                    'url' => ['admin_pusat/mobil/index'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/mobil', 'index'),
                                ],
                                [
                                    'label' => 'Data Pairing',
                                    'url' => ['admin_pusat/pairing/index'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/pairing', 'index'),
                                ],
                                [
                                    'label' => 'Data Unit Kerja',
                                    'url' => ['admin_pusat/unit-kerja/index'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/unit-kerja', 'index'),
                                ],
                                [
                                    'label' => 'Data Sub Unit Kerja',
                                    'url' => ['admin_pusat/sub-unit-kerja/index'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/sub-unit-kerja', 'index'),
                                ],
                                [
                                    'label' => 'Data Vendor',
                                    'url' => ['admin_pusat/vendor/index'],
                                    'visible' => Menu::menuVisible(true, 'admin_pusat/vendor', 'index'),
                                ],
                            ],
                        ],
                    ]; ?>

                    <?=
                        MenuWidget::widget([
                            'items' => $menu['sidebar'],
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bold submenu-active-bold menu-active-bg-light-yellow menu-active-text-dark-yellow menu-hover-darker-10 submenu-active-bg-light-blue',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                    ?>

                    <hr class="border-lighter margin-x-20">
                </aside>

                <div class="page-wrapper padding-x-30 m-padding-x-10 padding-y-15">
                    <?= $content ?>
                </div>
            </div>

            <footer class="footer bg-lightest padding-y-10 shadow-top-right text-azure">
                <div class="text-center fs-18">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-app.png" height="26px;" class="text-middle">
                    <span class="text-middle">© <?= date('Y') ?></span>
                </div>
            </footer>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>