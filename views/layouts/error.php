<?php

use app_flems\assets_manager\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();

if (Yii::$app->session->hasFlash('success'))
    $this->registerJs(
        'fn.alert("Success", "' . Yii::$app->session->getFlash('success') . '", "success");',
        3
    );
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="<?= Yii::$app->params['app.description'] ?>">
        <meta name="keywords" content="<?= Yii::$app->params['app.keywords'] ?>">
        <meta name="author" content="<?= Yii::$app->params['app.author'] ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <link rel="shortcut icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <!-- <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- /END @ALERT & CONFIRM -->

        <!-- START @WRAPPER -->
        <section id="wrapper" class="bg-lighter">
            <!-- START @HEADER -->
            <!-- <header id="header">
            </header> -->
            <!-- /END @HEADER -->

            <!-- START @BODY -->
            <section id="body">
                <!-- START @SIDEBAR LEFT -->
                <!-- <aside id="sidebar-left">
                </aside> -->
                <!-- /END @SIDEBAR LEFT -->

                <!-- START @PAGE WRAPPER -->
                <section id="page-wrapper">
                    <!-- START @PAGE SLIDER -->
                    <!-- <section id="page-slider">
                    </section> -->
                    <!-- /END @PAGE SLIDER -->

                    <!-- START @PAGE HEADER -->
                    <!-- <section id="page-header">
                    </section> -->
                    <!-- /END @PAGE HEADER -->

                    <!-- START @PAGE BODY -->
                    <section id="page-body">
                        <?= $content ?>
                    </section>
                    <!-- /END @PAGE BODY -->
                </section>
                <!-- /END @PAGE WRAPPER -->

                <!-- START @SIDEBAR RIGHT -->
                <!-- <aside id="sidebar-right">
                </aside> -->
                <!-- /END @SIDEBAR RIGHT -->
            </section>
            <!-- /END @BODY -->

            <!-- START @FOOTER -->
            <!-- <footer id="footer">
            </footer> -->
            <!-- /END @FOOTER -->
        </section>
        <!-- /END @WRAPPER -->

        <!-- START @BACK TO TOP -->
        <!-- <div id="back-to-top">
            <div class="padding-10 bg-gray fs-18 border">
                <span class="glyphicon glyphicon-arrow-up"></span>
            </div>
        </div> -->
        <!-- /END @BACK TO TOP -->

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>