<?php

use app_flems\assets_manager\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();

if (Yii::$app->session->hasFlash('success'))
    $this->registerJs(
        'fn.alert("Success", "' . Yii::$app->session->getFlash('success') . '", "success");',
        3
    );
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="<?= Yii::$app->params['app.description'] ?>">
        <meta name="keywords" content="<?= Yii::$app->params['app.keywords'] ?>">
        <meta name="author" content="<?= Yii::$app->params['app.author'] ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <link rel="shortcut icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <div class="wrapper has-bg-img">
            <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/login-v1.jpg');"></div>
            <div class="box box-break-sm has-positioned-child">
                <div class="top-center visible-sm-greater" style="z-index:2;top:50px;">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-inverse.png" width="100px;" class="bg-lightest shadow circle padding-5">
                </div>
                <div class="box-6 has-bg-img padding-x-60 m-padding-x-15 padding-y-5" style="height: 100vh;">
                    <div class="bg-img darker-50"></div>
                    <div class="bg-img azure-30"></div>
                    <div class="margin-top-30"></div>
                    <h1 class="text-lightest">
                        <!-- <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" width="25px;" class="margin-right-15"> -->
                        <?= Yii::$app->params['app.author'] ?>
                    </h1>
                    <div class="margin-top-150"></div>
                    <div class="text-lightest fs-60 fw-light">
                        Selamat Datang di bjb <span class="fw-bold text-light-yellow"><?= Yii::$app->params['app.name'] ?></span>
                    </div>
                    <div class="margin-top-10"></div>
                    <div class="text-light fs-30 fw-light">
                        <?= Yii::$app->params['app.description'] ?>
                    </div>
                    <div class="margin-top-100"></div>
                    <div>
                        <a href="#" class="button button-md rounded-xs padding-x-30 border-lightest bg-lightest text-darkest hover-bg-transparent">Panduan Penggunaan</a>
                    <div class="margin-top-100"></div>
                    </div>
                    <div>
                        <a href="#" class="text-yellow margin-right-30">Tentang <?= Yii::$app->params['app.name'] ?></a>
                        <a href="#" class="text-yellow margin-right-30">Kontak</a>
                        <a href="#" class="text-yellow margin-right-30">FAQ</a>
                    </div>
                </div>
                <div class="box-6 has-bg-img padding-x-60 m-padding-x-15 padding-y-5" style="height: 100vh;">
                    <div class="bg-img lighter-50"></div>
                    <div class="bg-img lighter-50"></div>
                    <div class="bg-img lighter-50"></div>
                    <div class="bg-img lighter-50"></div>
                    <div class="margin-top-150 m-margin-top-50"></div>
                    
                    <?= $content ?>
                </div>
            </div>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>