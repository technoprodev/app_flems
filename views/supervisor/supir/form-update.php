<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['supir']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['supir'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['user'], 'username', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'username', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['user'], 'username', ['class' => 'form-text']) ?>
        <?= Html::error($model['user'], 'username', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'username')->end(); ?>

    <?= $form->field($model['user'], 'name', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'name', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['user'], 'name', ['class' => 'form-text']) ?>
        <?= Html::error($model['user'], 'name', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'name')->end(); ?>

    <?= $form->field($model['user'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'email', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['user'], 'email', ['class' => 'form-text']) ?>
        <?= Html::error($model['user'], 'email', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'email')->end(); ?>

    <?= $form->field($model['user'], 'phone', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'phone', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['user'], 'phone', ['class' => 'form-text']) ?>
        <?= Html::error($model['user'], 'phone', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'phone')->end(); ?>

    <?= $form->field($model['supir'], 'nik', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'nik', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['supir'], 'nik', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['supir'], 'nik', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'nik')->end(); ?>

    <?= $form->field($model['supir'], 'tipe', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'tipe', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['supir'], 'tipe', ['Penumpang' => 'Penumpang', 'Bis' => 'Bis', 'Cargo' => 'Cargo', 'Shuttle' => 'Shuttle'], ['prompt' => 'Choose one please', 'class' => 'form-dropdown']) ?>
        <?= Html::error($model['supir'], 'tipe', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'tipe')->end(); ?>

    <?= $form->field($model['supir'], 'id_mobil', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'id_mobil', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['supir'], 'id_mobil', ArrayHelper::map(\app_flems\models\Mobil::find()->indexBy('id')->asArray()->all(), 'id', 'nomor_polisi'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown']); ?>
        <?= Html::error($model['supir'], 'id_mobil', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'id_mobil')->end(); ?>

    <?= $form->field($model['supir'], 'id_pool', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'id_pool', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['supir'], 'id_pool', ArrayHelper::map(\app_flems\models\UnitKerja::find()->indexBy('id')->asArray()->all(), 'id', 'unit_kerja'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown']); ?>
        <?= Html::error($model['supir'], 'id_pool', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'id_pool')->end(); ?>

    <?= $form->field($model['supir'], 'id_vendor', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'id_vendor', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['supir'], 'id_vendor', ArrayHelper::map(\app_flems\models\Vendor::find()->indexBy('id')->asArray()->all(), 'id', 'vendor'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown']); ?>
        <?= Html::error($model['supir'], 'id_vendor', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'id_vendor')->end(); ?>

    <?= $form->field($model['supir'], 'nomor_ktp', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'nomor_ktp', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['supir'], 'nomor_ktp', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['supir'], 'nomor_ktp', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'nomor_ktp')->end(); ?>

    <?= $form->field($model['supir'], 'tipe_sim', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'tipe_sim', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['supir'], 'tipe_sim', [ 'A' => 'A', 'B1' => 'B1', 'B2' => 'B2', 'C' => 'C', ], ['prompt' => 'Choose one please', 'class' => 'form-dropdown']) ?>
        <?= Html::error($model['supir'], 'tipe_sim', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'tipe_sim')->end(); ?>

    <?= $form->field($model['supir'], 'nomor_sim', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'nomor_sim', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['supir'], 'nomor_sim', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['supir'], 'nomor_sim', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'nomor_sim')->end(); ?>

    <?= $form->field($model['supir'], 'alamat', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'alamat', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextArea($model['supir'], 'alamat', ['class' => 'form-text', 'rows' => 6]) ?>
        <?= Html::error($model['supir'], 'alamat', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'alamat')->end(); ?>

    <?= $form->field($model['supir'], 'tempat_lahir', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'tempat_lahir', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['supir'], 'tempat_lahir', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['supir'], 'tempat_lahir', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'tempat_lahir')->end(); ?>

    <?= $form->field($model['supir'], 'tanggal_lahir', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'tanggal_lahir', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['supir'], 'tanggal_lahir', ['class' => 'form-text']) ?>
        <?= Html::error($model['supir'], 'tanggal_lahir', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'tanggal_lahir')->end(); ?>

    <?= $form->field($model['supir'], 'jenis_kelamin', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'jenis_kelamin', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['supir'], 'jenis_kelamin', [ 'Male' => 'Male', 'Female' => 'Female', ], ['prompt' => 'Choose one please', 'class' => 'form-dropdown']) ?>
        <?= Html::error($model['supir'], 'jenis_kelamin', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'jenis_kelamin')->end(); ?>

    <?= $form->field($model['supir'], 'pendidikan_terakhir', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'pendidikan_terakhir', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['supir'], 'pendidikan_terakhir', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['supir'], 'pendidikan_terakhir', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'pendidikan_terakhir')->end(); ?>

    <?= $form->field($model['supir'], 'golongan_darah', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'golongan_darah', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['supir'], 'golongan_darah', [ 'A' => 'A', 'AB' => 'AB', 'B' => 'B', 'O' => 'O', ], ['prompt' => 'Choose one please', 'class' => 'form-dropdown']) ?>
        <?= Html::error($model['supir'], 'golongan_darah', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'golongan_darah')->end(); ?>

    <?= $form->field($model['supir'], 'status', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'status', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['supir'], 'status', [ 'Status 1' => 'Status 1', 'Status 2' => 'Status 2', ], ['prompt' => 'Choose one please', 'class' => 'form-dropdown']) ?>
        <?= Html::error($model['supir'], 'status', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'status')->end(); ?>

    <?= $form->field($model['supir'], 'agama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'agama', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['supir'], 'agama', [ 'Islam' => 'Islam', 'Kristen' => 'Kristen', 'Katolik' => 'Katolik', 'Budha' => 'Budha', 'Hindu' => 'Hindu', 'Konghucu' => 'Konghucu', ], ['prompt' => 'Choose one please', 'class' => 'form-dropdown']) ?>
        <?= Html::error($model['supir'], 'agama', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'agama')->end(); ?>

    <?= $form->field($model['supir'], 'kewarganegaraan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'kewarganegaraan', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['supir'], 'kewarganegaraan', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['supir'], 'kewarganegaraan', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'kewarganegaraan')->end(); ?>

    <?= $form->field($model['supir'], 'pelatihan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'pelatihan', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['supir'], 'pelatihan', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['supir'], 'pelatihan', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'pelatihan')->end(); ?>

    <?= $form->field($model['supir'], 'nomor_kontrak', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'nomor_kontrak', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['supir'], 'nomor_kontrak', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['supir'], 'nomor_kontrak', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'nomor_kontrak')->end(); ?>

    <?= $form->field($model['supir'], 'tanggal_kontrak', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'tanggal_kontrak', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['supir'], 'tanggal_kontrak', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['supir'], 'tanggal_kontrak', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'tanggal_kontrak')->end(); ?>

    <?= $form->field($model['supir'], 'sanksi_keterangan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'sanksi_keterangan', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextArea($model['supir'], 'sanksi_keterangan', ['class' => 'form-text', 'rows' => 6]) ?>
        <?= Html::error($model['supir'], 'sanksi_keterangan', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'sanksi_keterangan')->end(); ?>

    <?= $form->field($model['supir'], 'sanksi_sp1', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'sanksi_sp1', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['supir'], 'sanksi_sp1', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['supir'], 'sanksi_sp1', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'sanksi_sp1')->end(); ?>

    <?= $form->field($model['supir'], 'sanksi_sp2', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'sanksi_sp2', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['supir'], 'sanksi_sp2', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['supir'], 'sanksi_sp2', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'sanksi_sp2')->end(); ?>

    <?= $form->field($model['supir'], 'sanksi_sp3', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'sanksi_sp3', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['supir'], 'sanksi_sp3', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['supir'], 'sanksi_sp3', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'sanksi_sp3')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['supir']->isNewRecord ? 'Create' : 'Update', ['class' => 'button bg-azure border-azure hover-bg-light-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'button text-azure border-azure hover-bg-light-azure']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'button text-azure border-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>