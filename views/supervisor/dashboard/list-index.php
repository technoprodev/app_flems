<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/supervisor/dashboard/list-index-butuh_persetujuan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/supervisor/dashboard/list-index-dalam_proses.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/supervisor/dashboard/list-index-selesai.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/supervisor/dashboard/list-index-revisi_anggaran.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/supervisor/dashboard/list-index-transaksi_admin_pengajuan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="box box-break-sm box-space-md box-gutter">
    <?php if ((new \yii\db\Query())
            ->select('count(*)')
            ->from('pesanan p')
            ->join('LEFT JOIN', 'sub_unit_kerja suk', 'p.id_sub_unit_kerja = suk.id')
            ->where('p.status_realisasi = "Diminta"')
            ->andWhere(['suk.id_unit_kerja' => Yii::$app->user->identity->karyawan->subUnitKerja->id_unit_kerja])->scalar())
    : ?>
        <div class="box-12 bg-lightest shadow padding-30 rounded-md">
            <h2 class="margin-top-0 margin-bottom-30 text-red">Butuh Persetujuan Mendesak !</h2>
            <div class="padding-15 border-light-red bg-light-red margin-bottom-15">
                Pesanan ini membutuhkan tambahan anggaran
            </div>
            <table id="table-revisi_anggaran" class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Status</th>
                        <th>Penumpang</th>
                        <th>Supir</th>
                        <th>Waktu Keberangkatan</th>
                        <th>Waktu Kembali</th>
                        <th>Anggaran</th>
                        <th>Tipe Perjalanan</th>
                    </tr>
                </thead>
            </table>
        </div>
    <?php else: ?>
        <div class="box-12 bg-lightest shadow padding-30 rounded-md">
            <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Alokasi Permintaan Kendaraan</h2>
            <ul class="nav nav-tabs f-bold">
                <li class="active"><a href="#tab-butuh_persetujuan" data-toggle="tab">Butuh Persetujuan</a></li>
                <li class="activ3"><a href="#tab-dalam_proses" data-toggle="tab">Dalam Proses</a></li>
                <li class="activ3"><a href="#tab-selesai" data-toggle="tab">Selesai</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane padding-20 fade active in" id="tab-butuh_persetujuan">
                    <table id="table-butuh_persetujuan" class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Penumpang</th>
                                <th>Supir</th>
                                <th>Waktu Keberangkatan</th>
                                <th>Waktu Kembali</th>
                                <th>Anggaran</th>
                                <th>Tipe Perjalanan</th>
                            </tr>
                        </thead>
                        <!-- <tbody>
                            <tr>
                                <td>
                                    <a href="" class="text-azure margin-right-5" data-confirm="Are you sure you want to approve this order?" data-method="post">Approve</a>
                                    <a href="" class="text-rose margin-right-5" data-confirm="Are you sure you want to reject this order?" data-method="post">Reject</a>
                                    <a href="<?= Yii::$app->urlManager->createUrl("supervisor/dashboard/detail-butuh-persetujuan") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                                    <a href="" class="text-azure margin-right-5" modal-md="" modal-title="#1">Ganti Supir</a>
                                </td>
                                <td>29333</td>
                                <td><b>admin pradana</b></td>
                                <td>Anton Suhendra</td>
                                <td>10/01/2018 08:00</td>
                                <td>20/01/2018 17:00</td>
                                <td>Rp 800.000</td>
                                <td>Dalam Kota</td>
                            </tr>
                        </tbody> -->
                    </table>
                </div>
                <div class="tab-pane padding-20 fade" id="tab-dalam_proses">
                    <table id="table-dalam_proses" class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Status</th>
                                <th>Penumpang</th>
                                <th>Supir</th>
                                <th>Waktu Keberangkatan</th>
                                <th>Waktu Kembali</th>
                                <th>Anggaran</th>
                                <th>Tipe Perjalanan</th>
                            </tr>
                        </thead>
                        <!-- <tbody>
                            <tr>
                                <td>
                                    <a href="<?= Yii::$app->urlManager->createUrl("supervisor/dashboard/detail-dalam-proses") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                                </td>
                                <td>29333</td>
                                <td><b>admin pradana</b></td>
                                <td>Anton Suhendra <span class="border-azure bg-light-azure padding-y-2 padding-x-5 fs-11 rounded-sm">Sudah di accept driver</span></td>
                                <td>10/01/2018 08:00</td>
                                <td>20/01/2018 17:00</td>
                                <td>Rp 800.000</td>
                                <td>Dalam Kota</td>
                            </tr>
                        </tbody> -->
                    </table>
                </div>
                <div class="tab-pane padding-20 fade" id="tab-selesai">
                    <table id="table-selesai" class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Status</th>
                                <th>Penumpang</th>
                                <th>Supir</th>
                                <th>Waktu Keberangkatan</th>
                                <th>Waktu Kembali</th>
                                <th>Anggaran</th>
                                <th>Tipe Perjalanan</th>
                            </tr>
                        </thead>
                        <!-- <tbody>
                            <tr>
                                <td>
                                    <a href="<?= Yii::$app->urlManager->createUrl("supervisor/dashboard/detail-selesai") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                                </td>
                                <td>29333</td>
                                <td><b>admin pradana</b></td>
                                <td>Anton Suhendra</td>
                                <td>10/01/2018 08:00</td>
                                <td>20/01/2018 17:00</td>
                                <td>Rp 800.000</td>
                                <td>Dalam Kota</td>
                            </tr>
                        </tbody> -->
                    </table>
                </div>
            </div>
        </div>
        <div class="box-12 bg-lightest shadow padding-30 rounded-md">
            <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Petty Cash</h2>
            <table id="table-transaksi_admin_pengajuan" class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th>Admin yang Mengajukan</th>
                        <th>Total yang Diajukan</th>
                        <th>Waktu Pengajuan</th>
                    </tr>
                </thead>
            </table>
        </div>
    <?php endif; ?>
</div>