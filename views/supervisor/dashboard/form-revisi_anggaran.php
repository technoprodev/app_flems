<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
// ddx('vm.pesanan.tol = ' . ($model['pesanan']->tol != null ? json_encode((int)$model['pesanan']->tol) : 'null') . ';');
$this->registerJsFile('@web/app/supervisor/dashboard/form-revisi_anggaran.js', ['depends' => 'technosmart\assets_manager\VueAsset']);
$this->registerJs(
    'vm.pesanan.anggaran = ' . ($model['pesanan']->anggaran != null ? json_encode((int)$model['pesanan']->anggaran) : 'null') . ';' .
    'vm.pesanan.bbm = ' . ($model['pesanan']->bbm != null ? json_encode((int)$model['pesanan']->bbm) : 'null') . ';' .
    'vm.pesanan.tol = ' . ($model['pesanan']->tol != null ? json_encode((int)$model['pesanan']->tol) : 'null') . ';' .
    'vm.pesanan.parkir = ' . ($model['pesanan']->parkir != null ? json_encode((int)$model['pesanan']->parkir) : 'null') . ';' .
    'vm.pesanan.revisi_bbm = ' . ($model['pesanan']->revisi_bbm != null ? json_encode((int)$model['pesanan']->revisi_bbm) : 'null') . ';' .
    'vm.pesanan.revisi_tol = ' . ($model['pesanan']->revisi_tol != null ? json_encode((int)$model['pesanan']->revisi_tol) : 'null') . ';' .
    'vm.pesanan.revisi_parkir = ' . ($model['pesanan']->revisi_parkir != null ? json_encode((int)$model['pesanan']->revisi_parkir) : 'null') . ';' .
    '',
    3
);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">#<?= $model['pesanan']->id ?> Butuh Tambahan Aggaran</h2>
<?php endif; ?>
        <div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Status</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->status ? $model['pesanan']->status : '(kosong)' ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Tipe Pesanan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->tipe ? $model['pesanan']->tipe : '(kosong)' ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Waktu Keberangkatan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->waktu_keberangkatan ? DateTime::createFromFormat('d/m/Y H:i:s', $model['pesanan']->waktu_keberangkatan)->format("d M, h:i") : '(kosong)' ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Waktu Kembali</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->waktu_kembali ? DateTime::createFromFormat('d/m/Y H:i:s', $model['pesanan']->waktu_kembali)->format("d M, h:i") : '(kosong)' ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Tujuan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="">
                        <?= $model['pesanan']->tujuan ? $model['pesanan']->tujuan : '(kosong)' ?>
                    </div>
                    
                    <?php foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan) : ?>
                    
                    <div class="padding-y-5">
                        <?= $pesananTujuan->id ? $pesananTujuan->tujuan : '(kosong)' ?>
                    </div>
                    
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Penumpang</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="">
                        <?= $model['pesanan']->penumpang->name ?>
                    </div>
                    
                    <?php foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang) : ?>
                    
                    <div class="padding-y-5">
                        <?= $pesananPenumpang->id ? $pesananPenumpang->name : '(kosong)' ?>
                    </div>
                    
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Keperluan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->keperluan ? $model['pesanan']->keperluan : '(kosong)' ?>
                </div>
            </div>

            <hr>

            <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Total Anggaran</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    {{rupiah(pesanan.anggaran)}}
                </div>
            </div>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    
                </div>
                <div class="box-10 m-padding-x-0">
                    <span class="fw-bold">Realisasi yang diinput Supir</span>
                </div>
            </div>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">BBM</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    {{pesanan.bbm != null ? rupiah(pesanan.bbm) : '(kosong)'}}
                </div>
            </div>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Tol</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    {{pesanan.tol != null ? rupiah(pesanan.tol) : '(kosong)'}}
                </div>
            </div>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Parkir</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    {{pesanan.parkir != null ? rupiah(pesanan.parkir) : '(kosong)'}}
                </div>
            </div>

            <?php if($model['pesanan']->bbm != $model['pesanan']->revisi_bbm || $model['pesanan']->tol != $model['pesanan']->revisi_tol || $model['pesanan']->parkir != $model['pesanan']->revisi_parkir) : ?>
                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left"></div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-15 border-light-green bg-light-green">
                            Realisasi yang diinput supir telah direvisi Admin
                        </div>
                        <div class="margin-top-15"></div>
                        <div>
                            <span class="fw-bold">Revisi yang diinput Admin</span>
                        </div>
                    </div>
                </div>

                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left">
                        <div class="fw-bold">Revisi BBM</div>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= $model['pesanan']->revisi_bbm != null ? 'Rp' . number_format($model['pesanan']->revisi_bbm, 2) : 'kosong' ?>
                        <!-- {{pesanan.revisi_bbm != null ? rupiah(pesanan.revisi_bbm) : '(kosong)'}} -->
                    </div>
                </div>

                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left">
                        <div class="fw-bold">Revisi Tol</div>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= $model['pesanan']->revisi_tol != null ? 'Rp' . number_format($model['pesanan']->revisi_tol, 2) : 'kosong' ?>
                        <!-- {{pesanan.revisi_tol != null ? rupiah(pesanan.revisi_tol) : '(kosong)'}} -->
                    </div>
                </div>

                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left">
                        <div class="fw-bold">Revisi Parkir</div>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= $model['pesanan']->revisi_parkir != null ? 'Rp' . number_format($model['pesanan']->revisi_parkir, 2) : 'kosong' ?>
                        <!-- {{pesanan.revisi_parkir != null ? rupiah(pesanan.revisi_parkir) : '(kosong)'}} -->
                    </div>
                </div>
            <?php endif; ?>

            <hr>

            <?= $form->field($model['pesanan'], 'status_realisasi', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <?= Html::activeLabel($model['pesanan'], 'status_realisasi', ['class' => 'form-label fw-bold', 'label' => 'Tindak Lanjut']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::activeHiddenInput($model['pesanan'], 'status_realisasi', ['v-model' => 'pesanan.status_realisasi']); ?>
                    <div class="form-checkbox form-checkbox-inline">
                        <label><input v-model="pesanan.revisi" value="0" type="radio"> Setuju (Tanpa Revisi)</label>
                        <label><input v-model="pesanan.revisi" value="1" type="radio"> Setuju (dan Revisi)</label>
                        <label><input v-model="pesanan.revisi" value="2" type="radio"> Tolak</label>
                    </div>
                    <?= Html::error($model['pesanan'], 'status_realisasi', ['class' => 'form-info']); ?>
                </div>
            <?= $form->field($model['pesanan'], 'status_realisasi')->end(); ?>

            <div v-if="pesanan.revisi == '0'">
                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left">
                        <span v-if="sisa_realisasi < 0" class="text-red">Penambahan Anggaran</span>
                        <span v-else class="text-azure">Sisa Anggaran</span>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        {{rupiah(sisa_realisasi < 1 ? sisa_realisasi*-1 : sisa_realisasi)}}
                    </div>
                </div>
                
                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left"></div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::submitButton('Submit', ['class' => 'button button-sm bg-azure border-azure hover-bg-light-azure']) ?>
                    </div>
                </div>
            </div>

            <div v-else-if="pesanan.revisi == '1'">
                <?= $form->field($model['pesanan'], 'revisi_bbm', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                        <?= Html::activeLabel($model['pesanan'], 'revisi_bbm', ['class' => 'form-label fw-bold']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextInput($model['pesanan'], 'revisi_bbm', ['class' => 'form-text', 'v-model.number' => 'pesanan.revisi_bbm']) ?>
                        <?= Html::error($model['pesanan'], 'revisi_bbm', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pesanan'], 'revisi_bbm')->end(); ?>

                <?= $form->field($model['pesanan'], 'revisi_tol', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                        <?= Html::activeLabel($model['pesanan'], 'revisi_tol', ['class' => 'form-label fw-bold']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextInput($model['pesanan'], 'revisi_tol', ['class' => 'form-text', 'v-model.number' => 'pesanan.revisi_tol']) ?>
                        <?= Html::error($model['pesanan'], 'revisi_tol', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pesanan'], 'revisi_tol')->end(); ?>

                <?= $form->field($model['pesanan'], 'revisi_parkir', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                        <?= Html::activeLabel($model['pesanan'], 'revisi_parkir', ['class' => 'form-label fw-bold']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextInput($model['pesanan'], 'revisi_parkir', ['class' => 'form-text', 'v-model.number' => 'pesanan.revisi_parkir']) ?>
                        <?= Html::error($model['pesanan'], 'revisi_parkir', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pesanan'], 'revisi_parkir')->end(); ?>

                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left">
                        <span v-if="sisa_realisasi < 0" class="text-red">Penambahan Anggaran</span>
                        <span v-else class="text-azure">Sisa Anggaran</span>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        {{rupiah(sisa_realisasi < 1 ? sisa_realisasi*-1 : sisa_realisasi)}}
                    </div>
                </div>

                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left"></div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::submitButton('Submit', ['class' => 'button button-sm bg-azure border-azure hover-bg-light-azure']) ?>
                    </div>
                </div>
            </div>

            <div v-else-if="pesanan.revisi == '2'">
                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left"></div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::submitButton('Submit', ['class' => 'button button-sm bg-red border-red hover-bg-light-red']) ?>
                    </div>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
