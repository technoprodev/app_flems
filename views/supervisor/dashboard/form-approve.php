<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\ChoicesAsset::register($this);

$this->registerJs(
    'vm.pesanan.id_supir = ' . json_encode((int)$model['pesanan']->id_supir) . ';' .
    '',
    3
);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">#<?= $model['pesanan']->id ?> Butuh Persetujuan</h2>
<?php endif; ?>
        <div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Status</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->status ? $model['pesanan']->status : '(kosong)' ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Tipe Pesanan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->tipe ? $model['pesanan']->tipe : '(kosong)' ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Waktu Keberangkatan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->waktu_keberangkatan ? DateTime::createFromFormat('d/m/Y H:i:s', $model['pesanan']->waktu_keberangkatan)->format("d M, h:i") : '(kosong)' ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Waktu Kembali</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->waktu_kembali ? DateTime::createFromFormat('d/m/Y H:i:s', $model['pesanan']->waktu_kembali)->format("d M, h:i") : '(kosong)' ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Tujuan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->tujuan ? $model['pesanan']->tujuan : '(kosong)' ?>
                    
                    <?php foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan) : ?>
                    
                    <div class="margin-top-5">
                        <?= $pesananTujuan->id ? $pesananTujuan->tujuan : '(kosong)' ?>
                    </div>
                    
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Penumpang</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->penumpang->name ?>
                    
                    <?php foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang) : ?>
                    
                    <div class="margin-top-5">
                        <?= $pesananPenumpang->id ? $pesananPenumpang->name : '(kosong)' ?>
                    </div>
                    
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Keperluan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="border-none padding-0"><?= $model['pesanan']->keperluan ? $model['pesanan']->keperluan : '(kosong)' ?></div>
                </div>
            </div>

            <hr>

            <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>

            <?= $form->field($model['pesanan'], 'anggaran', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'anggaran', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::activeTextInput($model['pesanan'], 'anggaran', ['class' => 'form-text']) ?>
                    <?= Html::error($model['pesanan'], 'anggaran', ['class' => 'form-info']); ?>
                </div>
            <?= $form->field($model['pesanan'], 'anggaran')->end(); ?>

            <?= $form->field($model['pesanan'], 'spj', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'spj', ['class' => 'form-label fw-bold', 'label' => 'SPJ ?']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= Html::activeRadioList($model['pesanan'], 'spj', ['Tidak', 'Ya'], ['class' => 'form-radio', 'unselect' => null,
                            'item' => function($index, $label, $name, $checked, $value){
                                $checked = $checked ? 'checked' : '';
                                $disabled = in_array($value, []) ? 'disabled' : '';
                                return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                            }]); ?>
                        <?= Html::error($model['pesanan'], 'spj', ['class' => 'form-info']); ?>
                    </div>
                </div>
            <?= $form->field($model['pesanan'], 'spj')->end(); ?>

            <?= $form->field($model['pesanan'], 'id_supir', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'id_supir', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::activeDropDownList($model['pesanan'], 'id_supir', ArrayHelper::map(
                        \Yii::$app->db->createCommand(
                            "
                                SELECT u.id, CONCAT(u.name, ' - ', m.nomor_polisi) as name
                                FROM supir s
                                JOIN user u ON u.id = s.id_user
                                JOIN mobil m on m.id = s.id_mobil
                                WHERE s.id_pool = :idPool
                                AND s.tipe = 'Penumpang'
                            ",
                            [
                                'idPool' => Yii::$app->user->identity->karyawan->id_pool,
                            ]
                        )->queryAll(),
                        'id', 'name'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown input-choices'/*, 'v-model' => 'pesanan.id_supir'*/]); ?>
                    <?= Html::error($model['pesanan'], 'id_supir', ['class' => 'form-info']); ?>
                </div>
            <?= $form->field($model['pesanan'], 'id_supir')->end(); ?>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::submitButton('Setuju', ['class' => 'button button-sm bg-azure border-azure hover-bg-light-azure']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
