<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/supervisor/laporan/list-pemakaian.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
        <table class="datatables table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th></th>
                    <th>Penumpang</th>
                    <th>Supir</th>
                    <th>Tipe Perjalanan</th>
                    <th>Mobil</th>
                    <th>Pool</th>
                    <th>Tujuan</th>
                    <th>Estimasi Jarak</th>
                    <th>Waktu Keberangkatan</th>
                    <th>Waktu Kembali</th>
                    <th>Anggaran</th>
                    <!-- <th>Biaya</th> -->
                </tr>
            </thead>
        </table>
    </div>
</div>