<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/supervisor/performa-supir/list-index.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Supir Performance</h2>
        <table id="table" class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th></th>
                    <th>Supir</th>
                    <th>Nik</th>
                    <th>Tipe</th>
                    <th>Vendor</th>
                    <th>Plat Number</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>