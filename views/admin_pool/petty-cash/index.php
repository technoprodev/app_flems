<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/admin_pool/petty-cash/list-index-transaksi_admin_pengajuan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/admin_pool/dashboard/list-index-transaksi_admin.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);

//
$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['transaksi_admin_pengajuan']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['transaksi_admin_pengajuan'], ['class' => '']);
}

if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}

$lastModel = \app_flems\models\TransaksiAdmin::find()->where(['id_admin' => Yii::$app->user->identity->id, 'tipe' => 'Kas'])->orderBy(['id' => SORT_DESC])->limit(1)->asArray()->one();
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-4 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Pengajuan Pettty Cash</h2>
        <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>

            <?php if ($error) : ?>
                <div class="alert alert-danger">
                    <?= $errorMessage ?>
                </div>
            <?php endif; ?>

            <?= $form->field($model['transaksi_admin_pengajuan'], 'total_yang_diajukan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <?= Html::activeLabel($model['transaksi_admin_pengajuan'], 'total_yang_diajukan', ['class' => 'form-label fw-bold']); ?>
                <?= Html::activeTextInput($model['transaksi_admin_pengajuan'], 'total_yang_diajukan', ['class' => 'form-text']) ?>
                <?= Html::error($model['transaksi_admin_pengajuan'], 'total_yang_diajukan', ['class' => 'form-info']); ?>
            <?= $form->field($model['transaksi_admin_pengajuan'], 'total_yang_diajukan')->end(); ?>

            <div class="form-wrapper clearfix">
                <?= Html::submitButton('Submit', ['class' => 'button button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="box-8 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Hasil Pengajuan</h2>
        <table id="table-transaksi_admin_pengajuan" class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Status Pengajuan</th>
                    <th>Total yang Diajukan</th>
                    <th>Waktu Pengajuan</th>
                    <th>Disetujui</th>
                    <th>Ditolak</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="box-12"></div>
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Transaksi Petty Cash</h2>
        <h6>Total Saldo Petty Cash Tersisa adalah: Rp <b class="text-azure"><?= number_format($lastModel ? $lastModel['saldo'] : 0, 2) ?></b></h6>
        <table id="table-transaksi_admin" class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Waktu</th>
                    <th>Keterangan</th>
                    <th>SPK</th>
                    <th>Debit</th>
                    <th>Kredit</th>
                    <th>Saldo</th>
                </tr>
            </thead>
        </table>
    </div>
</div>