<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/admin_pool/dashboard/form-realisasi.js', ['depends' => 'technosmart\assets_manager\VueAsset']);
/*ddx('vm.pesanan.status_realisasi = ' . ($model['pesanan']->status_realisasi != null ? json_encode($model['pesanan']->status_realisasi) : 'null') . ';' .
    'vm.pesanan.anggaran = ' . ($model['pesanan']->anggaran != null ? json_encode((int)$model['pesanan']->anggaran) : 'null') . ';' .
    'vm.pesanan.bbm = ' . ($model['pesanan']->bbm != null ? json_encode((int)$model['pesanan']->bbm) : 'null') . ';' .
    'vm.pesanan.tol = ' . ($model['pesanan']->tol != null ? json_encode((int)$model['pesanan']->tol) : 'null') . ';' .
    'vm.pesanan.parkir = ' . ($model['pesanan']->parkir != null ? json_encode((int)$model['pesanan']->parkir) : 'null') . ';' .
    'vm.pesanan.revisi_bbm = ' . ($model['pesanan']->revisi_bbm != null ? json_encode((int)$model['pesanan']->revisi_bbm) : 'null') . ';' .
    'vm.pesanan.revisi_tol = ' . ($model['pesanan']->revisi_tol != null ? json_encode((int)$model['pesanan']->revisi_tol) : 'null') . ';' .
    'vm.pesanan.revisi_parkir = ' . ($model['pesanan']->revisi_parkir != null ? json_encode((int)$model['pesanan']->revisi_parkir) : 'null') . ';' .
    '');*/
$this->registerJs(
    'vm.pesanan.status_realisasi = ' . ($model['pesanan']->status_realisasi != null ? json_encode($model['pesanan']->status_realisasi) : 'null') . ';' .
    'vm.pesanan.anggaran = ' . ($model['pesanan']->anggaran != null ? json_encode((int)$model['pesanan']->anggaran) : 'null') . ';' .
    'vm.pesanan.bbm = ' . ($model['pesanan']->bbm != null ? json_encode((int)$model['pesanan']->bbm) : 'null') . ';' .
    'vm.pesanan.tol = ' . ($model['pesanan']->tol != null ? json_encode((int)$model['pesanan']->tol) : 'null') . ';' .
    'vm.pesanan.parkir = ' . ($model['pesanan']->parkir != null ? json_encode((int)$model['pesanan']->parkir) : 'null') . ';' .
    'vm.pesanan.revisi_bbm = ' . ($model['pesanan']->revisi_bbm != null ? json_encode((int)$model['pesanan']->revisi_bbm) : 'null') . ';' .
    'vm.pesanan.revisi_tol = ' . ($model['pesanan']->revisi_tol != null ? json_encode((int)$model['pesanan']->revisi_tol) : 'null') . ';' .
    'vm.pesanan.revisi_parkir = ' . ($model['pesanan']->revisi_parkir != null ? json_encode((int)$model['pesanan']->revisi_parkir) : 'null') . ';' .
    '',
    3
);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Realisasi #<?= $model['pesanan']->id ?></h2>
<?php endif; ?>
        <div>
            <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>

            <div v-if="pesanan.status_realisasi == 'Disetujui Supervisor'">
                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left"></div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-15 border-light-azure bg-light-azure">
                            Pengajuan penambahan anggaran telah disetujui Supervisor
                        </div>
                    </div>
                </div>
            </div>

            <div v-else-if="pesanan.status_realisasi == 'Ditolak Supervisor' && (sisa_realisasi < 0 || (pesanan.revisi_bbm + pesanan.revisi_tol + pesanan.revisi_parkir > pesanan.bbm + pesanan.tol + pesanan.parkir))">
                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left"></div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-15 border-light-red bg-light-red">
                            Pengajuan revisi biaya yang melebihi biaya yang diinput supir telah <b>ditolak</b> Supervisor
                        </div>
                    </div>
                </div>
            </div>

            <div v-else-if="pesanan.status_realisasi == 'Ditolak Supervisor'">
                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left"></div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-15 border-light-red bg-light-red">
                            Pengajuan penambahan anggaran telah <b>ditolak</b> Supervisor
                        </div>
                    </div>
                </div>
            </div>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Anggaran</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    {{rupiah(pesanan.anggaran)}}
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold" v-if="pesanan.status_realisasi == 'Disetujui Supervisor' && sisa_realisasi < 0">Tambahan Anggaran</div>
                    <div class="fw-bold" v-else>Sisa Realisasi</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <span v-if="pesanan.status_realisasi == 'Disetujui Supervisor' && sisa_realisasi < 0" class="text-azure">{{rupiah(sisa_realisasi*-1)}}</span>
                    <span v-else-if="sisa_realisasi < 0" class="text-red">{{rupiah(sisa_realisasi)}}</span>
                    <span v-else class="text-azure">{{rupiah(sisa_realisasi)}}</span>
                </div>
            </div>

            <div v-if="pesanan.status_realisasi == 'Disetujui Supervisor'">
                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left">
                        <div class="fw-bold">Revisi BBM</div>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        {{pesanan.revisi_bbm != null ? rupiah(pesanan.revisi_bbm) : '(kosong)'}}
                    </div>
                </div>

                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left">
                        <div class="fw-bold">Revisi Tol</div>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        {{pesanan.revisi_tol != null ? rupiah(pesanan.revisi_tol) : '(kosong)'}}
                    </div>
                </div>

                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left">
                        <div class="fw-bold">Revisi Parkir</div>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        {{pesanan.revisi_parkir != null ? rupiah(pesanan.revisi_parkir) : '(kosong)'}}
                    </div>
                </div>
            </div>

            <div v-if="pesanan.status_realisasi == null">
                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left"></div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-15 border-light-azure bg-light-azure">
                            Biaya yang telah diinput supir bisa Anda revisi
                        </div>
                    </div>
                </div>
            </div>

            <div v-if="pesanan.status_realisasi == 'Ditolak Supervisor'">
                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left"></div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-15 border-light-red bg-light-red">
                            Harap ubah realisasi dibawah
                        </div>
                    </div>
                </div>
            </div>

            <div v-if="pesanan.status_realisasi != 'Disetujui Supervisor'">
                <?= $form->field($model['pesanan'], 'revisi_bbm', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                        <?= Html::activeLabel($model['pesanan'], 'revisi_bbm', ['class' => 'form-label fw-bold']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextInput($model['pesanan'], 'revisi_bbm', ['class' => 'form-text', 'v-model.number' => 'pesanan.revisi_bbm']) ?>
                        <?= Html::error($model['pesanan'], 'revisi_bbm', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pesanan'], 'revisi_bbm')->end(); ?>

                <?= $form->field($model['pesanan'], 'revisi_tol', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                        <?= Html::activeLabel($model['pesanan'], 'revisi_tol', ['class' => 'form-label fw-bold']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextInput($model['pesanan'], 'revisi_tol', ['class' => 'form-text', 'v-model.number' => 'pesanan.revisi_tol']) ?>
                        <?= Html::error($model['pesanan'], 'revisi_tol', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pesanan'], 'revisi_tol')->end(); ?>

                <?= $form->field($model['pesanan'], 'revisi_parkir', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                        <?= Html::activeLabel($model['pesanan'], 'revisi_parkir', ['class' => 'form-label fw-bold']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextInput($model['pesanan'], 'revisi_parkir', ['class' => 'form-text', 'v-model.number' => 'pesanan.revisi_parkir']) ?>
                        <?= Html::error($model['pesanan'], 'revisi_parkir', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pesanan'], 'revisi_parkir')->end(); ?>
            </div>
            
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left"></div>
                <div class="box-10 m-padding-x-0">
                    <div v-if="pesanan.status_realisasi != 'Disetujui Supervisor' && sisa_realisasi < 0" class="padding-15 border-light-red bg-light-red">
                        Realisasi yang melebihi anggaran memerlukan persetujuan supervisor sebelum SPKnya bisa di akhiri
                    </div>
                    <div v-else-if="pesanan.status_realisasi != 'Disetujui Supervisor' && (pesanan.revisi_bbm + pesanan.revisi_tol + pesanan.revisi_parkir > pesanan.bbm + pesanan.tol + pesanan.parkir)" class="padding-15 border-light-red bg-light-red">
                        Revisi yang melebihi biaya yang diinput supir memerlukan persetujuan supervisor sebelum SPKnya bisa di akhiri
                    </div>
                </div>
            </div>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Nominal Voucher BBM</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= number_format($model['pesanan']->nominal_voucher_bbm, 2) ?>
                </div>
            </div>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Nomor Voucher BBM</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->nomor_voucher_bbm ?>
                </div>
            </div>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left"></div>
                    <?= Html::submitButton('Akhiri SPK', ['v-if' => 'pesanan.status_realisasi == \'Disetujui Supervisor\'', 'class' => 'button button-sm bg-azure border-azure hover-bg-light-azure']) ?>
                    <?= Html::submitButton('Mohon Persetujuan ke Supervisor', ['v-else-if' => 'sisa_realisasi < 0 || (pesanan.revisi_bbm + pesanan.revisi_tol + pesanan.revisi_parkir > pesanan.bbm + pesanan.tol + pesanan.parkir)', 'class' => 'button button-sm bg-red border-red hover-bg-light-red']) ?>
                    <?= Html::submitButton('Akhiri SPK', ['v-else-if' => 'sisa_realisasi >= 0', 'class' => 'button button-sm bg-azure border-azure hover-bg-light-azure']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
