<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/admin_pool/dashboard/list-index-transaksi_admin.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/admin_pool/dashboard/list-index-revisi_anggaran.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/admin_pool/dashboard/list-index-cetak_spk.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/admin_pool/dashboard/list-index-dalam_proses.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/admin_pool/dashboard/list-index-selesai.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$lastModel = \app_flems\models\TransaksiAdmin::find()->where(['id_admin' => Yii::$app->user->identity->id, 'tipe' => 'Kas'])->orderBy(['id' => SORT_DESC])->limit(1)->asArray()->one();
?>

<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Petty Cash</h2>
        <h6>
            Total Saldo Petty Cash Tersisa adalah: Rp
            <b class="text-azure"><?= number_format($lastModel ? $lastModel['saldo'] : 0, 2) ?></b>
            <a href="<?= Yii::$app->urlManager->createUrl("admin_pool/petty-cash/index") ?>" class="button border-azure text-azure hover-bg-azure hover-text-lightest margin-left-15">
                Tambah Anggaran
            </a>
        </h6>
        <table id="table-transaksi_admin" class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Waktu</th>
                    <th>Keterangan</th>
                    <th>SPK</th>
                    <th>Debit</th>
                    <th>Kredit</th>
                    <th>Saldo</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Perjalanan</h2>
        <table id="table-revisi_anggaran" class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Revisi</th>
                    <th>Status</th>
                    <th>Penumpang</th>
                    <th>Supir</th>
                    <th>Waktu Keberangkatan</th>
                    <th>Waktu Kembali</th>
                    <th>Anggaran</th>
                    <th>Tipe Perjalanan</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Pesanan</h2>
        <ul class="nav nav-tabs f-bold">
            <li class="active"><a href="#tab-cetak_spk" data-toggle="tab">Cetak SPK</a></li>
            <li class="activ3"><a href="#tab-dalam_proses" data-toggle="tab">Dalam Proses</a></li>
            <li class="activ3"><a href="#tab-selesai" data-toggle="tab">Selesai</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane padding-20 fade active in" id="tab-cetak_spk">
                <table id="table-cetak_spk" class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Penumpang</th>
                            <th>Supir</th>
                            <th>Waktu Keberangkatan</th>
                            <th>Waktu Kembali</th>
                            <th>Anggaran</th>
                            <th>Tipe Perjalanan</th>
                        </tr>
                    </thead>
                    <!-- <tbody>
                        <tr>
                            <td>
                                <a href="" class="text-azure margin-right-5" data-confirm="Are you sure you want to cetak this order?" data-method="post">Cetak</a>
                                <a href="<?= Yii::$app->urlManager->createUrl("admin/dashboard/detail-cetak-spk") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td>29333</td>
                            <td><b>admin pradana</b></td>
                            <td>Wawan Nugraha</td>
                            <td>03/02/2018</td>
                            <td>06/02/2018</td>
                            <td>300.000</td>
                            <td>Dalam Kota</td>
                        </tr>
                        <tr>
                            <td>
                                <a href="" class="text-azure margin-right-5" data-confirm="Are you sure you want to cetak this order?" data-method="post">Cetak</a>
                                <a href="<?= Yii::$app->urlManager->createUrl("admin/dashboard/detail-cetak-spk") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td>29334</td>
                            <td><b>Bambang pradana</b></td>
                            <td>Ririn Nugraha</td>
                            <td>03/02/2018</td>
                            <td>06/02/2018</td>
                            <td>200.000</td>
                            <td>Luar Kota</td>
                        </tr>
                    </tbody> -->
                </table>
            </div>
            <div class="tab-pane padding-20 fade" id="tab-dalam_proses">
                <table id="table-dalam_proses" class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Status</th>
                            <th>Penumpang</th>
                            <th>Supir</th>
                            <th>Waktu Keberangkatan</th>
                            <th>Waktu Kembali</th>
                            <th>Anggaran</th>
                            <th>Tipe Perjalanan</th>
                        </tr>
                    </thead>
                    <!-- <tbody>
                        <tr>
                            <td>
                                <a href="<?= Yii::$app->urlManager->createUrl("admin/dashboard/add-anggaran") ?>" class="text-azure margin-right-5" modal-md="" modal-title="Add Anggaran for Pesanan 29333">Add Anggaran</a>
                                <a href="<?= Yii::$app->urlManager->createUrl("admin/dashboard/detail-dalam-proses") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td>29335</td>
                            <td><b>admin pradana</b></td>
                            <td>Wawan Nugraha</td>
                            <td>03/02/2018</td>
                            <td>06/02/2018</td>
                            <td>300.000</td>
                            <td>Dalam Kota</td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Yii::$app->urlManager->createUrl("admin/dashboard/close") ?>" class="text-azure margin-right-5" modal-md="" modal-title="Add Anggaran for Pesanan 29333">Close</a>
                                <a href="<?= Yii::$app->urlManager->createUrl("admin/dashboard/detail-dalam-proses") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td>29336</td>
                            <td><b>Bambang pradana</b></td>
                            <td>Ririn Nugraha</td>
                            <td>03/02/2018</td>
                            <td>06/02/2018</td>
                            <td>200.000</td>
                            <td>Luar Kota</td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Yii::$app->urlManager->createUrl("admin/dashboard/detail-dalam-proses") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td>29337</td>
                            <td><b>Lola pradana</b></td>
                            <td>Ririn Nugraha</td>
                            <td>03/02/2018</td>
                            <td>06/02/2018</td>
                            <td>200.000</td>
                            <td>Luar Kota</td>
                        </tr>
                    </tbody> -->
                </table>
            </div>
            <div class="tab-pane padding-20 fade" id="tab-selesai">
                <table id="table-selesai" class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Status</th>
                            <th>Penumpang</th>
                            <th>Supir</th>
                            <th>Waktu Keberangkatan</th>
                            <th>Waktu Kembali</th>
                            <th>Anggaran</th>
                            <th>Tipe Perjalanan</th>
                        </tr>
                    </thead>
                    <!-- <tbody>
                        <tr>
                            <td>
                                <a href="<?= Yii::$app->urlManager->createUrl("admin/dashboard/detail-selesai") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td>29338</td>
                            <td><b>Bianca pradana</b></td>
                            <td>Bowo Nugraha</td>
                            <td>03/02/2018</td>
                            <td>06/02/2018</td>
                            <td>200.000</td>
                            <td>Luar Kota</td>
                        </tr>
                    </tbody> -->
                </table>
            </div>
        </div>
    </div>
</div>