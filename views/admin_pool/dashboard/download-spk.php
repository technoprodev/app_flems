<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$waktu_keberangkatan = DateTime::createFromFormat('d/m/Y H:i:s', $model['pesanan']->waktu_keberangkatan);
$waktu_kembali = DateTime::createFromFormat('d/m/Y H:i:s', $model['pesanan']->waktu_kembali);
$hari = [
    'Sun' => 'Minggu',
    'Mon' => 'Senin',
    'Tue' => 'Selasa',
    'Wed' => 'Rabu',
    'Thu' => 'Kamis',
    'Fri' => 'Jumat',
    'Sat' => 'Sabtu'
];
?>

<div class="">
    <table class="table table-no-line margin-0">
        <tbody>
            <tr>
                <td class="padding-bottom-0 text-left">
                    <img src="<?= $this->render('img-download-spk'); ?>" height="30px;">
                </td>
                <td class="padding-bottom-0 text-right">
                    <h3 class="margin-0">SURAT PERINTAH KERJA (SPK) <span class="f-bold"><?= '#' . $model['pesanan']->id ?></span></h3>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table table-no-line margin-0">
        <tbody>
            <tr>
                <td class="padding-top-0">
                    <div class="border-azure padding-15">
                        <h4 class="text-azure margin-top-0">Detail Pesanan</h4>
                        <div>
                            <span style="display:inline-block;width:130px;">Nama Pemesan</span>
                            <span style="display:inline-block;">: <?= $model['pesanan']->id_penumpang ? $model['pesanan']->penumpang->name : '(kosong)' ?></span>
                        </div>
                        <div class="margin-top-5"></div>
                        <div>
                            <span style="display:inline-block;width:130px;">Unit Kerja</span>
                            <span style="display:inline-block;">: <?= $model['pesanan']->id_penumpang ? $model['pesanan']->penumpang->karyawan->subUnitKerja->unitKerja->unit_kerja : '(kosong)' ?></span>
                        </div>
                        <div class="margin-top-5"></div>
                        <div>
                            <span style="display:inline-block;width:130px;">No. HP</span>
                            <span style="display:inline-block;">: <?= $model['pesanan']->id_penumpang ? $model['pesanan']->penumpang->phone : '(kosong)' ?></span>
                        </div>

                        <div class="margin-top-30"></div>

                        <div>
                            <span style="display:inline-block;width:130px;">Penumpang</span>
                            <span style="display:inline-block;">: <?= $model['pesanan']->id_penumpang ? $model['pesanan']->penumpang->name : '(kosong)' ?></span>
                            <?php foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang) : ?>
                                <div class="margin-top-5">
                                    <span style="display:inline-block;width:130px;"></span>
                                    <span style="display:inline-block;">: <?= $pesananPenumpang->id ? $pesananPenumpang->name : '(kosong)' ?></span>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="margin-top-5"></div>
                        <div>
                            <span style="display:inline-block;width:130px;">Jenis Pesanan</span>
                            <span style="display:inline-block;">: <?= $model['pesanan']->tipe_penumpang ?></span>
                        </div>
                        <div class="margin-top-5"></div>
                        <div>
                            <span style="display:inline-block;width:130px;">Waktu Berangkat</span>
                            <span style="display:inline-block;">: <?= $hari[$waktu_keberangkatan->format('D')] . $waktu_keberangkatan->format(" d M Y, h:i") . ' WIB' ?></span>
                        </div>
                        <div class="margin-top-5"></div>
                        <div>
                            <span style="display:inline-block;width:130px;">Waktu Kembali</span>
                            <span style="display:inline-block;">: <?= $hari[$waktu_kembali->format('D')] . $waktu_kembali->format(" d M Y, h:i") . ' WIB' ?></span>
                        </div>
                        <div class="margin-top-5"></div>
                        <div>
                            <span style="display:inline-block;width:130px;">Tujuan</span>
                            <span style="display:inline-block;">: <?= $model['pesanan']->tujuan ?></span>
                            <?php foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan) : ?>
                                <div class="margin-top-5">
                                    <span style="display:inline-block;width:130px;"></span>
                                    <span style="display:inline-block;">: <?= $pesananTujuan->id ? $pesananTujuan->tujuan : '(kosong)' ?></span>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="margin-top-5"></div>
                        <div>
                            <span style="display:inline-block;width:130px;">Keterangan</span>
                            <span style="display:inline-block;">: <?= $model['pesanan']->keperluan ?></span>
                        </div>

                        <div class="margin-top-30"></div>

                        <div>
                            <span style="display:inline-block;width:130px;">Nama Supir</span>
                            <span style="display:inline-block;">: <?= $model['pesanan']->id_supir ? $model['pesanan']->supir->name : '(kosong)' ?></span>
                        </div>
                        <div class="margin-top-5"></div>
                        <div>
                            <span style="display:inline-block;width:130px;">No. HP</span>
                            <span style="display:inline-block;">: <?= $model['pesanan']->id_supir ? $model['pesanan']->supir->phone : '(kosong)' ?></span>
                        </div>

                        <div class="margin-top-30"></div>

                        <div>
                            <span style="display:inline-block;width:130px;">No. Polisi</span>
                            <span style="display:inline-block;">: <?= $model['pesanan']->id_mobil ? $model['pesanan']->mobil->nomor_polisi : '(kosong)' ?></span>
                        </div>
                        <div class="margin-top-5"></div>
                        <div>
                            <span style="display:inline-block;width:130px;">Tipe Kendaraan</span>
                            <span style="display:inline-block;">: <?= $model['pesanan']->id_mobil ? $model['pesanan']->mobil->merk . ' - ' . $model['pesanan']->mobil->merk  : '(kosong)' ?></span>
                        </div>
                    </div>
                </td>
                <td class="padding-top-0">
                    <div class="border-azure padding-15">
                        <h4 class="text-azure margin-top-0">Detail Pemakaian</h4>

                        <div>
                            <span style="display:inline-block;width:130px;">Odometer Awal</span>
                            <span style="display:inline-block;">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KM</span>
                        </div>
                        <div class="margin-top-5"></div>
                        <div>
                            <span style="display:inline-block;width:130px;">Odometer Akhir</span>
                            <span style="display:inline-block;">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KM</span>
                        </div>
                    </div>

                    <div class="margin-top-30"></div>

                    <div class="border-azure padding-15">
                        <h4 class="text-azure margin-top-0">Detail Biaya</h4>

                        <div>
                            <span style="display:inline-block;width:130px;">BBM</span>
                            <span style="display:inline-block;">:</span>
                        </div>
                        <div class="margin-top-5"></div>
                        <div>
                            <span style="display:inline-block;width:130px;">Tol</span>
                            <span style="display:inline-block;">:</span>
                        </div>
                        <div class="margin-top-5"></div>
                        <div>
                            <span style="display:inline-block;width:130px;">Parkir</span>
                            <span style="display:inline-block;">:</span>
                        </div>
                        <div class="margin-top-5"></div>
                        <div>
                            <span style="display:inline-block;width:130px;"><b>Total</b></span>
                            <span style="display:inline-block;">:</span>
                        </div>
                    </div>

                    <div class="margin-top-15"></div>

                    <table class="table table-no-line margin-0">
                        <tbody>
                            <tr class="text-center">
                                <td>
                                    <div>Dilaksanakan oleh,</div>
                                    <div class="margin-top-60"></div>
                                    <div><?= $model['pesanan']->id_supir ? $model['pesanan']->supir->name : '(kosong)' ?></div>
                                    <hr class="margin-0">
                                    <div>Supir</div>
                                </td>
                                <td>
                                    <div>Diperiksa oleh,</div>
                                    <div class="margin-top-60"></div>
                                    <div>Hasya</div>
                                    <hr class="margin-0">
                                    <div>User</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>
