<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/admin_pool/dashboard/form-realisasi.js', ['depends' => 'technosmart\assets_manager\VueAsset']);
$this->registerJs(
    'vm.pesanan.anggaran = ' . ($model['pesanan']->anggaran != null ? json_encode((int)$model['pesanan']->anggaran) : 'null') . ';' .
    'vm.pesanan.bbm = ' . ($model['pesanan']->bbm != null ? json_encode((int)$model['pesanan']->bbm) : 'null') . ';' .
    'vm.pesanan.tol = ' . ($model['pesanan']->tol != null ? json_encode((int)$model['pesanan']->tol) : 'null') . ';' .
    'vm.pesanan.parkir = ' . ($model['pesanan']->parkir != null ? json_encode((int)$model['pesanan']->parkir) : 'null') . ';' .
    'vm.pesanan.revisi_bbm = ' . ($model['pesanan']->revisi_bbm != null ? json_encode((int)$model['pesanan']->revisi_bbm) : 'null') . ';' .
    'vm.pesanan.revisi_tol = ' . ($model['pesanan']->revisi_tol != null ? json_encode((int)$model['pesanan']->revisi_tol) : 'null') . ';' .
    'vm.pesanan.revisi_parkir = ' . ($model['pesanan']->revisi_parkir != null ? json_encode((int)$model['pesanan']->revisi_parkir) : 'null') . ';' .
    '',
    3
);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Realisasi #<?= $model['pesanan']->id ?></h2>
<?php endif; ?>
        <div>
            <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>

            <div class="padding-15 border-light-azure bg-light-azure margin-bottom-15">Pengajuan penambahan anggaran telah disetujui Supervisor</div>
            
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Total Anggaran</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        {{pesanan.anggaran | rupiah}}
                    </div>
                </div>
            </div>
            <div class="form-group form-group-sm box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Sisa Realisasi</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <span v-if="sisa_realisasi < 0" class="text-red">{{sisa_realisasi | rupiah}}</span>
                    <span v-else class="text-azure">{{sisa_realisasi | rupiah}}</span>
                </div>
            </div>

            <?= $form->field($model['pesanan'], 'revisi_bbm', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'revisi_bbm', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->revisi_bbm ?>
                    </div>
                </div>
            <?= $form->field($model['pesanan'], 'revisi_bbm')->end(); ?>

            <?= $form->field($model['pesanan'], 'revisi_tol', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'revisi_tol', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->revisi_tol ?>
                    </div>
                </div>
            <?= $form->field($model['pesanan'], 'revisi_tol')->end(); ?>

            <?= $form->field($model['pesanan'], 'revisi_parkir', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'revisi_parkir', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->revisi_parkir ?>
                    </div>
                </div>
            <?= $form->field($model['pesanan'], 'revisi_parkir')->end(); ?>
            
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    
                </div>
                <div class="box-10 m-padding-x-0">
                    <div v-if="sisa_realisasi < 0" class="padding-15 border-light-red bg-light-red margin-bottom-15">
                        Realisasi yang melebihi anggaran memerlukan persetujuan supervisor sebelum SPKnya bisa di akhiri
                    </div>
                    <?= Html::submitButton('Mohon Persetujuan ke Supervisor', ['v-if' => 'sisa_realisasi < 0', 'class' => 'button button-sm bg-red border-red hover-bg-light-red']) ?>
                    <?= Html::submitButton('Akhiri SPK', ['v-if' => 'sisa_realisasi >= 0', 'class' => 'button button-sm bg-azure border-azure hover-bg-light-azure']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
