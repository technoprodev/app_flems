<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Tambah Anggaran</h2>
<?php endif; ?>
        <div>
            <div class="form-group form-group-sm box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Addition</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <input type="text" name="" class="form-text">
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    
                </div>
                <div class="box-10 m-padding-x-0">
                    <a href="" class="button button-sm bg-azure border-azure hover-bg-light-azure">Submit</a>
                </div>
            </div>
        </div>
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
