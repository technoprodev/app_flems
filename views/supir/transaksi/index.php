<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/supir/transaksi/list-index-transaksi_supir.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);

$lastModel = \app_flems\models\TransaksiSupir::find()->where(['id_supir' => Yii::$app->user->identity->id])->orderBy(['id' => SORT_DESC])->limit(1)->asArray()->one();
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Riwayat Transaksi Supir</h2>
        <h6>Saldo Mengendap Anda adalah: Rp <b class="text-azure"><?= number_format($lastModel ? $lastModel['saldo'] : 0, 2) ?></b></h6>
        <table id="table-transaksi_supir" class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Waktu</th>
                    <th>Keterangan</th>
                    <th>SPK</th>
                    <th>Debit</th>
                    <th>Kredit</th>
                    <th>Saldo</th>
                </tr>
            </thead>
        </table>
    </div>
</div>