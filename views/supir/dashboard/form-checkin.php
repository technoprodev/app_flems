<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\ChoicesAsset::register($this);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Checkin #<?= $model['pesanan']->id ?></h2>
<?php endif; ?>
        <div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Tujuan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->tujuan ? $model['pesanan']->tujuan : '(kosong)' ?>
                    </div>
                    
                    <?php foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan) : ?>
                    
                    <div class="padding-y-5">
                        <?= $pesananTujuan->id ? $pesananTujuan->tujuan : '(kosong)' ?>
                    </div>
                    
                    <?php endforeach; ?>

                    <?php foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan) : ?>
                    
                    <div class="padding-y-5">
                        <?= $pesananTujuan->id ? $pesananTujuan->tujuan : '(kosong)' ?>
                    </div>
                    
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Penumpang</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->penumpang->name ?>
                    </div>
                    
                    <?php foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang) : ?>
                    
                    <div class="padding-y-5">
                        <?= $pesananPenumpang->id ? $pesananPenumpang->name : '(kosong)' ?>
                    </div>
                    
                    <?php endforeach; ?>
                </div>
            </div>

            <hr>

            <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>

            <?= $form->field($model['pesanan'], 'virtual_otp_checkin', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'virtual_otp_checkin', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::activeTextInput($model['pesanan'], 'virtual_otp_checkin', ['class' => 'form-text']) ?>
                    <?= Html::error($model['pesanan'], 'virtual_otp_checkin', ['class' => 'form-info']); ?>
                </div>
            <?= $form->field($model['pesanan'], 'virtual_otp_checkin')->end(); ?>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::submitButton('Submit', ['class' => 'button button-sm bg-azure border-azure hover-bg-light-azure']) ?>
                    <?= Html::resetButton('Reset', ['class' => 'button button-sm bg-lightest text-azure border-azure hover-bg-light-azure']); ?>
                </div>
            </div>
            
            <?php ActiveForm::end(); ?>
        </div>
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
