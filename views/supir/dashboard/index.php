<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/supir/dashboard/index-pesanan-aktif.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/supir/dashboard/index-pesanan-baru.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>
<div class="box box-break-md box-space-md box-gutter">
    <div class="box-6 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Pesanan Baru</h2>

        <table class="datatables-pesanan-baru table table-no-line">
        </table>

        <?php if (false && $newPesanans) : ?>
            <?php foreach ($newPesanans as $key => $newPesanan) : ?>
                <div class="border rounded-xs padding-20 margin-bottom-15">
                    <h5 class="margin-top-0">
                        <span class="text-azure margin-right-10">#<?= $newPesanan->id ?></span>
                        <span class="fs-16 f-normal"><?= $newPesanan->tipe ?> - </span>
                        <span class="fs-16 f-bold">Menunggu Konfirmasi Anda</span>
                    </h5>

                    <div class="box box-break-sm">
                        <div class="box-12 padding-0 margin-bottom-10">
                            <div class="">
                                <span class="f-bold text-azure"><?= $newPesanan->penumpang->name ?></span>
                                -
                                <?= $newPesanan->tujuan ? $newPesanan->tujuan : '(kosong)' ?>
                            </div>
                        </div>

                        <div class="box-6 padding-0 margin-bottom-10">
                            <div class=""><code class="margin-0 bg-light-spring border-light-spring">Anggaran:</code> <?= $newPesanan->anggaran ? number_format($newPesanan->anggaran, 2) : '(kosong)' ?></div>
                            <div class=""><code class="margin-0 bg-light-spring border-light-spring">SPJ     :</code> <?= $newPesanan->spj ? 'Ya' : 'Tidak' ?></div>
                        </div>
                    
                        <div class="box-6 padding-0 margin-bottom-5">
                            <div class=""><code class="margin-0 bg-light-red border-light-red">Start:</code> <?= DateTime::createFromFormat('d/m/Y H:i:s', $newPesanan->waktu_keberangkatan)->format("d M, h:i") ?></div>
                            <div class=""><code class="margin-0 bg-light-red border-light-red">End  :</code> <?= DateTime::createFromFormat('d/m/Y H:i:s', $newPesanan->waktu_kembali)->format("d M, h:i") ?></div>
                        </div>

                        <div class="box-12 padding-0">
                            <a href="<?= Yii::$app->urlManager->createUrl(['supir/dashboard/konfirmasi', 'id' => $newPesanan->id]) ?>" class="button button-sm bg-azure border-azure hover-bg-light-azure" data-confirm="Anda yakin ingin mengkonfirmasi pesanan ini ?" data-method="post">Confirm</a>
                        </div>

                    </div>
                </div>
            <?php endforeach; ?>
        <?php elseif (false) : ?>
            <div class="text-red">Belum ada order baru di akun Anda</div>
        <?php endif; ?>
    </div>
    <div class="box-6 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Pesanan Aktif</h2>

        <table class="datatables-pesanan-aktif table table-no-line">
        </table>

        <?php if (false && $activePesanans) : ?>
            <?php foreach ($activePesanans as $key => $activePesanan) : ?>
                <div class="border rounded-xs padding-20 margin-bottom-15">
                    <h5 class="margin-top-0">
                        <span class="text-azure margin-right-10">#<?= $activePesanan->id ?></span>
                        <span class="fs-16 f-normal"><?= $activePesanan->tipe ?> - </span>
                        <span class="fs-16 f-bold">
                            <?php if ($activePesanan->status == 'Disetujui Supervisor') : ?>
                                SPK Belum di Cetak
                            <?php elseif($activePesanan->status == 'SPK Telah Siap') : ?>
                                Perjalanan belum Dimulai
                            <?php elseif($activePesanan->status == 'Checkin') : ?>
                                On The Way
                            <?php elseif($activePesanan->status == 'Checkout') : ?>
                                Checkout & Belum Realisasi
                            <?php endif; ?>
                        </span>
                    </h5>

                    <div class="box box-break-sm">
                        <div class="box-12 padding-0 margin-bottom-10">
                            <div class="">
                                <span class="f-bold text-azure"><?= $activePesanan->penumpang->name ?></span>
                                -
                                <?= $activePesanan->tujuan ? $activePesanan->tujuan : '(kosong)' ?>
                            </div>
                        </div>

                        <div class="box-6 padding-0 margin-bottom-10">
                            <div class=""><code class="margin-0 bg-light-spring border-light-spring">Anggaran:</code> <?= $activePesanan->anggaran ? number_format($activePesanan->anggaran, 2) : '(kosong)' ?></div>
                            <div class=""><code class="margin-0 bg-light-spring border-light-spring">SPJ     :</code> <?= $activePesanan->spj ? 'Ya' : 'Tidak' ?></div>
                        </div>
                    
                        <div class="box-6 padding-0 margin-bottom-5">
                            <div class=""><code class="margin-0 bg-light-red border-light-red">Start:</code> <?= DateTime::createFromFormat('d/m/Y H:i:s', $activePesanan->waktu_keberangkatan)->format("d M, h:i") ?></div>
                            <div class=""><code class="margin-0 bg-light-red border-light-red">End  :</code> <?= DateTime::createFromFormat('d/m/Y H:i:s', $activePesanan->waktu_kembali)->format("d M, h:i") ?></div>
                        </div>

                        <div class="box-12 padding-0">
                            <?php if ($activePesanan->status == 'Disetujui Supervisor') : ?>
                                <a class="button button-sm border-light-red text-red f-bold hover-text-red">Belum bisa checkin karena SPK belum di cetak</a>
                            <?php elseif($activePesanan->status == 'SPK Telah Siap') : ?>
                                <a href="<?= Yii::$app->urlManager->createUrl(['supir/dashboard/form-checkin', 'id' => $activePesanan->id]) ?>" class="button button-sm bg-azure border-azure hover-bg-light-azure" modal-md="" modal-title="Checkin #<?= $activePesanan->id ?>">Checkin</a>
                            <?php elseif($activePesanan->status == 'Checkin') : ?>
                                <a href="<?= Yii::$app->urlManager->createUrl(['supir/dashboard/form-checkout', 'id' => $activePesanan->id]) ?>" class="button button-sm bg-azure border-azure hover-bg-light-azure" modal-md="" modal-title="Checkin #<?= $activePesanan->id ?>">Checkout</a>
                            <?php elseif($activePesanan->status == 'Checkout') : ?>
                                <a class="button button-sm border-light-red text-red f-bold hover-text-red">Harap melakukan realisasi SPK ke Admin</a>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>
            <?php endforeach; ?>
        <?php elseif (false) : ?>
            <div class="text-red">Belum ada order yang aktif di akun Anda</div>
        <?php endif; ?>
    </div>
</div>