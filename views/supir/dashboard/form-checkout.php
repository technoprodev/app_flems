<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\ChoicesAsset::register($this);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Checkout #<?= $model['pesanan']->id ?></h2>
<?php endif; ?>
        <div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Waktu Checkin</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->waktu_checkin ? $model['pesanan']->waktu_checkin : '(kosong)' ?>
                    </div>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Tujuan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->tujuan ? $model['pesanan']->tujuan : '(kosong)' ?>
                    </div>
                    
                    <?php foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan) : ?>
                    
                    <div class="padding-y-5">
                        <?= $pesananTujuan->id ? $pesananTujuan->tujuan : '(kosong)' ?>
                    </div>
                    
                    <?php endforeach; ?>

                    <?php foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan) : ?>
                    
                    <div class="padding-y-5">
                        <?= $pesananTujuan->id ? $pesananTujuan->tujuan : '(kosong)' ?>
                    </div>
                    
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Penumpang</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        <?= $model['pesanan']->id_penumpang ? $model['pesanan']->penumpang->name : '(kosong)' ?>
                    </div>
                    
                    <?php foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang) : ?>
                    
                    <div class="padding-y-5">
                        <?= $pesananPenumpang->id ? $pesananPenumpang->name : '(kosong)' ?>
                    </div>
                    
                    <?php endforeach; ?>
                </div>
            </div>

            <hr>

            <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>

            <?= $form->field($model['pesanan'], 'virtual_otp_checkout', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'virtual_otp_checkout', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::activeTextInput($model['pesanan'], 'virtual_otp_checkout', ['class' => 'form-text']) ?>
                    <?= Html::error($model['pesanan'], 'virtual_otp_checkout', ['class' => 'form-info']); ?>
                </div>
            <?= $form->field($model['pesanan'], 'virtual_otp_checkout')->end(); ?>

            <?= $form->field($model['pesanan'], 'bbm', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'bbm', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::activeTextInput($model['pesanan'], 'bbm', ['class' => 'form-text']) ?>
                    <?= Html::error($model['pesanan'], 'bbm', ['class' => 'form-info']); ?>
                </div>
            <?= $form->field($model['pesanan'], 'bbm')->end(); ?>

            <?= $form->field($model['pesanan'], 'nominal_voucher_bbm', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'nominal_voucher_bbm', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::activeTextInput($model['pesanan'], 'nominal_voucher_bbm', ['class' => 'form-text']) ?>
                    <?= Html::error($model['pesanan'], 'nominal_voucher_bbm', ['class' => 'form-info']); ?>
                </div>
            <?= $form->field($model['pesanan'], 'nominal_voucher_bbm')->end(); ?>

            <?= $form->field($model['pesanan'], 'nomor_voucher_bbm', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'nomor_voucher_bbm', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::activeTextInput($model['pesanan'], 'nomor_voucher_bbm', ['class' => 'form-text']) ?>
                    <?= Html::error($model['pesanan'], 'nomor_voucher_bbm', ['class' => 'form-info']); ?>
                </div>
            <?= $form->field($model['pesanan'], 'nomor_voucher_bbm')->end(); ?>

            <?= $form->field($model['pesanan'], 'tol', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'tol', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::activeTextInput($model['pesanan'], 'tol', ['class' => 'form-text']) ?>
                    <?= Html::error($model['pesanan'], 'tol', ['class' => 'form-info']); ?>
                </div>
            <?= $form->field($model['pesanan'], 'tol')->end(); ?>

            <?= $form->field($model['pesanan'], 'parkir', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'parkir', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::activeTextInput($model['pesanan'], 'parkir', ['class' => 'form-text']) ?>
                    <?= Html::error($model['pesanan'], 'parkir', ['class' => 'form-info']); ?>
                </div>
            <?= $form->field($model['pesanan'], 'parkir')->end(); ?>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::submitButton('Submit', ['class' => 'button button-sm bg-azure border-azure hover-bg-light-azure']) ?>
                    <?= Html::resetButton('Reset', ['class' => 'button button-sm bg-lightest text-azure border-azure hover-bg-light-azure']); ?>
                </div>
            </div>
            
            <?php ActiveForm::end(); ?>
        </div>
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
