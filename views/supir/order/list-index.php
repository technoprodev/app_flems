<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/employee/order/list-index-completed.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/employee/order/list-index-rejected.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Pesanan</h2>
        <ul class="nav nav-tabs f-bold">
            <li class="active"><a href="#tab-completed" data-toggle="tab">Completed</a></li>
            <li class="activ3"><a href="#tab-rejected" data-toggle="tab">Rejected</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane padding-20 fade active in" id="tab-completed">
                <table id="table-completed" class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Status</th>
                            <th>Penumpang</th>
                            <th>Tipe</th>
                            <th>Tujuan</th>
                        </tr>
                    </thead>
                    <!-- <tbody>
                        <tr>
                            <td>
                                <a href="<?= Yii::$app->urlManager->createUrl("employee/order/detail-completed") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td><b>admin pradana</b></td>
                            <td>Reguler</td>
                            <td>Kantor Pusat, Jakarta Utara, DKI Jakarta</td>
                        </tr>
                    </tbody> -->
                </table>
            </div>
            <div class="tab-pane padding-20 fade" id="tab-rejected">
                <table id="table-rejected" class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Status</th>
                            <th>Penumpang</th>
                            <th>Tipe</th>
                            <th>Tujuan</th>
                        </tr>
                    </thead>
                    <!-- <tbody>
                        <tr>
                            <td>
                                <a href="<?= Yii::$app->urlManager->createUrl("employee/order/detail-rejected") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td><b>admin pradana</b></td>
                            <td>Reguler</td>
                            <td>Kantor Pusat, Jakarta Utara, DKI Jakarta</td>
                        </tr>
                    </tbody> -->
                </table>
            </div>
            <div class="tab-pane padding-20 fade" id="tab-closed">
                <table id="rejected" class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>no order</th>
                            <th>Penumpang</th>
                            <th>Supir</th>
                            <th>Waktu Keberangkatan</th>
                            <th>Waktu Kembali</th>
                            <th>Anggaran</th>
                            <th>Tipe</th>
                        </tr>
                    </thead>
                    <!-- <tbody>
                        <tr>
                            <td>
                                <a href="<?= Yii::$app->urlManager->createUrl("employee/order/detail-rejected") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td>29338</td>
                            <td><b>Bianca pradana</b></td>
                            <td>Bowo Nugraha</td>
                            <td>03/02/2018</td>
                            <td>06/02/2018</td>
                            <td>200.000</td>
                            <td>Luar Kota</td>
                        </tr>
                    </tbody> -->
                </table>
            </div>
        </div>
    </div>
</div>