<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/manager/dashboard/list-index-butuh_persetujuan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/manager/dashboard/list-index-dalam_proses.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/manager/dashboard/list-index-selesai.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Permintaan Kendaraan</h2>
        <ul class="nav nav-tabs f-bold">
            <li class="active"><a href="#tab-butuh_persetujuan" data-toggle="tab">Butuh Persetujuan</a></li>
            <li class="activ3"><a href="#tab-dalam_proses" data-toggle="tab">Dalam Proses</a></li>
            <li class="activ3"><a href="#tab-selesai" data-toggle="tab">Selesai</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane padding-20 fade active in" id="tab-butuh_persetujuan">
                <table id="table-butuh_persetujuan" class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Pemesan</th>
                            <!-- <th>Supir</th> -->
                            <th>Waktu Keberangkatan</th>
                            <th>Waktu Kembali</th>
                            <!-- <th>Anggaran</th> -->
                            <!-- <th>Tipe Perjalanan</th> -->
                        </tr>
                    </thead>
                    <!-- <tbody>
                        <tr>
                            <td>
                                <a href="" class="text-azure margin-right-5" data-confirm="Are you sure you want to approve this order?" data-method="post">Approve</a>
                                <a href="" class="text-rose margin-right-5" data-confirm="Are you sure you want to reject this order?" data-method="post">Reject</a>
                                <a href="<?= Yii::$app->urlManager->createUrl("manager/dashboard/detail-butuh-persetujuan") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td><b>admin pradana</b></td>
                            <td>Reguler</td>
                            <td>Kantor Pusat, Jakarta Utara, DKI Jakarta</td>
                        </tr>
                    </tbody> -->
                </table>
            </div>
            <div class="tab-pane padding-20 fade" id="tab-dalam_proses">
                <table id="table-dalam_proses" class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Status</th>
                            <th>Penumpang</th>
                            <th>Supir</th>
                            <th>Waktu Keberangkatan</th>
                            <th>Waktu Kembali</th>
                            <th>Anggaran</th>
                            <th>Tipe Perjalanan</th>
                        </tr>
                    </thead>
                    <!-- <tbody>
                        <tr>
                            <td>
                                <a href="<?= Yii::$app->urlManager->createUrl("manager/dashboard/detail-dalam_proses") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td><b>admin pradana</b></td>
                            <td>Reguler</td>
                            <td>Kantor Pusat, Jakarta Utara, DKI Jakarta</td>
                        </tr>
                    </tbody> -->
                </table>
            </div>
            <div class="tab-pane padding-20 fade" id="tab-selesai">
                <table id="table-selesai" class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Status</th>
                            <th>Penumpang</th>
                            <th>Supir</th>
                            <th>Waktu Keberangkatan</th>
                            <th>Waktu Kembali</th>
                            <th>Anggaran</th>
                        </tr>
                    </thead>
                    <!-- <tbody>
                        <tr>
                            <td>
                                <a href="<?= Yii::$app->urlManager->createUrl("manager/dashboard/detail-selesai") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td><b>admin pradana</b></td>
                            <td>Reguler</td>
                            <td>Kantor Pusat, Jakarta Utara, DKI Jakarta</td>
                        </tr>
                    </tbody> -->
                </table>
            </div>
        </div>
    </div>
</div>