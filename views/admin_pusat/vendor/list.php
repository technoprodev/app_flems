<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/admin_pusat/vendor/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<table class="datatables table table-striped table-hover table-nowrap">
    <thead>
        <tr class="text-dark">
            <th>Action</th>
            <th>Vendor</th>
            <th>Alamat</th>
            <th>Bidang Bisnis</th>
            <th>Nomor Pks</th>
            <th>Waktu Mulai Pks</th>
            <th>Waktu Selesai Pks</th>
            <th>Nomor Telpon</th>
            <th>Periode Bayar</th>
            <th>Pic1 Nama</th>
            <th>Pic1 Telpon</th>
            <th>Pic2 Nama</th>
            <th>Pic2 Telpon</th>
            <th>Pic3 Nama</th>
            <th>Pic3 Telpon</th>
            <th>Created At</th>
            <th>Updated At</th>
        </tr>
        <tr class="dt-search">
            <th></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search vendor"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search alamat"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search bidang bisnis"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search nomor pks"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search waktu mulai pks"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search waktu selesai pks"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search nomor telpon"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search periode bayar"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search pic1 nama"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search pic1 telpon"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search pic2 nama"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search pic2 telpon"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search pic3 nama"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search pic3 telpon"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search created at"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search updated at"/></th>
        </tr>
    </thead>
</table>

<?php if ($this->context->can('vendor')): ?>
    <hr class="margin-y-15">
    <div>
        <?= Html::a('Tambah Vendor Baru', ['create'], ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
    </div>
<?php endif; ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
