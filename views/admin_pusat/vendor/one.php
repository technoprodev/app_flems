<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['vendor'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->vendor ? $model['vendor']->vendor : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['alamat'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->alamat ? $model['vendor']->alamat : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['bidang_bisnis'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->bidang_bisnis ? $model['vendor']->bidang_bisnis : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['nomor_pks'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->nomor_pks ? $model['vendor']->nomor_pks : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['waktu_mulai_pks'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->waktu_mulai_pks ? $model['vendor']->waktu_mulai_pks : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['waktu_selesai_pks'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->waktu_selesai_pks ? $model['vendor']->waktu_selesai_pks : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['nomor_telpon'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->nomor_telpon ? $model['vendor']->nomor_telpon : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['periode_bayar'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->periode_bayar ? $model['vendor']->periode_bayar : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['pic1_nama'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->pic1_nama ? $model['vendor']->pic1_nama : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['pic1_telpon'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->pic1_telpon ? $model['vendor']->pic1_telpon : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['pic2_nama'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->pic2_nama ? $model['vendor']->pic2_nama : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['pic2_telpon'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->pic2_telpon ? $model['vendor']->pic2_telpon : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['pic3_nama'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->pic3_nama ? $model['vendor']->pic3_nama : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['pic3_telpon'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->pic3_telpon ? $model['vendor']->pic3_telpon : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['created_at'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->created_at ? $model['vendor']->created_at : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['vendor']->attributeLabels()['updated_at'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['vendor']->updated_at ? $model['vendor']->updated_at : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Update', ['update', 'id' => $model['vendor']->id], ['class' => 'button bg-azure border-azure hover-bg-light-azure']) ?>&nbsp;
            <?= Html::a('Delete', ['delete', 'id' => $model['vendor']->id], [
                'class' => 'button text-azure border-azure hover-bg-light-azure',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item ?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Back to list', ['index'], ['class' => 'button text-azure border-azure hover-bg-azure pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>