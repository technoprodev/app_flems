<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['vendor']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['vendor'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['vendor'], 'vendor', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'vendor', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'vendor', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['vendor'], 'vendor', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'vendor')->end(); ?>

    <?= $form->field($model['vendor'], 'alamat', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'alamat', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'alamat', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['vendor'], 'alamat', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'alamat')->end(); ?>

    <?= $form->field($model['vendor'], 'bidang_bisnis', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'bidang_bisnis', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'bidang_bisnis', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['vendor'], 'bidang_bisnis', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'bidang_bisnis')->end(); ?>

    <?= $form->field($model['vendor'], 'nomor_pks', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'nomor_pks', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'nomor_pks', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['vendor'], 'nomor_pks', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'nomor_pks')->end(); ?>

    <?= $form->field($model['vendor'], 'waktu_mulai_pks', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'waktu_mulai_pks', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'waktu_mulai_pks', ['class' => 'form-text']) ?>
        <?= Html::error($model['vendor'], 'waktu_mulai_pks', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'waktu_mulai_pks')->end(); ?>

    <?= $form->field($model['vendor'], 'waktu_selesai_pks', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'waktu_selesai_pks', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'waktu_selesai_pks', ['class' => 'form-text']) ?>
        <?= Html::error($model['vendor'], 'waktu_selesai_pks', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'waktu_selesai_pks')->end(); ?>

    <?= $form->field($model['vendor'], 'nomor_telpon', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'nomor_telpon', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'nomor_telpon', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['vendor'], 'nomor_telpon', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'nomor_telpon')->end(); ?>

    <?= $form->field($model['vendor'], 'periode_bayar', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'periode_bayar', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'periode_bayar', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['vendor'], 'periode_bayar', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'periode_bayar')->end(); ?>

    <?= $form->field($model['vendor'], 'pic1_nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'pic1_nama', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'pic1_nama', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['vendor'], 'pic1_nama', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'pic1_nama')->end(); ?>

    <?= $form->field($model['vendor'], 'pic1_telpon', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'pic1_telpon', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'pic1_telpon', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['vendor'], 'pic1_telpon', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'pic1_telpon')->end(); ?>

    <?= $form->field($model['vendor'], 'pic2_nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'pic2_nama', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'pic2_nama', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['vendor'], 'pic2_nama', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'pic2_nama')->end(); ?>

    <?= $form->field($model['vendor'], 'pic2_telpon', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'pic2_telpon', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'pic2_telpon', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['vendor'], 'pic2_telpon', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'pic2_telpon')->end(); ?>

    <?= $form->field($model['vendor'], 'pic3_nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'pic3_nama', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'pic3_nama', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['vendor'], 'pic3_nama', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'pic3_nama')->end(); ?>

    <?= $form->field($model['vendor'], 'pic3_telpon', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['vendor'], 'pic3_telpon', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['vendor'], 'pic3_telpon', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['vendor'], 'pic3_telpon', ['class' => 'form-info']); ?>
    <?= $form->field($model['vendor'], 'pic3_telpon')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['vendor']->isNewRecord ? 'Create' : 'Update', ['class' => 'button bg-azure border-azure hover-bg-light-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'button text-azure border-azure hover-bg-light-azure']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'button text-azure border-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>