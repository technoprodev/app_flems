<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['supir']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['supir'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="box box-break-sm margin-bottom-15">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user']->attributeLabels()['username'] ?></div>
        <div class="box-10 m-padding-x-0 text-dark"><?= $model['user']->username ? $model['user']->username : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-15">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user']->attributeLabels()['name'] ?></div>
        <div class="box-10 m-padding-x-0 text-dark"><?= $model['user']->name ? $model['user']->name : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-15">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user']->attributeLabels()['email'] ?></div>
        <div class="box-10 m-padding-x-0 text-dark"><?= $model['user']->email ? $model['user']->email : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>

    <div class="box box-break-sm margin-bottom-15">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user']->attributeLabels()['phone'] ?></div>
        <div class="box-10 m-padding-x-0 text-dark"><?= $model['user']->phone ? $model['user']->phone : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-15">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['nik'] ?></div>
        <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->nik ? $model['supir']->nik : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-15">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['tipe'] ?></div>
        <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->tipe ? $model['supir']->tipe : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-15">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['id_pool'] ?></div>
        <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->id_pool ? $model['supir']->pool->unit_kerja : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-15">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['id_vendor'] ?></div>
        <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->id_vendor ? $model['supir']->vendor->vendor : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>

    <?= $form->field($model['supir'], 'id_mobil', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['supir'], 'id_mobil', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['supir'], 'id_mobil', ArrayHelper::map(\app_flems\models\Mobil::find()->indexBy('id')->asArray()->all(), 'id', 'nomor_polisi'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown']); ?>
        <?= Html::error($model['supir'], 'id_mobil', ['class' => 'form-info']); ?>
    <?= $form->field($model['supir'], 'id_mobil')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['supir']->isNewRecord ? 'Create' : 'Update', ['class' => 'button bg-azure border-azure hover-bg-light-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'button text-azure border-azure hover-bg-light-azure']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'button text-azure border-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>