<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/admin_pusat/pairing/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<table class="datatables table table-striped table-hover table-nowrap">
    <thead>
        <tr>
            <th></th>
            <th>Nama</th>
            <th>Mobil</th>
            <th>Username</th>
            <th>Email</th>
            <th>Handphone</th>
            <th>Nik</th>
            <th>Tipe</th>
            <th>Pool</th>
            <th>Vendor</th>
        </tr>
        <tr class="dt-search">
            <th></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search nama"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search mobil"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search Username"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search Email"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search Handphone"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search Nik"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search tipe"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search pool"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search vendor"/></th>
        </tr>
    </thead>
</table>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
