<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user']->attributeLabels()['username'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['user']->username ? $model['user']->username : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user']->attributeLabels()['name'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['user']->name ? $model['user']->name : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user']->attributeLabels()['email'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['user']->email ? $model['user']->email : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>

<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user']->attributeLabels()['phone'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['user']->phone ? $model['user']->phone : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['nik'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->nik ? $model['supir']->nik : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['tipe'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->tipe ? $model['supir']->tipe : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['id_mobil'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->id_mobil ? $model['supir']->mobil->nomor_polisi : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['id_sub_unit_kerja'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->id_sub_unit_kerja ? $model['supir']->subUnitKerja->unit_kerja : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['id_vendor'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->id_vendor ? $model['supir']->vendor->vendor : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['nomor_ktp'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->nomor_ktp ? $model['supir']->nomor_ktp : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['tipe_sim'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->tipe_sim ? $model['supir']->tipe_sim : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['nomor_sim'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->nomor_sim ? $model['supir']->nomor_sim : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['alamat'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->alamat ? $model['supir']->alamat : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['tempat_lahir'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->tempat_lahir ? $model['supir']->tempat_lahir : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['tanggal_lahir'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->tanggal_lahir ? $model['supir']->tanggal_lahir : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['jenis_kelamin'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->jenis_kelamin ? $model['supir']->jenis_kelamin : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['pendidikan_terakhir'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->pendidikan_terakhir ? $model['supir']->pendidikan_terakhir : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['golongan_darah'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->golongan_darah ? $model['supir']->golongan_darah : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['status'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->status ? $model['supir']->status : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['agama'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->agama ? $model['supir']->agama : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['kewarganegaraan'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->kewarganegaraan ? $model['supir']->kewarganegaraan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['pelatihan'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->pelatihan ? $model['supir']->pelatihan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['nomor_kontrak'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->nomor_kontrak ? $model['supir']->nomor_kontrak : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['sanksi_keterangan'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->sanksi_keterangan ? $model['supir']->sanksi_keterangan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['sanksi_sp1'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->sanksi_sp1 ? $model['supir']->sanksi_sp1 : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['sanksi_sp2'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->sanksi_sp2 ? $model['supir']->sanksi_sp2 : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['sanksi_sp3'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->sanksi_sp3 ? $model['supir']->sanksi_sp3 : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['created_at'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->created_at ? $model['supir']->created_at : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['supir']->attributeLabels()['updated_at'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['supir']->updated_at ? $model['supir']->updated_at : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Update', ['update', 'id' => $model['supir']->id_user], ['class' => 'button bg-azure border-azure hover-bg-light-azure']) ?>&nbsp;
            <?= Html::a('Delete', ['delete', 'id' => $model['supir']->id_user], [
                'class' => 'button text-azure border-azure hover-bg-light-azure',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item ?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Back to list', ['index'], ['class' => 'button text-azure border-azure hover-bg-azure pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>