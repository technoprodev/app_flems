<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<div class="box box-break-sm box-space-md">
	<div class="box-2 bg-azure padding-15 rounded-sm text-center">
		<div>
			<i class="fa fa-user fs-30 circle-icon border-azure bg-lightest text-azure"></i>
		</div>
		<div class="margin-top-15"></div>
        <?= Html::a('Kelola Karyawan', ['admin_pusat/karyawan/index'], ['class' => 'button bg-lightest border-lightest rounded-sm inline-block text-wrap']) ?>
	</div>
	<div class="box-2 bg-cyan padding-15 rounded-sm text-center">
        <div>
			<i class="fa fa-user-secret fs-30 circle-icon border-cyan bg-lightest text-cyan"></i>
		</div>
		<div class="margin-top-15"></div>
		<?= Html::a('Kelola Supir', ['admin_pusat/supir/index'], ['class' => 'button bg-lightest border-lightest rounded-sm inline-block text-wrap']) ?>
	</div>
	<div class="box-2 bg-orange padding-15 rounded-sm text-center">
        <div>
			<i class="fa fa-building fs-30 circle-icon border-orange bg-lightest text-orange"></i>
		</div>
		<div class="margin-top-15"></div>
		<?= Html::a('Kelola Unit Kerja', ['admin_pusat/unit-kerja/index'], ['class' => 'button bg-lightest border-lightest rounded-sm inline-block text-wrap']) ?>
	</div>
	<div class="box-2 bg-magenta padding-15 rounded-sm text-center">
        <div>
			<i class="fa fa-building-o fs-30 circle-icon border-magenta bg-lightest text-magenta"></i>
		</div>
		<div class="margin-top-15"></div>
		<?= Html::a('Sub Unit Kerja', ['admin_pusat/sub-unit-kerja/index'], ['class' => 'button bg-lightest border-lightest rounded-sm inline-block text-wrap']) ?>
	</div>
	<div class="box-2 bg-chartreuse padding-15 rounded-sm text-center">
        <div>
			<i class="fa fa-car fs-30 circle-icon border-chartreuse bg-lightest text-chartreuse"></i>
		</div>
		<div class="margin-top-15"></div>
		<?= Html::a('Kelola Mobil', ['admin_pusat/mobil/index'], ['class' => 'button bg-lightest border-lightest rounded-sm inline-block text-wrap']) ?>
	</div>
	<div class="box-2 bg-violet padding-15 rounded-sm text-center">
        <div>
			<i class="fa fa-briefcase fs-30 circle-icon border-violet bg-lightest text-violet"></i>
		</div>
		<div class="margin-top-15"></div>
		<?= Html::a('Kelola Vendor', ['admin_pusat/vendor/index'], ['class' => 'button bg-lightest border-lightest rounded-sm inline-block text-wrap']) ?>
	</div>
	
</div>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>