<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit_kerja']->attributeLabels()['id'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit_kerja']->id ? $model['unit_kerja']->id : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit_kerja']->attributeLabels()['unit_kerja'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit_kerja']->unit_kerja ? $model['unit_kerja']->unit_kerja : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit_kerja']->attributeLabels()['saldo'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit_kerja']->saldo ? $model['unit_kerja']->saldo : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit_kerja']->attributeLabels()['keberangkatan_latitude'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit_kerja']->keberangkatan_latitude ? $model['unit_kerja']->keberangkatan_latitude : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit_kerja']->attributeLabels()['keberangkatan_longitude'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit_kerja']->keberangkatan_longitude ? $model['unit_kerja']->keberangkatan_longitude : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit_kerja']->attributeLabels()['created_at'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit_kerja']->created_at ? $model['unit_kerja']->created_at : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit_kerja']->attributeLabels()['updated_at'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit_kerja']->updated_at ? $model['unit_kerja']->updated_at : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Update', ['update', 'id' => $model['unit_kerja']->id], ['class' => 'button bg-azure border-azure hover-bg-light-azure']) ?>&nbsp;
            <?= Html::a('Delete', ['delete', 'id' => $model['unit_kerja']->id], [
                'class' => 'button text-azure border-azure hover-bg-light-azure',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item ?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Back to list', ['index'], ['class' => 'button text-azure border-azure hover-bg-azure pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>