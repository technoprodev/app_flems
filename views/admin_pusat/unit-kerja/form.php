<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['unit_kerja']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['unit_kerja'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['unit_kerja'], 'unit_kerja', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['unit_kerja'], 'unit_kerja', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['unit_kerja'], 'unit_kerja', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['unit_kerja'], 'unit_kerja', ['class' => 'form-info']); ?>
    <?= $form->field($model['unit_kerja'], 'unit_kerja')->end(); ?>

    <?= $form->field($model['unit_kerja'], 'saldo', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['unit_kerja'], 'saldo', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['unit_kerja'], 'saldo', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['unit_kerja'], 'saldo', ['class' => 'form-info']); ?>
    <?= $form->field($model['unit_kerja'], 'saldo')->end(); ?>

    <?= $form->field($model['unit_kerja'], 'keberangkatan_latitude', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['unit_kerja'], 'keberangkatan_latitude', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['unit_kerja'], 'keberangkatan_latitude', ['class' => 'form-text']) ?>
        <?= Html::error($model['unit_kerja'], 'keberangkatan_latitude', ['class' => 'form-info']); ?>
    <?= $form->field($model['unit_kerja'], 'keberangkatan_latitude')->end(); ?>

    <?= $form->field($model['unit_kerja'], 'keberangkatan_longitude', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['unit_kerja'], 'keberangkatan_longitude', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['unit_kerja'], 'keberangkatan_longitude', ['class' => 'form-text']) ?>
        <?= Html::error($model['unit_kerja'], 'keberangkatan_longitude', ['class' => 'form-info']); ?>
    <?= $form->field($model['unit_kerja'], 'keberangkatan_longitude')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['unit_kerja']->isNewRecord ? 'Create' : 'Update', ['class' => 'button bg-azure border-azure hover-bg-light-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'button text-azure border-azure hover-bg-light-azure']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'button text-azure border-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>