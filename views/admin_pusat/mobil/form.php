<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['mobil']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['mobil'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['mobil'], 'nomor_polisi', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'nomor_polisi', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'nomor_polisi', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['mobil'], 'nomor_polisi', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'nomor_polisi')->end(); ?>

    <?= $form->field($model['mobil'], 'dedicate', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'dedicate', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['mobil'], 'dedicate', $model['mobil']->getEnum('dedicate'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown']); ?>
        <?= Html::error($model['mobil'], 'dedicate', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'dedicate')->end(); ?>

    <?= $form->field($model['mobil'], 'id_vendor', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'id_vendor', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['mobil'], 'id_vendor', ArrayHelper::map(\app_flems\models\Vendor::find()->indexBy('id')->asArray()->all(), 'id', 'vendor'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown']); ?>
        <?= Html::error($model['mobil'], 'id_vendor', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'id_vendor')->end(); ?>

    <?= $form->field($model['mobil'], 'tipe', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'tipe', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'tipe', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['mobil'], 'tipe', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'tipe')->end(); ?>

    <?= $form->field($model['mobil'], 'merk', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'merk', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'merk', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['mobil'], 'merk', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'merk')->end(); ?>

    <?= $form->field($model['mobil'], 'warna', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'warna', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'warna', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['mobil'], 'warna', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'warna')->end(); ?>

    <?= $form->field($model['mobil'], 'tahun', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'tahun', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'tahun', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['mobil'], 'tahun', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'tahun')->end(); ?>

    <?= $form->field($model['mobil'], 'tipe_bbm', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'tipe_bbm', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'tipe_bbm', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['mobil'], 'tipe_bbm', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'tipe_bbm')->end(); ?>

    <?= $form->field($model['mobil'], 'odometer_unit_comes', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'odometer_unit_comes', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'odometer_unit_comes', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['mobil'], 'odometer_unit_comes', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'odometer_unit_comes')->end(); ?>

    <?= $form->field($model['mobil'], 'periode_sewa', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'periode_sewa', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'periode_sewa', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['mobil'], 'periode_sewa', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'periode_sewa')->end(); ?>

    <?= $form->field($model['mobil'], 'tanggal_mulai_sewa', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'tanggal_mulai_sewa', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'tanggal_mulai_sewa', ['class' => 'form-text']) ?>
        <?= Html::error($model['mobil'], 'tanggal_mulai_sewa', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'tanggal_mulai_sewa')->end(); ?>

    <?= $form->field($model['mobil'], 'tanggal_akhir_sewa', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'tanggal_akhir_sewa', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'tanggal_akhir_sewa', ['class' => 'form-text']) ?>
        <?= Html::error($model['mobil'], 'tanggal_akhir_sewa', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'tanggal_akhir_sewa')->end(); ?>

    <?= $form->field($model['mobil'], 'harga_per_bulan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'harga_per_bulan', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'harga_per_bulan', ['class' => 'form-text']) ?>
        <?= Html::error($model['mobil'], 'harga_per_bulan', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'harga_per_bulan')->end(); ?>

    <?= $form->field($model['mobil'], 'periode_service', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'periode_service', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'periode_service', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['mobil'], 'periode_service', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'periode_service')->end(); ?>

    <?= $form->field($model['mobil'], 'tanggal_pajak', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'tanggal_pajak', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'tanggal_pajak', ['class' => 'form-text']) ?>
        <?= Html::error($model['mobil'], 'tanggal_pajak', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'tanggal_pajak')->end(); ?>

    <?= $form->field($model['mobil'], 'masa_stnk', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'masa_stnk', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'masa_stnk', ['class' => 'form-text']) ?>
        <?= Html::error($model['mobil'], 'masa_stnk', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'masa_stnk')->end(); ?>

    <?= $form->field($model['mobil'], 'nama_pemilik', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['mobil'], 'nama_pemilik', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['mobil'], 'nama_pemilik', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['mobil'], 'nama_pemilik', ['class' => 'form-info']); ?>
    <?= $form->field($model['mobil'], 'nama_pemilik')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['mobil']->isNewRecord ? 'Create' : 'Update', ['class' => 'button bg-azure border-azure hover-bg-light-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'button text-azure border-azure hover-bg-light-azure']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'button text-azure border-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>