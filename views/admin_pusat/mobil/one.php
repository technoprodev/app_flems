<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['nomor_polisi'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->nomor_polisi ? $model['mobil']->nomor_polisi : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>

<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['dedicate'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->dedicate ? $model['mobil']->dedicate : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['id_vendor'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->id_vendor ? $model['mobil']->vendor->vendor : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['tipe'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->tipe ? $model['mobil']->tipe : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['merk'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->merk ? $model['mobil']->merk : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['warna'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->warna ? $model['mobil']->warna : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['tahun'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->tahun ? $model['mobil']->tahun : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['tipe_bbm'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->tipe_bbm ? $model['mobil']->tipe_bbm : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['odometer_unit_comes'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->odometer_unit_comes ? $model['mobil']->odometer_unit_comes : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['periode_sewa'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->periode_sewa ? $model['mobil']->periode_sewa : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['tanggal_mulai_sewa'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->tanggal_mulai_sewa ? $model['mobil']->tanggal_mulai_sewa : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['tanggal_akhir_sewa'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->tanggal_akhir_sewa ? $model['mobil']->tanggal_akhir_sewa : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['harga_per_bulan'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->harga_per_bulan ? $model['mobil']->harga_per_bulan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['periode_service'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->periode_service ? $model['mobil']->periode_service : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['tanggal_pajak'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->tanggal_pajak ? $model['mobil']->tanggal_pajak : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['masa_stnk'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->masa_stnk ? $model['mobil']->masa_stnk : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['nama_pemilik'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->nama_pemilik ? $model['mobil']->nama_pemilik : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['created_at'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->created_at ? $model['mobil']->created_at : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-15">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['mobil']->attributeLabels()['updated_at'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['mobil']->updated_at ? $model['mobil']->updated_at : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Update', ['update', 'id' => $model['mobil']->id], ['class' => 'button bg-azure border-azure hover-bg-light-azure']) ?>&nbsp;
            <?= Html::a('Delete', ['delete', 'id' => $model['mobil']->id], [
                'class' => 'button text-azure border-azure hover-bg-light-azure',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item ?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Back to list', ['index'], ['class' => 'button text-azure border-azure hover-bg-azure pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>