<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/admin_pusat/mobil/list-v1.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<table class="datatables table table-striped table-hover table-nowrap">
    <thead>
        <tr class="text-dark">
            <th>Action</th>
            <th>Nomor Polisi</th>
            <th>Dedicate</th>
            <th>Vendor</th>
            <th>Tipe</th>
            <th>Merk</th>
            <th>Warna</th>
            <th>Tahun</th>
            <th>Tipe Bbm</th>
            <th>Odometer Unit Comes</th>
            <th>Periode Sewa</th>
            <th>Tanggal Mulai Sewa</th>
            <th>Tanggal Akhir Sewa</th>
            <th>Harga Per Bulan</th>
            <th>Periode Service</th>
            <th>Tanggal Pajak</th>
            <th>Masa Stnk</th>
            <th>Nama Pemilik</th>
            <th>Created At</th>
            <th>Updated At</th>
        </tr>
        <tr class="dt-search">
            <th></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search nomor polisi"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search dedicate"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search vendor"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search tipe"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search merk"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search warna"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search tahun"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search tipe bbm"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search odometer unit comes"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search periode sewa"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search tanggal mulai sewa"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search tanggal akhir sewa"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search harga per bulan"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search periode service"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search tanggal pajak"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search masa stnk"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search nama pemilik"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search created at"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search updated at"/></th>
        </tr>
    </thead>
</table>

<?php if ($this->context->can('mobil')): ?>
    <hr class="margin-y-15">
    <div>
        <?= Html::a('Tambah Mobil Baru', ['create'], ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
    </div>
<?php endif; ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
