<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['karyawan']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['karyawan'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

    <div class="box box-break-sm margin-bottom-10">
        <div class="box-1 padding-x-0">Nama : </div>
        <div class="box-11 m-padding-x-0 text-dark">
            <?= $model['user']->name ? $model['user']->name : '<span class="text-gray f-italic">(kosong)</span>' ?>
        </div>
    </div>

    <div class="box box-break-sm margin-bottom-10">
        <div class="box-1 padding-x-0"><?= $model['user']->attributeLabels()['username'] ?> : </div>
        <div class="box-11 m-padding-x-0 text-dark">
            <?= $model['user']->username ? $model['user']->username : '<span class="text-gray f-italic">(kosong)</span>' ?>
        </div>
    </div>

    <hr>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['user'], 'password', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'password', ['class' => 'form-label', 'label' => 'Password Baru']); ?>
        <?= Html::activePasswordInput($model['user'], 'password', ['class' => 'form-text', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'password', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'password')->end(); ?>
  
    <?= $form->field($model['user'], 'password_repeat', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'password_repeat', ['class' => 'form-label']); ?>
        <?= Html::activePasswordInput($model['user'], 'password_repeat', ['class' => 'form-text', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'password_repeat', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'password_repeat')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['karyawan']->isNewRecord ? 'Create' : 'Update', ['class' => 'button bg-azure border-azure hover-bg-light-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'button text-azure border-azure hover-bg-light-azure']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'button text-azure border-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
