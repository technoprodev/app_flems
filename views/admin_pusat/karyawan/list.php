<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/admin_pusat/karyawan/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<table class="datatables table table-striped table-hover table-nowrap">
    <thead>
        <tr class="text-dark">
            <th></th>
            <th>Username</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Handphone</th>
            <th>Unit Kerja</th>
            <th>Sub Unit Kerja</th>
            <th>Pool</th>
            <th>Nik</th>
        </tr>
        <tr class="dt-search">
            <th></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search username"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search nama"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search email"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search handphone"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search unit kerja"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search sub unit kerja"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search pool"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search nik"/></th>
        </tr>
    </thead>
</table>

<?php if ($this->context->can('karyawan')): ?>
    <hr class="margin-y-15">
    <div>
        <?= Html::a('Tambah Karyawan Baru', ['create'], ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
    </div>
<?php endif; ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
