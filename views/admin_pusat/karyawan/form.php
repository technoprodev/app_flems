<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['karyawan']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['karyawan'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['user'], 'username', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'username', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['user'], 'username', ['class' => 'form-text']) ?>
        <?= Html::error($model['user'], 'username', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'username')->end(); ?>
  
    <?= $form->field($model['user'], 'password', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'password', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activePasswordInput($model['user'], 'password', ['class' => 'form-text', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'password', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'password')->end(); ?>
  
    <?= $form->field($model['user'], 'password_repeat', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'password_repeat', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activePasswordInput($model['user'], 'password_repeat', ['class' => 'form-text', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'password_repeat', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'password_repeat')->end(); ?>

    <div class="form-wrapper field-roles-assignments">
        <label class="form-label">Roles</label>
        <?= Html::checkboxList(
            'assignments[]',
            $model['assignments'],
            $roles
        ) ?>
    </div>

    <?= $form->field($model['user'], 'name', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'name', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['user'], 'name', ['class' => 'form-text']) ?>
        <?= Html::error($model['user'], 'name', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'name')->end(); ?>

    <?= $form->field($model['user'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'email', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['user'], 'email', ['class' => 'form-text']) ?>
        <?= Html::error($model['user'], 'email', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'email')->end(); ?>

    <?= $form->field($model['user'], 'phone', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'phone', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['user'], 'phone', ['class' => 'form-text']) ?>
        <?= Html::error($model['user'], 'phone', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'phone')->end(); ?>

    <?= $form->field($model['karyawan'], 'id_sub_unit_kerja', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['karyawan'], 'id_sub_unit_kerja', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['karyawan'], 'id_sub_unit_kerja', ArrayHelper::map(\app_flems\models\SubUnitKerja::find()->select(['sub_unit_kerja.id', 'CONCAT(unit_kerja, " - ", sub_unit_kerja) AS sub_unit_kerja'])->join('LEFT JOIN', 'unit_kerja uk', 'uk.id = sub_unit_kerja.id_unit_kerja')->indexBy('id')->asArray()->all(), 'id', 'sub_unit_kerja'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown']); ?>
        <?= Html::error($model['karyawan'], 'id_sub_unit_kerja', ['class' => 'form-info']); ?>
    <?= $form->field($model['karyawan'], 'id_sub_unit_kerja')->end(); ?>

    <?= $form->field($model['karyawan'], 'id_pool', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['karyawan'], 'id_pool', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeDropDownList($model['karyawan'], 'id_pool', ArrayHelper::map(\app_flems\models\UnitKerja::find()->indexBy('id')->asArray()->all(), 'id', 'unit_kerja'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown']); ?>
        <?= Html::error($model['karyawan'], 'id_pool', ['class' => 'form-info']); ?>
    <?= $form->field($model['karyawan'], 'id_pool')->end(); ?>

    <?= $form->field($model['karyawan'], 'nik', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['karyawan'], 'nik', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['karyawan'], 'nik', ['class' => 'form-text', 'maxlength' => true]) ?>
        <?= Html::error($model['karyawan'], 'nik', ['class' => 'form-info']); ?>
    <?= $form->field($model['karyawan'], 'nik')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['karyawan']->isNewRecord ? 'Create' : 'Update', ['class' => 'button bg-azure border-azure hover-bg-light-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'button text-azure border-azure hover-bg-light-azure']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'button text-azure border-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
