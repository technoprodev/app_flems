<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/dispatcher/supir/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure"><?= $title ?></h2>
<?php endif; ?>

<table class="datatables table table-striped table-hover table-nowrap">
    <thead>
        <tr>
            <th></th>
            <th>Username</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Handphone</th>
            <th>Nik</th>
            <th>Tipe</th>
            <th>Mobil</th>
            <th>Pool</th>
            <th>Vendor</th>
            <th>Nomor Ktp</th>
            <th>Tipe Sim</th>
            <th>Nomor Sim</th>
            <th>Alamat</th>
            <th>Tempat Lahir</th>
            <th>Tanggal Lahir</th>
            <th>Jenis Kelamin</th>
            <th>Pendidikan Terakhir</th>
            <th>Golongan Darah</th>
            <th>Status</th>
            <th>Agama</th>
            <th>Kewarganegaraan</th>
            <th>Pelatihan</th>
            <th>Nomor Kontrak</th>
            <th>Tanggal Kontrak</th>
        </tr>
        <tr class="dt-search">
            <th></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search Username"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search Nama"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search Email"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search Handphone"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search Nik"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search tipe"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search mobil"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search pool"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search vendor"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search nomor ktp"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search tipe sim"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search nomor sim"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search alamat"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search tempat lahir"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search tanggal lahir"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search jenis kelamin"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search pendidikan terakhir"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search golongan darah"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search status"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search agama"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search kewarganegaraan"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search pelatihan"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search nomor kontrak"/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search tanggal kontrak"/></th>
        </tr>
    </thead>
</table>

<?php if ($this->context->can('supir')): ?>
    <hr class="margin-y-15">
    <div>
        <?= Html::a('Tambah Supir Baru', ['create'], ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
    </div>
<?php endif; ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
