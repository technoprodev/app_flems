<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\ChoicesAsset::register($this);
$this->registerJsFile('@web/app/dispatcher/dashboard/form-butuh-persetujuan.js', ['depends' => 'technosmart\assets_manager\VueResourceAsset']);
$this->registerJsFile('@web/app/dispatcher/dashboard/list-butuh-persetujuan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">#<?= $model['pesanan']->id ?> Butuh Persetujuan</h2>
<?php endif; ?>
        <div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Status</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->status ? $model['pesanan']->status : '(kosong)' ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Tipe Pesanan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->tipe ? $model['pesanan']->tipe : '(kosong)' ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Waktu Keberangkatan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->waktu_keberangkatan ? DateTime::createFromFormat('d/m/Y H:i:s', $model['pesanan']->waktu_keberangkatan)->format("d M, h:i") : '(kosong)' ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Waktu Kembali</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->waktu_kembali ? DateTime::createFromFormat('d/m/Y H:i:s', $model['pesanan']->waktu_kembali)->format("d M, h:i") : '(kosong)' ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Tujuan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->tujuan ? $model['pesanan']->tujuan : '(kosong)' ?>
                    
                    <?php foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan) : ?>
                    
                    <div class="margin-top-5">
                        <?= $pesananTujuan->id ? $pesananTujuan->tujuan : '(kosong)' ?>
                    </div>

                    <?php endforeach; ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Penumpang</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= $model['pesanan']->penumpang->name ?>
                    
                    <?php foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang) : ?>
                    
                    <div class="margin-top-5">
                        <?= $pesananPenumpang->id ? $pesananPenumpang->name : '(kosong)' ?>
                    </div>

                    <?php endforeach; ?>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Keperluan</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="border-none padding-0"><?= $model['pesanan']->keperluan ? $model['pesanan']->keperluan : '(kosong)' ?></div>
                </div>
            </div>

            <hr>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Estimasi Jarak</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="border-none padding-0"><?= $model['pesanan']->estimasi_jarak ? $model['pesanan']->estimasi_jarak . ' KM' : '(kosong)' ?></div>
                </div>
            </div>

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <div class="fw-bold">Estimasi Biaya</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="border-none padding-0"><?= $model['pesanan']->estimasi_biaya ? 'Rp ' . number_format($model['pesanan']->estimasi_biaya, 2) : '(kosong)' ?></div>
                </div>
            </div>

            <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>

            <?= $form->field($model['pesanan'], 'anggaran', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'anggaran', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::activeTextInput($model['pesanan'], 'anggaran', ['class' => 'form-text']) ?>
                    <?= Html::error($model['pesanan'], 'anggaran', ['class' => 'form-info']); ?>
                </div>
            <?= $form->field($model['pesanan'], 'anggaran')->end(); ?>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 text-right m-text-left"></div>
                <div class="box-10 m-padding-x-0">
                    <div class="bg-azure padding-x-15 padding-y-5" style="width: 500px; max-width: 100%;">
                        Estimasi jarak perjalanan ini adalah <?= $model['pesanan']->estimasi_jarak > 60 ? 'diatas' : 'dibawah' ?> 60km, maka secara default tergolong sebagai <?= $model['pesanan']->estimasi_jarak > 60 ? 'SPJ' : 'Bukan SPJ' ?>. Namun Anda diperbolehkan menggantinya jika memang diperlukan.
                    </div>
                    <div class="margin-top-15"></div>
                </div>
            </div>

            <?= $form->field($model['pesanan'], 'spj', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left">
                    <?= Html::activeLabel($model['pesanan'], 'spj', ['class' => 'form-label fw-bold', 'label' => 'SPJ ?']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::activeRadioList($model['pesanan'], 'spj', ['Tidak', 'Ya'], ['class' => 'form-radio', 'unselect' => null,
                        'item' => function($index, $label, $name, $checked, $value){
                            $checked = $checked ? 'checked' : '';
                            $disabled = in_array($value, []) ? 'disabled' : '';
                            return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                        }]); ?>
                    <?= Html::error($model['pesanan'], 'spj', ['class' => 'form-info']); ?>
                </div>
            <?= $form->field($model['pesanan'], 'spj')->end(); ?>

            <!-- <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    
                </div>
                <div class="box-10 m-padding-x-0">
                    <a class="button button-sm text-azure border-azure hover-bg-light-azure">Pilih Supir</a>
                </div>
            </div> -->

            
            <?= $form->field($model['pesanan'], 'id_supir', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <?= Html::activeLabel($model['pesanan'], 'id_supir', ['class' => 'form-label fw-bold']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div id="pesanan-id_supir" class="form-radio">
                    
                        <div class="scoll-x margin-top-min-5">
                            <table class="datatables-supir table table-striped table-hover table-nowrap">
                                <thead>
                                    <tr>
                                        <th class="padding-right-0" rowspan="2" style="vertical-align: top;text-align: center;"><!-- Pilih disini <br>  --><img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/arrow-down.png" width="30px;" class="margin-top-5"></th>
                                        <th>Supir</th>
                                        <th>Dedicate ?</th>
                                        <th>Mobil</th>
                                        <th>Nopol</th>
                                        <th>Status Supir</th>
                                        <th>Kota Terakhir</th>
                                        <th>Status SPK Terakhir</th>
                                        <th>Waktu Keberangkatan</th>
                                        <th>Waktu Kembali</th>
                                        <th>Jumlah Tunggakan</th>
                                    </tr>
                                    <tr class="dt-search">
                                        <!-- <th></th> -->
                                        <th><input type="text" class="form-text border-none padding-0" placeholder="search supir"/></th>
                                        <th><input type="text" class="form-text border-none padding-0" placeholder="search dedicate"/></th>
                                        <th><input type="text" class="form-text border-none padding-0" placeholder="search mobil"/></th>
                                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nopol"/></th>
                                        <th><input type="text" class="form-text border-none padding-0" placeholder="search status supir"/></th>
                                        <th><input type="text" class="form-text border-none padding-0" placeholder="search waktu mulai"/></th>
                                        <th><input type="text" class="form-text border-none padding-0" placeholder="search waktu selesai"/></th>
                                        <th><input type="text" class="form-text border-none padding-0" placeholder="search kota terakhir"/></th>
                                        <th><input type="text" class="form-text border-none padding-0" placeholder="search status spk terakhir"/></th>
                                        <th><input type="text" class="form-text border-none padding-0" placeholder="search jumlah tunggakan"/></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        
                    </div>

                    <?php Html::activeDropDownList($model['pesanan'], 'id_supir', ArrayHelper::map(
                        \Yii::$app->db->createCommand(
                            "
                                SELECT u.id, u.name
                                FROM supir s
                                JOIN user u ON u.id = s.id_user
                                WHERE s.id_pool = :idPool
                                AND s.tipe = 'Penumpang'
                            ",
                            [
                                'idPool' => Yii::$app->user->identity->karyawan->id_pool,
                            ]
                        )->queryAll(),
                        'id', 'name'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown input-choices', 'v-model' => 'pesanan.id_supir']); ?>
                    <?= Html::error($model['pesanan'], 'id_supir', ['class' => 'form-info']); ?>
                </div>
            <?= $form->field($model['pesanan'], 'id_supir')->end(); ?>

            <!-- <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="fw-bold">Mobil</div>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5">
                        {{pesanan.virtual_mobil}}
                    </div>
                </div>
            </div> -->

            <div class="box box-break-sm margin-bottom-15">
                <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                    
                </div>
                <div class="box-10 m-padding-x-0">
                    <?= Html::submitButton('Submit', ['class' => 'button button-sm bg-azure border-azure hover-bg-light-azure']) ?>
                    <?= Html::resetButton('Reset', ['class' => 'button button-sm bg-lightest text-azure border-azure hover-bg-light-azure']); ?>
                </div>
            </div>
            
            <?php ActiveForm::end(); ?>
        </div>
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
