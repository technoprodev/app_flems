<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/dispatcher/dashboard/list-index-butuh_persetujuan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/dispatcher/dashboard/list-index-dalam_proses.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/dispatcher/dashboard/list-index-selesai.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="box box-break-sm box-space-md box-gutter">
    <div class="box-12 bg-lightest shadow padding-30 rounded-md">
        <h2 class="margin-top-0 margin-bottom-30 text-dark-azure">Pesanan</h2>
        <ul class="nav nav-tabs f-bold">
            <li class="active"><a href="#tab-butuh_persetujuan" data-toggle="tab">Butuh Persetujuan</a></li>
            <li class="activ3"><a href="#tab-dalam_proses" data-toggle="tab">Dalam Proses</a></li>
            <li class="activ3"><a href="#tab-selesai" data-toggle="tab">Selesai</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane padding-20 fade active in" id="tab-butuh_persetujuan">
                <table id="table-butuh_persetujuan" class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Penumpang</th>
                            <th>Supir</th>
                            <th>Waktu Keberangkatan</th>
                            <th>Waktu Kembali</th>
                            <th>Anggaran</th>
                            <th>Tipe Perjalanan</th>
                        </tr>
                    </thead>
                    <!-- <tbody>
                        <tr>
                            <td>
                                <a href="<?= Yii::$app->urlManager->createUrl("dispatcher/dashboard/form-butuh-persetujuan") ?>" class="text-azure margin-right-5">Approve</a>
                            </td>
                            <td><b>admin pradana</b></td>
                            <td>Reguler</td>
                            <td>Kantor Pusat, Jakarta Utara, DKI Jakarta</td>
                        </tr>
                    </tbody> -->
                </table>
            </div>
            <div class="tab-pane padding-20 fade" id="tab-dalam_proses">
                <table id="table-dalam_proses" class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Status</th>
                            <th>Penumpang</th>
                            <th>Supir</th>
                            <th>Waktu Keberangkatan</th>
                            <th>Waktu Kembali</th>
                            <th>Anggaran</th>
                            <th>Tipe Perjalanan</th>
                        </tr>
                    </thead>
                    <!-- <tbody>
                        <tr>
                            <td>
                                <a href="<?= Yii::$app->urlManager->createUrl("dispatcher/dashboard/detail-dalam-proses") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td><b>admin pradana</b></td>
                            <td>Reguler</td>
                            <td>Kantor Pusat, Jakarta Utara, DKI Jakarta</td>
                        </tr>
                    </tbody> -->
                </table>
            </div>
            <div class="tab-pane padding-20 fade" id="tab-selesai">
                <table id="table-selesai" class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Status</th>
                            <th>Penumpang</th>
                            <th>Supir</th>
                            <th>Waktu Keberangkatan</th>
                            <th>Waktu Kembali</th>
                            <th>Anggaran</th>
                            <th>Tipe Perjalanan</th>
                        </tr>
                    </thead>
                    <!-- <tbody>
                        <tr>
                            <td>
                                <a href="<?= Yii::$app->urlManager->createUrl("dispatcher/dashboard/detail-selesai") ?>" class="text-spring margin-right-5" modal-md="" modal-title="#29333">Detail</a>
                            </td>
                            <td><b>admin pradana</b></td>
                            <td>Reguler</td>
                            <td>Kantor Pusat, Jakarta Utara, DKI Jakarta <span class="border-azure bg-light-azure padding-y-2 padding-x-5 fs-11 rounded-sm">Sudah di accept driver</span></td>
                        </tr>
                    </tbody> -->
                </table>
            </div>
        </div>
    </div>
</div>