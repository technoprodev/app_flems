<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['login']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['login'], ['class' => '']);
}
?>

<div style="max-width: 350px; width: 100%;">
    <div class="padding-top-30"></div>

    <div class="text-dark fs-26 fw-light">
        Login pada form berikut untuk menggunakan aplikasi
    </div>

    <div class="margin-top-50"></div>

    <?php $form = ActiveForm::begin(['id' => 'app']); ?>

    <?= $form->field($model['login'], 'login', ['options' => ['class' => 'form-wrapper']])->begin(); ?>
        <?= Html::activeLabel($model['login'], 'login', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activeTextInput($model['login'], 'login', ['class' => 'form-text border-light-orange', 'maxlength' => true]); ?>
        <?= Html::error($model['login'], 'login', ['class' => 'help-block help-block-error']); ?>
    <?= $form->field($model['login'], 'login')->end(); ?>

    <?= $form->field($model['login'], 'password', ['options' => ['class' => 'form-wrapper']])->begin(); ?>
        <?= Html::activeLabel($model['login'], 'password', ['class' => 'form-label fw-bold']); ?>
        <?= Html::activePasswordInput($model['login'], 'password', ['class' => 'form-text border-light-orange', 'maxlength' => true]); ?>
        <?= Html::error($model['login'], 'password', ['class' => 'help-block help-block-error']); ?>
    <?= $form->field($model['login'], 'password')->end(); ?>

    <?= $form->field($model['login'], 'rememberMe', ['options' => ['class' => 'form-wrapper']])->begin(); ?>
        <div class="checkbox">
            <?= Html::activeCheckbox($model['login'], 'rememberMe', ['uncheck' => 0]); ?>
        </div>
        <?= Html::error($model['login'], 'rememberMe', ['class' => 'help-block help-block-error']); ?>
    <?= $form->field($model['login'], 'rememberMe')->end(); ?>

    <?php if ($error) : ?>
        <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="margin-top-50"></div>

    <div class="form-group">
        <?= Html::submitButton('Login', ['class' => 'button padding-x-30 border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>