<?php
namespace app_flems\console\controllers;

use yii\console\Controller;
use technosmart\models\Permission;

class InitController extends Controller
{
    public $dir = [
        'app_flems\controllers' => '@app_flems/controllers',
    ];

    public function actionPermission()
    {
        if (Permission::initAllController($this->dir)) return 0;
        else return 1;
    }

    public function actionCache()
    {
        \Yii::$app->runAction('cache/flush-all');

        \Yii::$app->cache->cachePath = \Yii::getAlias('@app_flems/runtime/cache');
        \Yii::$app->runAction('cache/flush-all');
    }
}