sum(ptunggakan.anggaran-(ptunggakan.revisi_bbm+ptunggakan.revisi_tol+ptunggakan.revisi_parkir)) AS jumlah_tunggakan

select
    u.id,
    u.name AS supir,
    m.dedicate AS dedicate,
    concat(m.merk, ' ', m.tipe) AS mobil,
    m.nomor_polisi AS nopol,
    case when pt.status = "Checkout" then "Sedang Bertugas"  when pt.status = "SPK Selesai" then "Tersedia" end AS status_supir,
    pt.waktu_mulai AS waktu_mulai,
    pt.waktu_selesai AS waktu_selesai,
    pt.tujuan_kota AS kota_terakhir,
    pt.status AS status_spk_terakhir
from supir s
LEFT JOIN user u ON u.id = s.id_user
LEFT JOIN mobil m ON s.id_mobil = m.id
LEFT JOIN pesanan pt ON pt.id_supir = s.id_user
JOIN (
	select id_supir, max(waktu_selesai) as max_waktu_selesai
	from pesanan
	where status = "Checkout" or status = "SPK Selesai"
	group by id_supir
) pp ON pp.id_supir = pt.id_supir AND pp.max_waktu_selesai = pt.waktu_selesai




select id, nama, max(skor) from coba group by nama, id

SELECT  coba.nama, coba.skor, coba.id
FROM coba
JOIN (
	select nama, max(skor) as maxskor
	from coba
	group by nama
) c
on coba.nama = c.nama
and coba.skor = c.maxskor