/**
 * Technoart v0.0.9 (https://technoartcss.com)
 * Copyright (c) 2018-present Fandy Pradana (https://prafandy.com)
 * Licensed under MIT (https://github.com/technoprodev/technoart/blob/master/LICENSE)
 */
var technoart = {
  backToTop: backToTop,
  fixedOnScroll: fixedOnScroll,
  footerFixed: footerFixed,
  menuY: menuY,
  toggleSidebarLeft: toggleSidebarLeft
};
//# sourceMappingURL=index.js.map