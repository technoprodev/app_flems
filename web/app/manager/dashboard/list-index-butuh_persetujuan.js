$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('#table-butuh_persetujuan');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('manager/dashboard/datatables-butuh-persetujuan'),
                    type: 'POST'
                },
                order: [[3, 'desc']],
                columns: [
                    {
                        data: 'id',
                        'searchable': false,
                        'orderable': false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<a data-toggle="tooltip" data-placement="top" title="Setuju" href="' + fn.urlTo('manager/dashboard/approve/' + data) + '" class="text-azure margin-right-10" data-confirm="Are you sure you want to approve this order?" data-method="post"><img src="https://image.flaticon.com/icons/svg/179/179372.svg" height="20" width="auto"></a>' +
                                '<a data-toggle="tooltip" data-placement="top" title="Tolak" href="' + fn.urlTo('manager/dashboard/reject/' + data) + '" class="text-rose margin-right-10" data-confirm="Are you sure you want to reject this order?" data-method="post"><img src="https://image.flaticon.com/icons/svg/179/179429.svg" height="20" width="auto"></a>' +
                                '<a data-toggle="tooltip" data-placement="top" title="Detail" href="' + fn.urlTo('manager/dashboard/detail-butuh-persetujuan/' + data) + '" class="text-spring margin-right-10" modal-md="" modal-title="#' + row.id + '"><img src="https://image.flaticon.com/icons/svg/179/179614.svg" height="20" width="auto"></a>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent:'&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.id',
                            display: 'id',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '#' + data +
                                '';
                        },
                    },
                    {
                        data: {
                            _: 'u.name',
                            display: 'name',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<b>' + data + '</b>' +
                                '';
                        },
                    },
                    /*{
                        data: {
                            _: 'su.name',
                            display: 'driver_name',
                        },
                        defaultContent:'&nbsp;',
                    },*/
                    {data: 'waktu_keberangkatan'},
                    {data: 'waktu_kembali'},
                    // {data: 'anggaran'},
                    // {data: 'spj'},
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});