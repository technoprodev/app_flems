$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('#table');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('supervisor/performa-supir/datatables'),
                    type: 'POST'
                },
                order: [[1, 'asc']],
                columns: [
                    {
                        data: 'id',
                        'searchable': false,
                        'orderable': false,
                        render: function ( data, type, row ) {
                            return ''
                            return '' +
                                '<a data-toggle="tooltip" data-placement="top" title="Detail" href="' + fn.urlTo('supervisor/performa-supir/detail/' + data) + '" class="text-azure margin-right-10" modal-md="" modal-title="#' + row.id + '">Lihat Absen</a>' +
                                '<a data-toggle="tooltip" data-placement="top" title="Detail" href="' + fn.urlTo('supervisor/performa-supir/detail/' + data) + '" class="text-azure margin-right-10" modal-md="" modal-title="#' + row.id + '">Lihat SPK</a>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent:'&nbsp;',
                    },
                    {
                        data: {
                            _: 'u.name',
                            display: 'name',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<b>' + data + '</b>' +
                                '';
                        },
                    },
                    {data: 'nik'},
                    {data: 'tipe'},
                    {data: 'vendor'},
                    {data: 'nomor_polisi'},
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});