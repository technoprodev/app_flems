$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-supir');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('dispatcher/dashboard/datatables-supir'),
                    type: 'POST'
                },
                dom: '<"clearfix margin-bottom-30 scroll-x"rt><"clearfix"p>',
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<label><input name="Pesanan[id_supir]" value="' + data + '" type="radio"></label>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                        className: 'text-center padding-right-0',
                    },
                    {
                        data: {
                            _: 'u.name',
                            display: 'supir',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {data: 'dedicate'},
                    {data: 'mobil'},
                    {data: 'nopol'},
                    {data: 'status_supir'},
                    {data: 'kota_terakhir'},
                    {data: 'status_spk_terakhir'},
                    {data: 'waktu_keberangkatan'},
                    {data: 'waktu_kembali'},
                    // {data: 'jumlah_tunggakan'},
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});