if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            pesanan: {
                id_supir: '',
                virtual_mobil: '',
            },
        },
        computed: {
        },
        watch: {
            'pesanan.id_supir': function(newVal, oldVal) {
                this.pesanan.virtual_mobil = 'Searching...';
                this.$http.get(fn.urlTo('dispatcher/dashboard/get-mobil/' + newVal)).then(
                    function (response) {
                        this.pesanan.virtual_mobil = response.body;
                    }, function (response) {
                        console.log(response);
                        console.log(fn.urlTo('dispatcher/dashboard/get-mobil/' + newVal));
                    }
                );
            },
        },
        methods: {
        },
        filters: {
            rupiah: function(value) {
                if (typeof value !== "number") {
                    return value;
                }
                var formatter = new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR',
                    minimumFractionDigits: 2
                });
                return formatter.format(value);
            },
        },
    });
}