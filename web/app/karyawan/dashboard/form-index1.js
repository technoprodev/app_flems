$(document).ready(function() {
    var el = $('.input-flatpickr-time-custom');
    $.each(el, function() {
        var $this = $(this);

        $this.wrap('<div class="input-group"></div>').before('<span class="input-group-addon square"><i class="fa fa-calendar"></i></span>');
    });
    var pesananWaktuKeberangkatan = flatpickr('#pesanan-waktu_keberangkatan', {
        allowInput: true,
        timeFormat: 'd/m/Y H:i',
        enableTime: true,
        time_24hr: true,
        defaultHour: 8,
        maxDate: $('#pesanan-waktu_kembali').attr('value'),
        onClose: function(selectedDates, dateStr, instance) {
            pesananWaktuKembali.set('minDate', dateStr);
        },
    });
    var pesananWaktuKembali = flatpickr('#pesanan-waktu_kembali', {
        allowInput: true,
        timeFormat: 'd/m/Y H:i',
        enableTime: true,
        time_24hr: true,
        defaultHour: 8,
        minDate: $('#pesanan-waktu_keberangkatan').attr('value'),
        onClose: function(selectedDates, dateStr, instance) {
            pesananWaktuKeberangkatan.set('maxDate', dateStr);
        },
    });
});

if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    }); 

    var vm = new Vue({
        el: '#app',
        data: {
            tempReturn: null,
            pesanan: {
                status_realisasi: '',
                tipe: '',
                tipe_penumpang: '',
                tujuan: null,
                tujuan_latitude: null,
                tujuan_longitude: null,
                pesananTujuans: [],
                pesananPenumpangs: [],
                id_supir: '',
                virtual_mobil: '',
                anggaran: 0,
                bbm: 0,
                tol: 0,
                parkir: 0,
                revisi_bbm: 0,
                revisi_tol: 0,
                revisi_parkir: 0,
                revisi: '',
            },
        },
        computed: {
            'sisa_realisasi': function() {
                return this.pesanan.anggaran - (this.pesanan.revisi_bbm + this.pesanan.revisi_tol + this.pesanan.revisi_parkir);
            },
        },
        watch: {
            'pesanan.id_supir': function(newVal, oldVal) {
                this.pesanan.virtual_mobil = 'Searching...';
                this.$http.get(fn.urlTo('dispatcher/dashboard/get-mobil/' + newVal)).then(
                    function (response) {
                        this.pesanan.virtual_mobil = response.body;
                    }, function (response) {
                        console.log(response);
                        console.log(fn.urlTo('dispatcher/dashboard/get-mobil/' + newVal));
                    }
                );
            },
            'pesanan.revisi': function(newVal, oldVal) {
                if (newVal == '0' || newVal == '1') {
                    this.pesanan.status_realisasi = 'Disetujui Supervisor';
                } else if (newVal == '2') {
                    this.pesanan.status_realisasi = 'Ditolak Supervisor';
                }
            },
        },
        methods: {
            addPesananPenumpang: function() {
                this.pesanan.pesananPenumpangs.push({
                    id: null,
                    id_pesanan: null,
                    id_penumpang: '',
                });
            },
            removePesananPenumpang: function(i, isNew) {
                if (this.pesanan.pesananPenumpangs[i].id == null)
                    this.pesanan.pesananPenumpangs.splice(i, 1);
                else
                    this.pesanan.pesananPenumpangs[i].id*=-1;
            },
            addPesananTujuan: function(data) {
                // this.pesanan.pesananTujuans.push({
                //     id: null,
                //     id_pesanan: null,
                //     tujuan: '',
                //     tujuan_latitude: '',
                //     tujuan_longitude: '',
                // });
                if (this.pesanan.tujuan) {
                    this.pesanan.pesananTujuans.push(
                        Object.assign({
                            id: null,
                            id_pesanan: null,
                            tujuan: '',
                            tujuan_latitude: '',
                            tujuan_longitude: '',
                        }, data)
                    );
                } else {
                    this.pesanan.tujuan = data.tujuan;
                    this.pesanan.tujuan_latitude = data.tujuan_latitude;
                    this.pesanan.tujuan_longitude = data.tujuan_longitude;
                }
            },
            removePesananTujuan: function(i, isNew) {
                if (this.pesanan.pesananTujuans[i].id == null)
                    this.pesanan.pesananTujuans.splice(i, 1);
                else
                    this.pesanan.pesananTujuans[i].id*=-1;
            },
            removeTujuan: function() {
                this.pesanan.tujuan = null;
                this.pesanan.tujuan_latitude = null;
                this.pesanan.tujuan_longitude = null;
            },
        },
        filters: {
            rupiah: function(value) {
                if (typeof value !== "number") {
                    return value;
                }
                var formatter = new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR',
                    minimumFractionDigits: 2
                });
                return formatter.format(value);
            },
        },
    });

    var vm1 = new Vue({
        el: '#app-shuttle',
        data: {
            tempReturn: null,
        },
    });
}