google.maps.event.addDomListener(window, 'load', initMap);

function initMap() {
    var map = new google.maps.Map(document.getElementById('map1'), {
        center: {
            lat: -1.671232,
            lng: 117.507388, 
        },
        zoom: 4
    });

    var input = document.getElementById('map1-input');

    $(input)
        .on("keypress", function(e) {
            if (e.keyCode === 13) {
                e.preventDefault();
            }
        })
        .on("blur", function() {
            $(this).val("");
        })

    // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert('No details available for input: ' + place.name);
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);

            vm.addPesananTujuan({
                tujuan: place.name,
                tujuan_latitude: place.geometry.location.lat(),
                tujuan_longitude: place.geometry.location.lng(),
            });
            document.getElementById("map1-input").value = '';
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17); // Why 17? Because it looks good.
        }

        marker.setIcon( /** @type {google.maps.Icon} */ ({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
        console.log(place.geometry);
        console.log(place);
    });
}

$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        $.fn.dataTableExt.oStdClasses.sLength = 'dataTables_length form-wrapper';
        var el = $('.datatables-dalam-proses');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                dom: '<"clearfix margin-bottom-30"l><"clearfix margin-bottom-30 scroll-x"rt><"clearfix"ip>',
                lengthMenu: [[10, 25, 50, 100, -1], ['10 entries', '25 entries', '50 entries', '100 entries', 'All entries']],
                // stateSave: true,
                ajax: {
                    url: fn.urlTo('karyawan/dashboard/datatables-dalam-proses'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        className: 'padding-0',
                        render: function ( data, type, row ) {
                            var batalkanPesanan = '';
                            if (row.status == 'Diminta' || row.status == 'Disetujui Manager' || row.status == 'Dialokasikan Dispatcher' || row.status == 'Disetujui Supervisor') {
                                batalkanPesanan = '<a href="' + fn.urlTo('karyawan/dashboard/batalkan-pesanan', {id: data}) + '" class="" data-toggle="tooltip" data-placement="top" title="Batalkan pesanan" data-confirm="Apakah Anda yakin membatalkan pesanan ini ?" data-method="post"><img src="https://image.flaticon.com/icons/svg/179/179429.svg" height="30" width="auto"></a>';
                            }
                            return '' +
                                '<div class="border rounded-xs padding-20 margin-bottom-15">' +
                                '    <h5 class="margin-top-0">' +
                                '        <span class="text-azure margin-right-10">#' + row.id + '</span>' +
                                '        <span class="fs-16 f-normal">' + (row.tujuan ? row.tujuan : '(kosong)') + '</span>' +
                                /*'        <span class="fs-16 f-bold">' + row.status + '</span>' +*/
                                '    </h5>' +
                                '    <div class="box box-break-sm">' +
                                '        <div class="box-12 padding-0 margin-bottom-10">' +
                                '            <div class="text-azure"><code class="margin-0 bg-light-red border-light-red margin-right-5">Status &nbsp;:</code> ' + row.status + '</div>' +
                                (row.tipe_penumpang ?
                                '            <div class=""><code class="margin-0 bg-light-red border-light-red margin-right-5">Jenis &nbsp;&nbsp;:</code> ' + row.tipe_penumpang + '</div>' : '') +
                                '        </div>' +
                                '        <div class="box-6 padding-0 margin-bottom-15">' +
                                '            <div class=""><code class="margin-0 bg-light-spring border-light-spring margin-right-5">OTP In &nbsp;:</code> ' + (row.otp_checkin ? row.otp_checkin : '(kosong)') + '</div>' +
                                '            <div class=""><code class="margin-0 bg-light-spring border-light-spring margin-right-5">OTP Out :</code> ' + (row.otp_checkout ? row.otp_checkout : '(kosong)') + '</div>' +
                                '        </div>' +
                                '        <div class="box-6 padding-0 margin-bottom-15">' +
                                '            <div class=""><code class="margin-0 bg-light-azure border-light-azure margin-right-5">Start:</code> ' + row.waktu_keberangkatan + '</div>' +
                                '            <div class=""><code class="margin-0 bg-light-azure border-light-azure margin-right-5">End &nbsp;:</code> ' + row.waktu_kembali + '</div>' +
                                '        </div>' +
                                '        <div class="box-12 padding-0 margin-bottom-15">' +
                                '            <div class=""><code class="margin-0 bg-light-orange border-light-orange margin-right-5">Supir &nbsp;&nbsp;:</code> ' + (row.supir ? row.supir : '(kosong)') + '</div>' +
                                '            <div class=""><code class="margin-0 bg-light-orange border-light-orange margin-right-5">HP Supir:</code> ' + (row.hp_supir ? row.hp_supir : '(kosong)') + '</div>' +
                                '        </div>' +
                                '        <div class="box-12 padding-0">' +
                                '            <a href="' + fn.urlTo('karyawan/dashboard/detail', {id: data}) + '" class="margin-right-10" modal-md="" modal-title="#' + row.id + '"><img src="https://image.flaticon.com/icons/svg/179/179614.svg" height="30" width="auto"></a>' +
                                batalkanPesanan +
                                '        </div>' +
                                '    </div>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    /*{data: 'id_combination'},
                    {
                        data: {
                            _: 'dco.name',
                            filter: 'dco.name',
                            sort: 'dco.name',
                            display: 'link',
                        },
                        defaultContent: '&nbsp;',
                    },*/
                ],
                drawCallback: function(settings) {
                    $('.datatables-dalam-proses thead').remove();
                    technoart.footerFixed();
                },
                language: {
                    lengthMenu : '_MENU_',
                    search: '',
                    searchPlaceholder: 'search all here...',
                    buttons: {
                        copyTitle: 'Title',
                        copyKeys: 'copy keys',
                        copySuccess: {
                            _: '%d rows copied',
                            1: '1 row copied'
                        }
                    },
                    emptyTable: 'No data available'
                }
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});

$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        $.fn.dataTableExt.oStdClasses.sLength = 'dataTables_length form-wrapper';
        var el = $('.datatables-selesai');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                dom: '<"clearfix margin-bottom-30"l><"clearfix margin-bottom-30 scroll-x"rt><"clearfix"ip>',
                lengthMenu: [[10, 25, 50, 100, -1], ['10 entries', '25 entries', '50 entries', '100 entries', 'All entries']],
                // stateSave: true,
                ajax: {
                    url: fn.urlTo('karyawan/dashboard/datatables-selesai'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<div class="border rounded-xs padding-20 margin-bottom-15">' +
                                '    <h5 class="margin-top-0">' +
                                '        <span class="text-azure margin-right-10">#' + row.id + '</span>' +
                                '        <span class="fs-16 f-normal">' + (row.tujuan ? row.tujuan : '(kosong)') + '</span>' +
                                /*'        <span class="fs-16 f-bold">' + row.status + '</span>' +*/
                                '    </h5>' +
                                '    <div class="box box-break-sm">' +
                                '        <div class="box-12 padding-0 margin-bottom-10">' +
                                '            <div class="text-azure"><code class="margin-0 bg-light-red border-light-red margin-right-5">Status &nbsp;:</code> ' + row.status + '</div>' +
                                (row.tipe_penumpang ?
                                '            <div class=""><code class="margin-0 bg-light-red border-light-red margin-right-5">Jenis &nbsp;&nbsp;:</code> ' + row.tipe_penumpang + '</div>' : '') +
                                '        </div>' +
                                '        <div class="box-6 padding-0 margin-bottom-15">' +
                                '            <div class=""><code class="margin-0 bg-light-spring border-light-spring margin-right-5">OTP In &nbsp;:</code> ' + (row.otp_checkin ? row.otp_checkin : '(kosong)') + '</div>' +
                                '            <div class=""><code class="margin-0 bg-light-spring border-light-spring margin-right-5">OTP Out :</code> ' + (row.otp_checkout ? row.otp_checkout : '(kosong)') + '</div>' +
                                '        </div>' +
                                '        <div class="box-6 padding-0 margin-bottom-15">' +
                                '            <div class=""><code class="margin-0 bg-light-azure border-light-azure margin-right-5">Start:</code> ' + row.waktu_keberangkatan + '</div>' +
                                '            <div class=""><code class="margin-0 bg-light-azure border-light-azure margin-right-5">End &nbsp;:</code> ' + row.waktu_kembali + '</div>' +
                                '        </div>' +
                                '        <div class="box-12 padding-0 margin-bottom-15">' +
                                '            <div class=""><code class="margin-0 bg-light-orange border-light-orange margin-right-5">Supir &nbsp;&nbsp;:</code> ' + (row.supir ? row.supir : '(kosong)') + '</div>' +
                                '            <div class=""><code class="margin-0 bg-light-orange border-light-orange margin-right-5">HP Supir:</code> ' + (row.hp_supir ? row.hp_supir : '(kosong)') + '</div>' +
                                '        </div>' +
                                '        <div class="box-12 padding-0">' +
                                '            <a href="' + fn.urlTo('karyawan/dashboard/detail', {id: data}) + '" class="margin-right-10" modal-md="" modal-title="#' + row.id + '"><img src="https://image.flaticon.com/icons/svg/179/179614.svg" height="30" width="auto"></a>' +
                                '            <a class=""><img src="https://image.flaticon.com/icons/svg/179/179429.svg" height="30" width="auto"></a>' +
                                '        </div>' +
                                '    </div>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    /*{data: 'id_combination'},
                    {
                        data: {
                            _: 'dco.name',
                            filter: 'dco.name',
                            sort: 'dco.name',
                            display: 'link',
                        },
                        defaultContent: '&nbsp;',
                    },*/
                ],
                drawCallback: function(settings) {
                    $('.datatables-selesai thead').remove();
                    technoart.footerFixed();
                },
                language: {
                    lengthMenu : '_MENU_',
                    search: '',
                    searchPlaceholder: 'search all here...',
                    buttons: {
                        copyTitle: 'Title',
                        copyKeys: 'copy keys',
                        copySuccess: {
                            _: '%d rows copied',
                            1: '1 row copied'
                        }
                    },
                    emptyTable: 'No data available'
                }
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});