$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('#table-transaksi_admin_pengajuan');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('admin_pool/petty-cash/datatables-transaksi-admin-pengajuan'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'status_pengajuan',
                        render: function ( data, type, row ) {
                            return '' +
                                data
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent:'&nbsp;',
                    },
                    {data: 'total_yang_diajukan'},
                    {data: 'waktu_pengajuan'},
                    {
                        data: {
                            _: 'us.name',
                            display: 'supervisor_yang_menyetujui',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ut.name',
                            display: 'supervisor_yang_menolak',
                        },
                        defaultContent: '&nbsp;',
                    },
                    /*{
                        data: 'id_pesanan',
                        render: function ( data, type, row ) {
                            if (data) {
                                return '' +
                                    '<a data-toggle="tooltip" data-placement="top" title="Detail" href="' + fn.urlTo('admin_pool/petty-cash/detail-transaksi-admin-pengajuan/' + data) + '" modal-md="" modal-title="#' + data + '" class="text-azure">#'+ data +'</a>' +
                                    '';
                            } else {
                                return '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent:'&nbsp;',
                    },*/
                    /*{
                        data: 'id',
                        'searchable': false,
                        'orderable': false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<a data-toggle="tooltip" data-placement="top" title="Cetak SPK" href="' + fn.urlTo('admin_pool/petty-cash/cetak/' + data) + '" class="text-azure margin-right-10" data-confirm="Apakah Anda yakin mencetak SPK ini ?" data-method="post"><img src="https://image.flaticon.com/icons/svg/179/179949.svg" height="20" width="auto"></a>' +
                                '<a data-toggle="tooltip" data-placement="top" title="Detail" href="' + fn.urlTo('admin_pool/petty-cash/detail-cetak-spk/' + data) + '" class="text-spring margin-right-10" modal-md="" modal-title="#' + row.id + '"><img src="https://image.flaticon.com/icons/svg/179/179614.svg" height="20" width="auto"></a>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent:'&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.id',
                            display: 'id',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '#' + data +
                                '';
                        },
                    },
                    {
                        data: {
                            _: 'u.name',
                            display: 'name',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<b>' + data + '</b>' +
                                '';
                        },
                    },
                    {
                        data: {
                            _: 'su.name',
                            display: 'driver_name',
                        },
                        defaultContent:'&nbsp;',
                    },*/
                    /*{data: 'waktu_keberangkatan'},
                    {data: 'waktu_kembali'},
                    {data: 'anggaran'},
                    {data: 'spj'},*/
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});