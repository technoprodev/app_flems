$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('#table-revisi_anggaran');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('admin_pool/dashboard/datatables-revisi-anggaran'),
                    type: 'POST'
                },
                order: [[1, 'asc']],
                columns: [
                    {
                        data: 'id',
                        'searchable': false,
                        'orderable': false,
                        render: function ( data, type, row ) {
                            var realisasi = '';
                            if (row.status == 'Checkout') {
                                if (row.status_realisasi == 'Disetujui Supervisor') {
                                    realisasi = '<a data-toggle="tooltip" data-placement="top" title="Realisasi" href="' + fn.urlTo('admin_pool/dashboard/form-realisasi/' + data) + '" class="text-azure margin-right-10"><img src="https://image.flaticon.com/icons/svg/432/432277.svg" height="20" width="auto"></a>';
                                } else if (row.status_realisasi == 'Ditolak Supervisor') {
                                    realisasi = '<a data-toggle="tooltip" data-placement="top" title="Realisasi" href="' + fn.urlTo('admin_pool/dashboard/form-realisasi/' + data) + '" class="text-azure margin-right-10"><img src="https://image.flaticon.com/icons/svg/432/432277.svg" height="20" width="auto"></a>';
                                }
                            }
                            return '' +
                                '<a data-toggle="tooltip" data-placement="top" title="Detail" href="' + fn.urlTo('admin_pool/dashboard/detail-revisi-anggaran/' + data) + '" class="text-spring margin-right-10" modal-md="" modal-title="#' + row.id + '"><img src="https://image.flaticon.com/icons/svg/179/179614.svg" height="20" width="auto"></a>' +
                                realisasi +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent:'&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.id',
                            display: 'id',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '#' + data +
                                '';
                        },
                    },
                    {
                        data: 'status_realisasi',
                        render: function ( data, type, row ) {
                            if (data == 'Diminta')
                                return '' +
                                    '<span class="text-spring">' + data + '</span>' +
                                    '';
                            else if (data == 'Disetujui Supervisor')
                                return '' +
                                    '<span class="text-azure">' + data + '</span>' +
                                    '';
                            else if (data == 'Ditolak Supervisor')
                                return '' +
                                    '<span class="text-red">' + data + '</span>' +
                                    '';
                        },
                    },
                    {data: 'status'},
                    {
                        data: {
                            _: 'u.name',
                            display: 'name',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<b>' + data + '</b>' +
                                '';
                        },
                    },
                    {
                        data: {
                            _: 'su.name',
                            display: 'driver_name',
                        },
                        defaultContent:'&nbsp;',
                    },
                    {data: 'waktu_keberangkatan'},
                    {data: 'waktu_kembali'},
                    {data: 'anggaran'},
                    {data: 'spj'},
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});