$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('#table-transaksi_supir');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('supir/transaksi/datatables-transaksi-supir'),
                    type: 'POST'
                },
                columns: [
                    {data: 'waktu'},
                    {data: 'keterangan'},
                    {
                        data: 'id_pesanan',
                        render: function ( data, type, row ) {
                            if (data) {
                                return '' +
                                    '<a data-toggle="tooltip" data-placement="top" title="Detail" href="' + fn.urlTo('supir/transaksi/detail-pesanan/' + data) + '" modal-md="" modal-title="#' + data + '" class="text-azure">#'+ data +'</a>' +
                                    '';
                            } else {
                                return '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent:'&nbsp;',
                    },
                    {data: 'debit'},
                    {data: 'kredit'},
                    {data: 'saldo'},
                ],
                dom: '<"clearfix margin-bottom-30 scroll-x"rt><"clearfix"ip>',
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});