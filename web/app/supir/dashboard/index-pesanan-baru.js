$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        $.fn.dataTableExt.oStdClasses.sLength = 'dataTables_length form-wrapper';
        var el = $('.datatables-pesanan-baru');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                dom: '<"clearfix margin-bottom-30"l><"clearfix margin-bottom-30 scroll-x"rt><"clearfix"ip>',
                lengthMenu: [[10, 25, 50, 100, -1], ['10 entries', '25 entries', '50 entries', '100 entries', 'All entries']],
                // stateSave: true,
                ajax: {
                    url: fn.urlTo('supir/dashboard/datatables-pesanan-baru'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        className: 'padding-0',
                        render: function ( data, type, row ) {
                            var action = '<a href="' + fn.urlTo('supir/dashboard/konfirmasi', {id: row.id}) + '" class="button button-sm bg-azure border-azure hover-bg-light-azure" data-confirm="Anda yakin ingin mengkonfirmasi pesanan ini ?" data-method="post">Confirm</a>';
                            return '' +
                                '<div class="border rounded-xs padding-20 margin-bottom-15">' +
                                '    <h5 class="margin-top-0">' +
                                '        <span class="text-azure margin-right-10">#' + row.id + '</span>' +
                                '        <span class="fs-16 f-normal">' + (row.tujuan ? row.tujuan : '(kosong)') + '</span>' +
                                /*'        <span class="fs-16 f-bold">' + row.status + '</span>' +*/
                                '    </h5>' +
                                '    <div class="box box-break-sm">' +
                                '        <div class="box-6 padding-0 margin-bottom-10">' +
                                '            <div class="text-azure"><code class="margin-0 bg-light-red border-light-red margin-right-5">Status:</code> ' + row.status + '</div>' +
                                (row.tipe_penumpang ?
                                '            <div class=""><code class="margin-0 bg-light-red border-light-red margin-right-5">Jenis :</code> ' + row.tipe_penumpang + '</div>' : '') +
                                '        </div>' +
                                '        <div class="box-6 padding-0 margin-bottom-15">' +
                                '            <div class=""><code class="margin-0 bg-light-spring border-light-spring margin-right-5">Anggaran&nbsp;&nbsp;&nbsp;&nbsp;:</code> ' + (row.anggaran ? row.anggaran : '(kosong)') + '</div>' +
                                '            <div class=""><code class="margin-0 bg-light-spring border-light-spring margin-right-5">SPJ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</code> ' + (row.spj == '1' ? 'Ya' : 'Tidak') + '</div>' +
                                '        </div>' +
                                '        <div class="box-6 padding-0 margin-bottom-15">' +
                                '            <div class=""><code class="margin-0 bg-light-azure border-light-azure margin-right-5">Start :</code> ' + row.waktu_keberangkatan + '</div>' +
                                '            <div class=""><code class="margin-0 bg-light-azure border-light-azure margin-right-5">End &nbsp;&nbsp;:</code> ' + row.waktu_kembali + '</div>' +
                                '        </div>' +
                                '        <div class="box-6 padding-0 margin-bottom-15">' +
                                '            <div class=""><code class="margin-0 bg-light-orange border-light-orange margin-right-5">Penumpang &nbsp;&nbsp;:</code> ' + (row.penumpang ? row.penumpang : '(kosong)') + '</div>' +
                                '            <div class=""><code class="margin-0 bg-light-orange border-light-orange margin-right-5">HP Penumpang:</code> ' + (row.hp_penumpang ? row.hp_penumpang : '(kosong)') + '</div>' +
                                '        </div>' +
                                '        <div class="box-12 padding-0">' +
                                action
                                '        </div>' +
                                '    </div>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    /*{data: 'id_combination'},
                    {
                        data: {
                            _: 'dco.name',
                            filter: 'dco.name',
                            sort: 'dco.name',
                            display: 'link',
                        },
                        defaultContent: '&nbsp;',
                    },*/
                ],
                drawCallback: function(settings) {
                    technoart.footerFixed();
                    $('.datatables-pesanan-baru thead').remove();
                },
                language: {
                    lengthMenu : '_MENU_',
                    search: '',
                    searchPlaceholder: 'search all here...',
                    buttons: {
                        copyTitle: 'Title',
                        copyKeys: 'copy keys',
                        copySuccess: {
                            _: '%d rows copied',
                            1: '1 row copied'
                        }
                    },
                    emptyTable: 'No data available'
                }
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});