$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('admin_pusat/pairing/datatables'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '<a href="' + fn.urlTo('admin_pusat/pairing', {id: data}) + '" class="text-azure" modal-md="" modal-title="Data Keterangan ' + row.name + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('admin_pusat/pairing/update', {id: data}) + '" class="text-spring"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {data: 'name'},
                    {
                        data: {
                            _: 'm.merk',
                            filter: 'm.merk',
                            sort: 'm.merk',
                            display: 'mobil',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ukp.unit_kerja',
                            filter: 'ukp.unit_kerja',
                            sort: 'ukp.unit_kerja',
                            display: 'pool',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'v.vendor',
                            filter: 'v.vendor',
                            sort: 'v.vendor',
                            display: 'vendor',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {data: 'username'},
                    {data: 'email'},
                    {data: 'phone'},
                    {data: 'nik'},
                    {data: 'tipe'},
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});