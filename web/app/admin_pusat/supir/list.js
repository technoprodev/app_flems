$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('admin_pusat/supir/datatables'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '<a href="' + fn.urlTo('admin_pusat/supir', {id: data}) + '" class="text-azure" modal-md="" modal-title="Data Keterangan ' + row.name + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('admin_pusat/supir/update', {id: data}) + '" class="text-spring"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('admin_pusat/supir/delete', {id: data}) + '" class="text-rose" data-confirm="Are you sure you want to delete this item?" data-method="post"><i class="fa fa-trash-o"></i></a>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {data: 'username'},
                    {data: 'name'},
                    {data: 'email'},
                    {data: 'phone'},
                    {data: 'nik'},
                    {data: 'tipe'},
                    {
                        data: {
                            _: 'm.merk',
                            filter: 'm.merk',
                            sort: 'm.merk',
                            display: 'mobil',
                        },
                        defaultContent: '&nbsp;',
                    },
                    /*{
                        data: {
                            _: 'uk.unit_kerja',
                            filter: 'uk.unit_kerja',
                            sort: 'uk.unit_kerja',
                            display: 'unit_kerja',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'suk.sub_unit_kerja',
                            filter: 'suk.sub_unit_kerja',
                            sort: 'suk.sub_unit_kerja',
                            display: 'sub_unit_kerja',
                        },
                        defaultContent: '&nbsp;',
                    },*/
                    {
                        data: {
                            _: 'ukp.unit_kerja',
                            filter: 'ukp.unit_kerja',
                            sort: 'ukp.unit_kerja',
                            display: 'pool',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'v.vendor',
                            filter: 'v.vendor',
                            sort: 'v.vendor',
                            display: 'vendor',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {data: 'nomor_ktp'},
                    {data: 'tipe_sim'},
                    {data: 'nomor_sim'},
                    {data: 'alamat'},
                    {data: 'tempat_lahir'},
                    {data: 'tanggal_lahir'},
                    {data: 'jenis_kelamin'},
                    {data: 'pendidikan_terakhir'},
                    {data: 'golongan_darah'},
                    {data: 'status'},
                    {data: 'agama'},
                    {data: 'kewarganegaraan'},
                    {data: 'pelatihan'},
                    {data: 'nomor_kontrak'},
                    {data: 'tanggal_kontrak'},
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});