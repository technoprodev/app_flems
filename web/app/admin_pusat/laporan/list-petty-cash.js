$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('admin_pusat/laporan/datatables-petty-cash'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id_user',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<a data-toggle="tooltip" data-placement="top" title="Detail Petty Cash" href="' + fn.urlTo('admin_pusat/laporan/detail-petty-cash/' + data) + '" class="text-spring margin-right-10" modal-md="" modal-title="#' + row.name + '"><img src="https://image.flaticon.com/icons/svg/179/179614.svg" height="20" width="auto"></a>' +
                                // '<a data-toggle="tooltip" data-placement="top" title="Cetak SPK" href="' + fn.urlTo('admin_pusat/dashboard/cetak/' + data) + '" class="text-azure margin-right-10" data-confirm="Apakah Anda yakin mencetak SPK ini ?" data-method="post"><img src="https://image.flaticon.com/icons/svg/179/179949.svg" height="20" width="auto"></a>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent:'&nbsp;',
                    },
                    /*{
                        data: {
                            _: 'u.name',
                            display: 'name',
                        },
                        defaultContent:'&nbsp;',
                    },*/
                    {
                        data: {
                            _: 'u.name',
                            display: 'karyawan_name',
                        },
                        defaultContent:'&nbsp;',
                    },
                    /*{data: 'tipe_penumpang'},
                    {
                        data: {
                            _: 'm.nomor_polisi',
                            display: 'mobil',
                        },
                        defaultContent:'&nbsp;',
                    },*/
                    {data: 'unit_kerja'},
                    /*{data: 'tujuan_kota'},
                    {data: 'estimasi_jarak'},
                    {data: 'waktu_keberangkatan'},
                    {data: 'waktu_kembali'},
                    {data: 'anggaran'},*/
                    /*{
                        data: {
                            _: 'p.id',
                            display: 'biaya',
                        },
                        defaultContent:'&nbsp;',
                    },*/
                    {
                        data: {
                            _: 'ta.saldo',
                            display: 'saldo',
                        },
                        defaultContent:'&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});