$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('admin_pusat/karyawan/datatables'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '<a href="' + fn.urlTo('admin_pusat/karyawan', {id: data}) + '" class="text-azure" modal-md="" modal-title="Data Keterangan ' + row.name + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('admin_pusat/karyawan/update', {id: data}) + '" class="text-spring"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('admin_pusat/karyawan/reset-password', {id: data}) + '" class="text-orange"><i class="fa fa-lock"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('admin_pusat/karyawan/delete', {id: data}) + '" class="text-rose" data-confirm="Are you sure you want to delete this item?" data-method="post"><i class="fa fa-trash-o"></i></a>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {data: 'username'},
                    {data: 'name'},
                    {data: 'email'},
                    {data: 'phone'},
                    {
                        data: {
                            _: 'uk.unit_kerja',
                            filter: 'uk.unit_kerja',
                            sort: 'uk.unit_kerja',
                            display: 'unit_kerja',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'suk.sub_unit_kerja',
                            filter: 'suk.sub_unit_kerja',
                            sort: 'suk.sub_unit_kerja',
                            display: 'sub_unit_kerja',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ukp.unit_kerja',
                            filter: 'ukp.unit_kerja',
                            sort: 'ukp.unit_kerja',
                            display: 'pool',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {data: 'nik'},
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});