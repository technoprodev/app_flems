$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('#complete');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('karyawan/dashboard/datatables-complete'),
                    type: 'POST'
                },
                order: [[1, 'asc']],
                columns: [
                    {
                        data: 'id',
                        'searchable': false,
                        'orderable': false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<a data-toggle="tooltip" data-placement="top" title="Detail" href="' + fn.urlTo('karyawan/dashboard/' + data) + '" class="text-azure" modal-md="" modal-title="#' + row.id + '"><img src="https://image.flaticon.com/icons/svg/179/179614.svg" height="20" width="auto"></a>&nbsp;&nbsp;' +
                                '<a data-toggle="tooltip" data-placement="top" title="Detail" href="' + fn.urlTo('karyawan/dashboard/cancel/' + data) + '" class="text-rose" data-confirm="Are you sure you want to cancel this order?" data-method="post"><i class="fa fa-close"></i></a>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent:'&nbsp;',
                    },
                    {
                        data: {
                            _: 'u.name',
                            display: 'name',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<b>' + data + '</b>' +
                                '';
                        },
                    },
                    {data: 'status'},
                    {data: 'tipe'},
                    {
                        data: {
                            _: 'p.tujuan_alamat',
                            'filter': 'p.tujuan_alamat',
                            'sort': 'p.tujuan_alamat',
                            display: 'tujuan',
                        },
                        defaultContent:'&nbsp;',
                    },
                    {data: 'waktu_keberangkatan'},
                    {data: 'waktu_kembali'},
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});