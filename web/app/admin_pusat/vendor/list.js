$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('admin_pusat/vendor/datatables'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '<a href="' + fn.urlTo('admin_pusat/vendor/' + data) + '" class="text-azure" modal-md="" modal-title="Data Keterangan Vendor ' + row.id + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('admin_pusat/vendor/update/' + data) + '" class="text-spring"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('admin_pusat/vendor/delete/' + data) + '" class="text-rose" data-confirm="Are you sure you want to delete this item?" data-method="post"><i class="fa fa-trash-o"></i></a>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {data: 'vendor'},
                    {data: 'alamat'},
                    {data: 'bidang_bisnis'},
                    {data: 'nomor_pks'},
                    {data: 'waktu_mulai_pks'},
                    {data: 'waktu_selesai_pks'},
                    {data: 'nomor_telpon'},
                    {data: 'periode_bayar'},
                    {data: 'pic1_nama'},
                    {data: 'pic1_telpon'},
                    {data: 'pic2_nama'},
                    {data: 'pic2_telpon'},
                    {data: 'pic3_nama'},
                    {data: 'pic3_telpon'},
                    {data: 'created_at'},
                    {data: 'updated_at'},
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});