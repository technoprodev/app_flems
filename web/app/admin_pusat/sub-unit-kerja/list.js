$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('admin_pusat/sub-unit-kerja/datatables'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '<a href="' + fn.urlTo('admin_pusat/sub-unit-kerja/' + data) + '" class="text-azure" modal-md="" modal-title="Data Keterangan Sub-unit-kerja ' + row.id + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('admin_pusat/sub-unit-kerja/update/' + data) + '" class="text-spring"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('admin_pusat/sub-unit-kerja/delete/' + data) + '" class="text-rose" data-confirm="Are you sure you want to delete this item?" data-method="post"><i class="fa fa-trash-o"></i></a>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'uk.unit_kerja',
                            filter: 'uk.unit_kerja',
                            sort: 'uk.unit_kerja',
                            display: 'unit_kerja',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {data: 'sub_unit_kerja'},
                    {data: 'created_at'},
                    {data: 'updated_at'},
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});