<?php
namespace app_flems\controllers\admin_pool;

use Yii;
use app_flems\models\Pesanan;
use app_flems\models\TransaksiAdminPengajuan;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use Dompdf\Dompdf;

class PettyCashController extends Controller
{
    public static $permissions = [
        ['admin-pool', 'Hak akses admin pool']
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [[
                    'index',
                ], 'admin-pool'],
            ]),
        ];
    }

    protected function findModel($id)
    {
        if (($model = TransaksiAdminPengajuan::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatablesTransaksiAdminPengajuan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'tap.status_pengajuan',
                'FORMAT(tap.total_yang_diajukan, 2) AS total_yang_diajukan',
                'DATE_FORMAT(tap.waktu_pengajuan, "%d %M %Y, %H:%i") AS waktu_pengajuan',
                'us.name AS supervisor_yang_menyetujui',
                'ut.name AS supervisor_yang_menolak',
            ])
            ->from('transaksi_admin_pengajuan tap')
            ->join('LEFT JOIN', 'user us', 'us.id = tap.supervisor_yang_menyetujui')
            ->join('LEFT JOIN', 'user ut', 'ut.id = tap.supervisor_yang_menolak')
            ->orderBy(['tap.id' => SORT_DESC])
            ->where(['tap.id_pool' => Yii::$app->user->identity->karyawan->id_pool])
            ->andWhere(['tap.admin_yang_mengajukan' => Yii::$app->user->identity->id])
            ->andWhere(['tap.tipe' => 'Kas'])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionIndex()
    {
        $error = true;

        $model['transaksi_admin_pengajuan'] = isset($id) ? $this->findModel($id) : new TransaksiAdminPengajuan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaksi_admin_pengajuan']->load($post);

            $transaction['transaksi_admin_pengajuan'] = TransaksiAdminPengajuan::getDb()->beginTransaction();

            try {
                $model['transaksi_admin_pengajuan']->status_pengajuan = 'Diminta';
                $model['transaksi_admin_pengajuan']->id_pool = Yii::$app->user->identity->karyawan->id_pool;
                $model['transaksi_admin_pengajuan']->admin_yang_mengajukan = Yii::$app->user->identity->id;
                $model['transaksi_admin_pengajuan']->tipe = 'Kas';
                if (!$model['transaksi_admin_pengajuan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }

                $error = false;

                $transaction['transaksi_admin_pengajuan']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['transaksi_admin_pengajuan']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('index', [
                'model' => $model,
                'title' => 'Petty Cash Admin Pool',
            ]);
        else
            return $this->redirect(['index']);
    }
}