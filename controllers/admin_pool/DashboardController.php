<?php
namespace app_flems\controllers\admin_pool;

use Yii;
use app_flems\models\Pesanan;
use app_flems\models\TransaksiAdmin;
use app_flems\models\TransaksiSupir;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use Dompdf\Dompdf;

class DashboardController extends Controller
{
    public static $permissions = [
        ['admin-pool', 'Hak akses admin pool']
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [[
                    'index',
                    'datatables-cetak-spk',
                    'cetak',
                    'detail-cetak-spk',
                    'datatables-dalam-proses',
                    'form-tambah-anggaran',
                    'form-realisasi',
                    'form-realisasi_disetujui',
                    'form-realisasi_ditolak',
                    'detail-dalam-proses',
                    'datatables-selesai',
                    'detail-selesai',
                ], 'admin-pool'],
            ]),
        ];
    }

    protected function findModel($id)
    {
        if (($model = Pesanan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPesananRealisasi($id)
    {
        if (($model = Pesanan::find()->where(['id' => $id, 'id_pool' => Yii::$app->user->identity->karyawan->id_pool])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPesananRealisasiDisetujui($id)
    {
        if (($model = Pesanan::find()->where(['id' => $id, 'id_pool' => Yii::$app->user->identity->karyawan->id_pool])->andWhere('status_realisasi = "Disetujui Supervisor"')->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPesananRealisasiDitolak($id)
    {
        if (($model = Pesanan::find()->where(['id' => $id, 'id_pool' => Yii::$app->user->identity->karyawan->id_pool])->andWhere('status_realisasi = "Ditolak Supervisor"')->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelTransaksiAdminNew($idAdmin, $tipe)
    {
        $lastModel = TransaksiAdmin::find()->where(['id_admin' => $idAdmin, 'tipe' => $tipe])->orderBy(['id' => SORT_DESC])->limit(1)->one();
        
        $model = new TransaksiAdmin();
        $model->id_admin = $idAdmin;
        $model->tipe = $tipe;
        // $model->waktu = new \yii\db\Expression('NOW()');
        
        if ($lastModel) {
            $model->saldo = $lastModel->saldo;
        } else {
            $model->saldo = 0;
        }
        return $model;
    }

    protected function findModelTransaksiSupirNew($idSupir)
    {
        $lastModel = TransaksiSupir::find()->where(['id_supir' => $idSupir])->orderBy(['id' => SORT_DESC])->limit(1)->one();
        
        $model = new TransaksiSupir();
        $model->id_supir = $idSupir;
        // $model->waktu = new \yii\db\Expression('NOW()');
        
        if ($lastModel) {
            $model->saldo = $lastModel->saldo;
        } else {
            $model->saldo = 0;
        }
        return $model;
    }

    public function actionDatatablesTransaksiAdmin()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'ta.id',
                'DATE_FORMAT(ta.waktu, "%d %M %Y, %H:%i") AS waktu',
                'ta.keterangan',
                'FORMAT(ta.debit, 2) AS debit',
                'FORMAT(ta.kredit, 2) AS kredit',
                'FORMAT(ta.saldo, 2) AS saldo',
                'ta.id_pesanan',
            ])
            ->from('transaksi_admin ta')
            ->orderBy(['ta.id' => SORT_DESC])
            ->where(['ta.id_admin' => Yii::$app->user->identity->id])
            ->andWhere(['ta.tipe' => 'Kas'])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDetailTransaksiAdmin($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        return $this->render('detail-cetak_spk', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionDatatablesRevisiAnggaran()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status_realisasi',
                'p.status',
                'u.name',
                'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->join('LEFT JOIN', 'sub_unit_kerja suk', 'p.id_sub_unit_kerja = suk.id')
            ->where('p.status_realisasi IS NOT NULL')
            ->andWhere(['p.spk_dicetak_admin' => Yii::$app->user->identity->id])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDetailRevisiAnggaran($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        return $this->render('detail-revisi_anggaran', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionDatatablesCetakSpk()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where('p.status = "Disetujui Supervisor"')
            ->andWhere(['p.id_pool' => Yii::$app->user->identity->karyawan->id_pool])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionCetak($id)
    {
        $error = true;

        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        $model['transaksi_admin'] = $this->findModelTransaksiAdminNew(Yii::$app->user->identity->id, 'Kas');
        $model['transaksi_admin']->kredit = $model['pesanan']->anggaran;
        $model['transaksi_admin']->saldo -= $model['transaksi_admin']->kredit;
        $model['transaksi_admin']->keterangan = 'Cetak SPK';
        $model['transaksi_admin']->id_pesanan = $model['pesanan']->id;

        $model['transaksi_supir'] = $this->findModelTransaksiSupirNew($model['pesanan']->id_supir);
        $model['transaksi_supir']->id_admin = Yii::$app->user->identity->id;
        $model['transaksi_supir']->debit = $model['pesanan']->anggaran;
        $model['transaksi_supir']->saldo += $model['transaksi_supir']->debit;
        $model['transaksi_supir']->keterangan = 'Cetak SPK';
        $model['transaksi_supir']->id_pesanan = $model['pesanan']->id;

        $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

        try {
            if ($model['pesanan']->status != 'Disetujui Supervisor') {
                Yii::$app->session->setFlash('error', 'SPK Pesanan ini tidak dapat dicetak.');
                return $this->redirect(['index']);
            } else if ($model['pesanan']->supir_konfirmasi != 1) {
                Yii::$app->session->setFlash('error', 'SPK pesanan ini tidak bisa dicetak karena supir belum konfirmasi.');
                return $this->redirect(['index']);
            } else if ($model['transaksi_admin']->saldo < 0) {
                Yii::$app->session->setFlash('error', 'Saldo tidak mencukupi.');
                return $this->redirect(['index']);
            } else {
                $model['pesanan']->status = 'SPK Telah Siap';
                $model['pesanan']->spk_dicetak_admin = Yii::$app->user->identity->id;
                if (!$model['pesanan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                if (!$model['transaksi_admin']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                if (!$model['transaksi_supir']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
            }

            $error = false;

            $transaction['pesanan']->commit();
            Yii::$app->session->setFlash('info', 'Data berhasil disubmit.');
        } catch (\Throwable $e) {
            $error = true;
            $transaction['pesanan']->rollBack();
            if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
        }

        if ($error)
            throw new \yii\web\HttpException(400, 'We are sorry, but we cannot proceed your action. Please try again.');
        else {
            foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;

            Yii::$app->view->params['title'] = '#' . $model['pesanan']->id;
            $this->layout = 'download';
            
            // return $this->render('download-spk', ['title' => '#' . $model['pesanan']->id,'model' => $model,]);
            $dompdf = new \Dompdf\Dompdf();
            $dompdf->loadHtml($this->render('download-spk', ['title' => '#' . $model['pesanan']->id,'model' => $model,]));
            $dompdf->setPaper('A4', 'landscape');
            $dompdf->render();
            $dompdf->stream();
            exit;
            return $this->redirect(['index'/*, 'id' => $id*/]);
        }
    }
    
    public function actionPrint($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();
        foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
            $model['pesanan_penumpang'][] = $pesananPenumpang;

        foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
            $model['pesanan_tujuan'][] = $pesananTujuan;

        $this->layout = 'download';
        // return $this->render('print-cetak', ['title' => '#' . $model['pesanan']->id,'model' => $model,]);
        
        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($this->render('print-cetak', ['title' => '#' . $model['pesanan']->id,'model' => $model,]));
        $dompdf->setPaper('A5', 'portrait');
        $dompdf->render();
        $dompdf->stream('SPK #' . $model['pesanan']->id . '.pdf');
        exit;
    }

    public function actionDetailCetakSpk($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        return $this->render('detail-cetak_spk', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionDatatablesDalamProses()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where(['p.status' => ['SPK Telah Siap','Checkin','Checkout']])
            ->andWhere(['p.spk_dicetak_admin' => Yii::$app->user->identity->id])
            ->andWhere(['p.status_realisasi' => null])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionFormRealisasi($id)
    {
        $error = true;

        $model['pesanan'] = isset($id) ? $this->findModelPesananRealisasi($id) : new Pesanan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pesanan']->load($post);

            $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

            try {
                $sisa_realisasi = $model['pesanan']->anggaran - ($model['pesanan']->revisi_bbm + $model['pesanan']->revisi_tol + $model['pesanan']->revisi_parkir);

                $model['transaksi_admin'] = $this->findModelTransaksiAdminNew(Yii::$app->user->identity->id, 'Kas');
                $model['transaksi_supir'] = $this->findModelTransaksiSupirNew($model['pesanan']->id_supir);

                // jika sisa realisasi dibawah 0 dan revisi yang diinput admin tidak lebih besar dari yang diinput supir
                if ($sisa_realisasi >= 0 && ($model['pesanan']->revisi_bbm + $model['pesanan']->revisi_tol + $model['pesanan']->revisi_parkir) <= ($model['pesanan']->bbm + $model['pesanan']->tol + $model['pesanan']->parkir)) {
                    $model['pesanan']->status = 'SPK Selesai';
                    $model['pesanan']->waktu_spk_selesai = new \yii\db\Expression("now()");
                    $model['pesanan']->spk_diselesaikan_admin = Yii::$app->user->identity->id;

                    $model['transaksi_admin']->debit = $sisa_realisasi;
                    $model['transaksi_admin']->saldo += $model['transaksi_admin']->debit;
                    $model['transaksi_admin']->keterangan = 'Sisa Realisasi SPK';

                    $model['transaksi_admin']->id_pesanan = $model['pesanan']->id;
                    if (!$model['transaksi_admin']->save()) {
                        throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                    }

                    $model['transaksi_supir']->kredit = $model['pesanan']->revisi_bbm + $model['pesanan']->revisi_tol + $model['pesanan']->revisi_parkir;
                    $model['transaksi_supir']->saldo -= $model['transaksi_supir']->kredit;
                    $model['transaksi_supir']->keterangan = 'Realisasi SPK';

                    $model['transaksi_supir']->id_admin = Yii::$app->user->identity->id;
                    $model['transaksi_supir']->id_pesanan = $model['pesanan']->id;
                    if (!$model['transaksi_supir']->save()) {
                        throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                    }

                    $model['transaksi_supir'] = $this->findModelTransaksiSupirNew($model['pesanan']->id_supir);
                    $model['transaksi_supir']->kredit = $sisa_realisasi;
                    $model['transaksi_supir']->saldo -= $model['transaksi_supir']->kredit;
                    $model['transaksi_supir']->keterangan = 'Sisa Realisasi SPK';

                    $model['transaksi_supir']->id_admin = Yii::$app->user->identity->id;
                    $model['transaksi_supir']->id_pesanan = $model['pesanan']->id;
                    if (!$model['transaksi_supir']->save()) {
                        throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                    }

                    $model['pesanan']->status_realisasi = null;
                } else {
                    // jika disetujui spv
                    if ($model['pesanan']->status_realisasi == 'Disetujui Supervisor') {
                        $model['pesanan']->status = 'SPK Selesai';
                        $model['pesanan']->waktu_spk_selesai = new \yii\db\Expression("now()");
                        $model['pesanan']->spk_diselesaikan_admin = Yii::$app->user->identity->id;

                        $model['transaksi_supir']->kredit = $model['pesanan']->revisi_bbm + $model['pesanan']->revisi_tol + $model['pesanan']->revisi_parkir;
                        $model['transaksi_supir']->saldo -= $model['transaksi_supir']->kredit;
                        $model['transaksi_supir']->keterangan = 'Realisasi SPK';

                        $model['transaksi_supir']->id_admin = Yii::$app->user->identity->id;
                        $model['transaksi_supir']->id_pesanan = $model['pesanan']->id;
                        if (!$model['transaksi_supir']->save()) {
                            throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                        }

                        $model['transaksi_supir'] = $this->findModelTransaksiSupirNew($model['pesanan']->id_supir);

                        // cek sisa anggarannya berapa
                        if ($sisa_realisasi >= 0) {
                            $model['transaksi_admin']->debit = $sisa_realisasi;
                            $model['transaksi_admin']->saldo += $model['transaksi_admin']->debit;
                            $model['transaksi_admin']->keterangan = 'Sisa Realisasi SPK';

                            $model['transaksi_supir']->kredit = $sisa_realisasi;
                            $model['transaksi_supir']->saldo -= $model['transaksi_supir']->kredit;
                            $model['transaksi_supir']->keterangan = 'Sisa Realisasi SPK';
                        } else {
                            $model['transaksi_admin']->kredit = $sisa_realisasi * -1;
                            $model['transaksi_admin']->saldo -= $model['transaksi_admin']->kredit;
                            $model['transaksi_admin']->keterangan = 'Tambahan Realisasi SPK';

                            $model['transaksi_supir']->debit = $sisa_realisasi * -1;
                            $model['transaksi_supir']->saldo += $model['transaksi_supir']->debit;
                            $model['transaksi_supir']->keterangan = 'Tambahan Realisasi SPK';
                        }
                        
                        $model['transaksi_admin']->id_pesanan = $model['pesanan']->id;
                        if (!$model['transaksi_admin']->save()) {
                            throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                        }
                        
                        $model['transaksi_supir']->id_admin = Yii::$app->user->identity->id;
                        $model['transaksi_supir']->id_pesanan = $model['pesanan']->id;
                        if (!$model['transaksi_supir']->save()) {
                            throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                        }

                        $model['pesanan']->status_realisasi = null;
                    } else {
                        $model['pesanan']->status_realisasi = 'Diminta';
                        $model['pesanan']->realisasi_diminta_admin = Yii::$app->user->identity->id;
                    }
                }
                if (!$model['pesanan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $error = false;

                $transaction['pesanan']->commit();
                Yii::$app->session->setFlash('info', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pesanan']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;

            if (!$model['pesanan']->status_realisasi) {
                $model['pesanan']->revisi_bbm = $model['pesanan']->bbm;
                $model['pesanan']->revisi_tol = $model['pesanan']->tol;
                $model['pesanan']->revisi_parkir = $model['pesanan']->parkir;
            }
        }

        if ($error)
            return $this->render('form-realisasi', [
                'model' => $model,
                'title' => 'Realisasi #' . $model['pesanan']->id,
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionDetailDalamProses($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        return $this->render('detail-dalam_proses', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionDatatablesSelesai()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where('p.status = "SPK Selesai"')
            ->andWhere(['p.spk_dicetak_admin' => Yii::$app->user->identity->id])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDetailSelesai($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        return $this->render('detail-selesai', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionIndex()
    {
        return $this->render('list-index', [
            'title' => 'Pesanan',
        ]);
    }

    // hapus

    public function aactionFormTambahAnggaran()
    {
        return $this->render('form-tambah_anggaran', [
            'title' => 'Tambah Budget',
        ]);
    }

    public function aactionFormRealisasiDisetujui($id)
    {
        $error = true;

        $model['pesanan'] = isset($id) ? $this->findModelPesananRealisasiDisetujui($id) : new Pesanan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pesanan']->load($post);

            $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

            try {
                $sisa_realisasi = $model['pesanan']->anggaran - ($model['pesanan']->revisi_bbm + $model['pesanan']->revisi_tol + $model['pesanan']->revisi_parkir);
                if ($sisa_realisasi >= 0) {
                    $model['pesanan']->status = 'SPK Selesai';
                    $model['pesanan']->waktu_spk_selesai = new \yii\db\Expression("now()");
                    $model['pesanan']->spk_diselesaikan_admin = Yii::$app->user->identity->id;
                    $model['pesanan']->status_realisasi = null;
                } else {
                    $model['pesanan']->status_realisasi = 'Diminta';
                    $model['pesanan']->realisasi_diminta_admin = Yii::$app->user->identity->id;
                }
                if (!$model['pesanan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $error = false;

                $transaction['pesanan']->commit();
                Yii::$app->session->setFlash('info', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pesanan']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;

            if (!$model['pesanan']->status_realisasi) {
                $model['pesanan']->revisi_bbm = $model['pesanan']->bbm;
                $model['pesanan']->revisi_tol = $model['pesanan']->tol;
                $model['pesanan']->revisi_parkir = $model['pesanan']->parkir;
            }
        }

        if ($error)
            return $this->render('form-realisasi', [
                'model' => $model,
                'title' => 'Realisasi #' . $model['pesanan']->id,
            ]);
        else
            return $this->redirect(['index']);
    }

    public function aactionFormRealisasiDitolak($id)
    {
        $error = true;

        $model['pesanan'] = isset($id) ? $this->findModelPesananRealisasiDitolak($id) : new Pesanan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pesanan']->load($post);

            $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

            try {
                $sisa_realisasi = $model['pesanan']->anggaran - ($model['pesanan']->revisi_bbm + $model['pesanan']->revisi_tol + $model['pesanan']->revisi_parkir);
                if ($sisa_realisasi >= 0) {
                    $model['pesanan']->status = 'SPK Selesai';
                    $model['pesanan']->waktu_spk_selesai = new \yii\db\Expression("now()");
                    $model['pesanan']->spk_diselesaikan_admin = Yii::$app->user->identity->id;
                    $model['pesanan']->status_realisasi = null;
                } else {
                    $model['pesanan']->status_realisasi = 'Diminta';
                    $model['pesanan']->realisasi_diminta_admin = Yii::$app->user->identity->id;
                }
                if (!$model['pesanan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $error = false;

                $transaction['pesanan']->commit();
                Yii::$app->session->setFlash('info', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pesanan']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;

            if (!$model['pesanan']->status_realisasi) {
                $model['pesanan']->revisi_bbm = $model['pesanan']->bbm;
                $model['pesanan']->revisi_tol = $model['pesanan']->tol;
                $model['pesanan']->revisi_parkir = $model['pesanan']->parkir;
            }
        }

        if ($error)
            return $this->render('form-realisasi', [
                'model' => $model,
                'title' => 'Realisasi #' . $model['pesanan']->id,
            ]);
        else
            return $this->redirect(['index']);
    }
}
