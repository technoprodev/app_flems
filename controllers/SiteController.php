<?php
namespace app_flems\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_flems\models\User;
use app_flems\models\Login;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['logout'], true, ['@'], ['POST']],
            ]),
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionRedirectSlash($url = '')
    {
        $newUrl = rtrim($url, '/');
        if ($newUrl) $newUrl = '/' . $newUrl;
        return $this->redirect(Yii::$app->getRequest()->getBaseUrl() . $newUrl, 301);
    }
    
    //
    
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['karyawan']))
            return $this->redirect('karyawan/dashboard');
        if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['manager']))
            return $this->redirect('manager/dashboard');
        if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['dispatcher']))
            return $this->redirect('dispatcher/dashboard');
        if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['supervisor']))
            return $this->redirect('supervisor/dashboard');
        if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['admin-pool']))
            return $this->redirect('admin_pool/dashboard');
        if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['admin-pusat']))
            return $this->redirect('admin_pusat/dashboard');
        if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['supir']))
            return $this->redirect('supir/dashboard');
        Yii::$app->user->logout();
        Yii::$app->session->setFlash('success', 'Your role is not exists in system yet, please try login using another account.');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest)
            return $this->goHome();

        $model['login'] = new Login(['scenario' => 'using-login']);
        
        if ($model['login']->load(Yii::$app->request->post()) && $model['login']->login()) {
            return $this->goBack();
        } else {
            $this->layout = 'auth';
            return $this->render('login', [
                'model' => $model,
                'title' => 'Login',
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}