<?php
namespace app_flems\controllers\admin_pusat;

use Yii;
use app_flems\models\UnitKerja;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * UnitKerjaController implements highly advanced CRUD actions for UnitKerja model.
 */
class UnitKerjaController extends Controller
{
    public static $permissions = [
        ['unit-kerja', 'Hak akses Admin Pusat'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'create', 'update', 'delete'], 'unit-kerja'],
            ]),
        ];
    }

    /**
     * Finds the UnitKerja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return UnitKerja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UnitKerja::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'unit_kerja',
                'keberangkatan_latitude',
                'keberangkatan_longitude',
                'created_at',
                'updated_at',
            ])
            ->from('unit_kerja')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), UnitKerja::getDb());
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param string $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'Daftar Unit kerja',
            ]);
        }
        
        // view single data
        $model['unit_kerja'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail Unit kerja ' . $model['unit_kerja']->id,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionCreate()
    {
        $error = true;

        $model['unit_kerja'] = isset($id) ? $this->findModel($id) : new UnitKerja();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['unit_kerja']->load($post);

            $transaction['unit_kerja'] = UnitKerja::getDb()->beginTransaction();

            try {
                if (!$model['unit_kerja']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $error = false;

                $transaction['unit_kerja']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['unit_kerja']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Unit kerja',
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $error = true;

        $model['unit_kerja'] = isset($id) ? $this->findModel($id) : new UnitKerja();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['unit_kerja']->load($post);

            $transaction['unit_kerja'] = UnitKerja::getDb()->beginTransaction();

            try {
                if (!$model['unit_kerja']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $error = false;

                $transaction['unit_kerja']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['unit_kerja']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Unit kerja ' . $model['unit_kerja']->id,
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Deletes an existing UnitKerja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
