<?php
namespace app_flems\controllers\admin_pusat;

use Yii;
use app_flems\models\Karyawan;
use app_flems\models\User;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * KaryawanController implements highly advanced CRUD actions for Karyawan model.
 */
class KaryawanController extends Controller
{
    public static $permissions = [
        ['karyawan', 'Hak akses Admin Pusat'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'create', 'update', 'delete'], 'karyawan'],
            ]),
        ];
    }

    /**
     * Finds the Karyawan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Karyawan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Karyawan::find()->where(['id_user' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelUser($id)
    {
        if (($model = User::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'u.id',
                'u.username',
                'u.name',
                'u.email',
                'u.phone',
                'uk.unit_kerja AS unit_kerja',
                'suk.sub_unit_kerja AS sub_unit_kerja',
                'ukp.unit_kerja AS pool',
                'k.nik',
            ])
            ->from('karyawan k')
            ->join('LEFT JOIN', 'user u', 'u.id = k.id_user')
            ->join('LEFT JOIN', 'sub_unit_kerja suk', 'suk.id = k.id_sub_unit_kerja')
            ->join('LEFT JOIN', 'unit_kerja uk', 'uk.id = suk.id_unit_kerja')
            ->join('LEFT JOIN', 'unit_kerja ukp', 'ukp.id = k.id_pool')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Karyawan::getDb());
    }

    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'Daftar Karyawan',
            ]);
        }
        
        // view single data
        $model['karyawan'] = $this->findModel($id);
        $model['user'] = $this->findModelUser($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail Karyawan ' . $model['user']->name,
        ]);
    }

    public function actionCreate()
    {
        $error = true;

        $model['karyawan'] = isset($id) ? $this->findModel($id) : new Karyawan();
        $model['user'] = isset($id) ? $this->findModelUser($id) : new User();

        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        unset($roles['supir']);
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name; 

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['karyawan']->load($post);
            $model['user']->load($post);

            $model['assignments'] = Yii::$app->request->post('assignments', []);
            // $model['assignments'] = ['karyawan'];

            $transaction['karyawan'] = Karyawan::getDb()->beginTransaction();

            try {
                if ($model['user']->isNewRecord) {
                    $model['user']->status = 1;
                    $model['user']->setPassword($model['user']->password);
                    $model['user']->generateAuthKey();
                }
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                $model['karyawan']->id_user = $model['user']->id;
                if (!$model['karyawan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }

                Yii::$app->authManager->revokeAll($model['user']->id);
                
                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }
                
                $error = false;

                $transaction['karyawan']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['karyawan']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'roles' => $roles,
                'title' => 'Add New Karyawan',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionUpdate($id = null)
    {
        $error = true;

        $model['karyawan'] = isset($id) ? $this->findModel($id) : new Karyawan();
        $model['user'] = isset($id) ? $this->findModelUser($id) : new User();

        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        unset($roles['supir']);
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name; 

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['karyawan']->load($post);
            $model['user']->load($post);

            $model['assignments'] = Yii::$app->request->post('assignments', []);
            // $model['assignments'] = ['karyawan'];

            $transaction['karyawan'] = Karyawan::getDb()->beginTransaction();

            try {
                if ($model['user']->isNewRecord) {
                    $model['user']->status = 1;
                    $model['user']->setPassword($model['user']->password);
                    $model['user']->generateAuthKey();
                }
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                $model['karyawan']->id_user = $model['user']->id;
                if (!$model['karyawan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }

                Yii::$app->authManager->revokeAll($model['user']->id);
                
                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }
                
                $error = false;

                $transaction['karyawan']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['karyawan']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-update', [
                'model' => $model,
                'roles' => $roles,
                'title' => 'Update Karyawan ' . $model['user']->name,
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionResetPassword($id)
    {
        $error = true;

        $model['karyawan'] = isset($id) ? $this->findModel($id) : new Karyawan();
        $model['user'] = isset($id) ? $this->findModelUser($id) : new User();
        $model['user']->scenario = 'password';

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);

            $transaction['user'] = User::getDb()->beginTransaction();

            try {
                $model['user']->setPassword($model['user']->password);
                $model['user']->generateAuthKey();
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                $transaction['user']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-reset-password', [
                'model' => $model,
                'title' => 'Reset Password Karyawan',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
