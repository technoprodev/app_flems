<?php
namespace app_flems\controllers\admin_pusat;

use Yii;
use app_flems\models\Vendor;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * VendorController implements highly advanced CRUD actions for Vendor model.
 */
class VendorController extends Controller
{
    public static $permissions = [
        ['vendor', 'Hak akses Admin Pusat'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'create', 'update', 'delete'], 'vendor'],
            ]),
        ];
    }

    /**
     * Finds the Vendor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Vendor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vendor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'vendor',
                'alamat',
                'bidang_bisnis',
                'nomor_pks',
                'waktu_mulai_pks',
                'waktu_selesai_pks',
                'nomor_telpon',
                'periode_bayar',
                'pic1_nama',
                'pic1_telpon',
                'pic2_nama',
                'pic2_telpon',
                'pic3_nama',
                'pic3_telpon',
                'created_at',
                'updated_at',
            ])
            ->from('vendor')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Vendor::getDb());
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param string $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'Daftar Vendor',
            ]);
        }
        
        // view single data
        $model['vendor'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail Vendor ' . $model['vendor']->vendor,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionCreate()
    {
        $render = false;

        $model['vendor'] = isset($id) ? $this->findModel($id) : new Vendor();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['vendor']->load($post);

            $transaction['vendor'] = Vendor::getDb()->beginTransaction();

            try {
                if (!$model['vendor']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $transaction['vendor']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['vendor']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['vendor']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Vendor',
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $render = false;

        $model['vendor'] = isset($id) ? $this->findModel($id) : new Vendor();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['vendor']->load($post);

            $transaction['vendor'] = Vendor::getDb()->beginTransaction();

            try {
                if (!$model['vendor']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $transaction['vendor']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['vendor']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['vendor']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Vendor ' . $model['vendor']->vendor,
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Vendor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
