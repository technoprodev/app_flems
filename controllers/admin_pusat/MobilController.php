<?php
namespace app_flems\controllers\admin_pusat;

use Yii;
use app_flems\models\Mobil;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * MobilController implements highly advanced CRUD actions for Mobil model.
 */
class MobilController extends Controller
{
    public static $permissions = [
        ['mobil', 'Hak akses Admin Pusat'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'create', 'update', 'delete'], 'mobil'],
            ]),
        ];
    }

    /**
     * Finds the Mobil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Mobil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mobil::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'm.id',
                'm.nomor_polisi',
                'm.dedicate',
                'v.vendor',
                'm.tipe',
                'm.merk',
                'm.warna',
                'm.tahun',
                'm.tipe_bbm',
                'm.odometer_unit_comes',
                'm.periode_sewa',
                'm.tanggal_mulai_sewa',
                'm.tanggal_akhir_sewa',
                'm.harga_per_bulan',
                'm.periode_service',
                'm.tanggal_pajak',
                'm.masa_stnk',
                'm.nama_pemilik',
                'm.created_at',
                'm.updated_at',
            ])
            ->from('mobil m')
            ->join('LEFT JOIN', 'vendor v', 'v.id = m.id_vendor')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Mobil::getDb());
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param string $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'Daftar Kendaraan',
            ]);
        }
        
        // view single data
        $model['mobil'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail Kendaraan ' . $model['mobil']->nomor_polisi,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionCreate()
    {
        $error = true;

        $model['mobil'] = isset($id) ? $this->findModel($id) : new Mobil();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['mobil']->load($post);

            $transaction['mobil'] = Mobil::getDb()->beginTransaction();

            try {
                if (!$model['mobil']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $error = false;

                $transaction['mobil']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['mobil']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Mobil',
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $error = true;

        $model['mobil'] = isset($id) ? $this->findModel($id) : new Mobil();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['mobil']->load($post);

            $transaction['mobil'] = Mobil::getDb()->beginTransaction();

            try {
                if (!$model['mobil']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $error = false;

                $transaction['mobil']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['mobil']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Mobil ' . $model['mobil']->nomor_polisi,
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Mobil model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
