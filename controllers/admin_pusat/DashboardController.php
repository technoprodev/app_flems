<?php
namespace app_flems\controllers\admin_pusat;

use Yii;
use app_flems\models\Pesanan;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class DashboardController extends Controller
{
    protected function findModel($id)
    {
        if (($model = Pesanan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionIndex()
    {
        return $this->render('index', [
            // 'model' => $model,
            'title' => 'Beranda Admin Pusat',
        ]);
    }
}
