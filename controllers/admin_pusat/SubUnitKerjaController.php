<?php
namespace app_flems\controllers\admin_pusat;

use Yii;
use app_flems\models\SubUnitKerja;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * SubUnitKerjaController implements highly advanced CRUD actions for SubUnitKerja model.
 */
class SubUnitKerjaController extends Controller
{
    public static $permissions = [
        ['sub-unit-kerja', 'Hak akses Admin Pusat'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'create', 'update', 'delete'], 'sub-unit-kerja'],
            ]),
        ];
    }

    /**
     * Finds the SubUnitKerja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return SubUnitKerja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SubUnitKerja::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'suk.id',
                'uk.unit_kerja',
                'suk.sub_unit_kerja',
                'suk.created_at',
                'suk.updated_at',
            ])
            ->from('sub_unit_kerja suk')
            ->join('LEFT JOIN', 'unit_kerja uk', 'uk.id = suk.id_unit_kerja')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), SubUnitKerja::getDb());
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param string $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'Daftar Sub unit kerja',
            ]);
        }
        
        // view single data
        $model['sub_unit_kerja'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail Sub unit kerja ' . $model['sub_unit_kerja']->id,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionCreate()
    {
        $error = true;

        $model['sub_unit_kerja'] = isset($id) ? $this->findModel($id) : new SubUnitKerja();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['sub_unit_kerja']->load($post);

            $transaction['sub_unit_kerja'] = SubUnitKerja::getDb()->beginTransaction();

            try {
                if (!$model['sub_unit_kerja']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $error = false;

                $transaction['sub_unit_kerja']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['sub_unit_kerja']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Sub unit kerja',
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $error = true;

        $model['sub_unit_kerja'] = isset($id) ? $this->findModel($id) : new SubUnitKerja();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['sub_unit_kerja']->load($post);

            $transaction['sub_unit_kerja'] = SubUnitKerja::getDb()->beginTransaction();

            try {
                if (!$model['sub_unit_kerja']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $error = false;
                
                $transaction['sub_unit_kerja']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['sub_unit_kerja']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Sub unit kerja ' . $model['sub_unit_kerja']->id,
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Deletes an existing SubUnitKerja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
