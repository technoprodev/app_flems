<?php
namespace app_flems\controllers\dispatcher;

use Yii;
use app_flems\models\Pesanan;
use app_flems\models\Supir;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class DashboardController extends Controller
{
    public static $permissions = [
        ['dispatcher', 'Hak akses dispatcher']
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [[
                    'index',
                    'datatables-butuh-persetujuan',
                    'form-butuh-persetujuan',
                    'datatables-dalam-proses',
                    'detail-dalam-proses',
                    'datatables-selesai',
                    'detail-selesai',
                ], 'dispatcher'],
            ]),
        ];
    }

    protected function findModel($id)
    {
        if (($model = Pesanan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatablesButuhPersetujuan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where('p.status = "Disetujui Manager"')
            ->andWhere(['p.id_pool' => Yii::$app->user->identity->karyawan->pool->id])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionGetMobil($id)
    {
        return $this->json(\Yii::$app->db->createCommand(
            "SELECT c.nomor_polisi
            FROM supir s
            JOIN mobil c ON c.id = s.id_mobil
            WHERE s.id_user = :id
            ", ['id' => $id]
        )->queryScalar());
    }

    public function actionDatatablesSupir()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'u.id',
                'u.name AS supir',
                'm.dedicate AS dedicate',
                'concat(m.merk, " ", m.tipe) AS mobil',
                'm.nomor_polisi AS nopol',
                new \yii\db\Expression('case when p.status = "Checkin" or p.status = "Checkout" then "Sedang Bertugas" else "Tersedia" end AS status_supir'),
                'p.tujuan_kota AS kota_terakhir',
                'p.status AS status_spk_terakhir',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
            ])
            ->from('supir s')
            ->join('LEFT JOIN', 'user u', 'u.id = s.id_user')
            ->join('LEFT JOIN', 'mobil m', 'm.id = s.id_mobil')
            ->join('LEFT JOIN', '(
                    select id_supir, max(waktu_kembali) as max_waktu_kembali
                    from pesanan
                    group by id_supir
                ) pp', 'pp.id_supir = s.id_user')
            ->join('LEFT JOIN', 'pesanan p', 'pp.max_waktu_kembali = p.waktu_kembali')
        ;
        // where status = "Checkout" or status = "SPK Selesai"
        
        return $this->datatables($query, Yii::$app->request->post(), Supir::getDb());
    }

    public function actionFormButuhPersetujuan($id)
    {
        $error = true;

        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();
        $model['pesanan']->scenario = 'dispatcher';

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pesanan']->load($post);

            $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

            try {
                if ($model['pesanan']->status == 'Disetujui Manager') {
                    $model['pesanan']->status = 'Dialokasikan Dispatcher';
                    $model['pesanan']->disetujui_dispatcher = Yii::$app->user->identity->id;
                    $supir = Supir::findOne($model['pesanan']->id_supir);
                    $mobil = $supir ? $supir->mobil : null;
                    $model['pesanan']->id_mobil = $mobil ? $mobil->id : null;
                    $model['pesanan']->validateSupir();
                    if ($model['pesanan']->errors || !$model['pesanan']->save()) {
                        throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                    }
                } else {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $error = false;

                $transaction['pesanan']->commit();
                Yii::$app->session->setFlash('info', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pesanan']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;
        }

        if ($error)
            return $this->render('form-butuh_persetujuan', [
                'model' => $model,
                'title' => '#' . $model['pesanan']->id . ' Butuh Persetujuan',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionDatatablesDalamProses()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where(['p.status' => ['Dialokasikan Dispatcher','Disetujui Supervisor','SPK Telah Siap','Checkin','Checkout']])
            ->andWhere(['p.disetujui_dispatcher' => Yii::$app->user->identity->id])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDetailDalamProses($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();
        
        return $this->render('detail-dalam_proses', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionDatatablesSelesai()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where(['p.status' => ['Ditolak Supervisor','SPK Selesai']])
            ->andWhere(['p.disetujui_dispatcher' => Yii::$app->user->identity->id])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDetailSelesai($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();
        
        return $this->render('detail-selesai', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionIndex()
    {
        return $this->render('list-index', [
            'title' => 'Pesanan',
        ]);
    }
}
