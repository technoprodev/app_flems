<?php
namespace app_flems\controllers\supir;

use Yii;
use app_flems\models\Pesanan;
use app_flems\models\PesananPenumpang;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class DashboardController extends Controller
{
    public static $permissions = [
        ['supir', 'Hak akses supir']
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [[
                    'index',
                ], 'supir'],
            ]),
        ];
    }

    protected function findModel($id)
    {
        if (($model = Pesanan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPesananNew($id)
    {
        if (($model = Pesanan::find()->where(['id' => $id, 'status' => ['Disetujui Supervisor'], 'id_supir' => Yii::$app->user->identity->id, 'supir_konfirmasi' => null])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPesananActiveCheckin($id)
    {
        if (($model = Pesanan::find()->where(['id' => $id, 'status' => ['SPK Telah Siap'], 'id_supir' => Yii::$app->user->identity->id, 'supir_konfirmasi' => 1])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPesananActiveCheckout($id)
    {
        if (($model = Pesanan::find()->where(['id' => $id, 'status' => ['Checkin'], 'id_supir' => Yii::$app->user->identity->id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPesananPenumpang($id)
    {
        if (($model = PesananPenumpang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatablesPesananBaru()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.tipe',
                'p.tipe_penumpang',
                'p.status',
                'p.tujuan',
                'p.otp_checkin',
                'p.otp_checkout',
                'p.anggaran',
                'p.spj',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'u.name AS penumpang',
                'u.phone AS hp_penumpang',
                'su.name AS supir',
                'su.phone AS hp_supir',
                'p.supir_konfirmasi',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where(['p.status' => ['Disetujui Supervisor']])
            ->andWhere(['p.id_supir' => Yii::$app->user->identity->id])
            ->andWhere(['p.supir_konfirmasi' => null])
            ->orderBy(['p.id' => SORT_DESC])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDatatablesPesananAktif()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.tipe',
                'p.tipe_penumpang',
                'p.status',
                'p.tujuan',
                'p.otp_checkin',
                'p.otp_checkout',
                'p.anggaran',
                'p.spj',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'u.name AS penumpang',
                'u.phone AS hp_penumpang',
                'su.name AS supir',
                'su.phone AS hp_supir',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where(['p.status' => ['Disetujui Supervisor', 'SPK Telah Siap', 'Checkin', 'Checkout']])
            ->andWhere(['p.id_supir' => Yii::$app->user->identity->id])
            ->andWhere(['p.supir_konfirmasi' => 1])
            ->orderBy(['p.id' => SORT_DESC])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionIndex()
    {
        $pesananBaru = Pesanan::find()->where(['status' => ['Disetujui Supervisor', 'SPK Telah Siap', 'Checkin', 'Checkout'], 'id_supir' => Yii::$app->user->identity->id, 'supir_konfirmasi' => 1])->orderBy(['id' => SORT_ASC])->all();
        $pesananAktif = Pesanan::find()->where(['status' => ['Disetujui Supervisor'], 'id_supir' => Yii::$app->user->identity->id, 'supir_konfirmasi' => null])->orderBy(['id' => SORT_ASC])->all();
        
        return $this->render('index', [
            'pesananBaru' => $pesananBaru,
            'pesananAktif' => $pesananAktif,
            'title' => 'Beranda Karyawan',
        ]);
    }

    public function actionKonfirmasi($id)
    {
        $error = false;

        $model['pesanan'] = isset($id) ? $this->findModelPesananNew($id) : new Pesanan();

        $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

        try {
            $model['pesanan']->supir_konfirmasi = 1;
            if (!$model['pesanan']->save()) {
                throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
            }
            
            $transaction['pesanan']->commit();
            Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
        } catch (\Exception $e) {
            $error = true;
            $transaction['pesanan']->rollBack();
        } catch (\Throwable $e) {
            $error = true;
            $transaction['pesanan']->rollBack();
        }

        if ($error)
            throw new \yii\web\HttpException(400, 'We are sorry, but we cannot proceed your action. Please try again.');
        else
            return $this->redirect(['index']);
    }

    public function actionFormCheckin($id)
    {
        $render = false;

        $model['pesanan'] = isset($id) ? $this->findModelPesananActiveCheckin($id) : new Pesanan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pesanan']->load($post);

            $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

            try {
                $model['pesanan']->status = 'Checkin';
                $model['pesanan']->waktu_checkin = new \yii\db\Expression("now()");
                if (!$model['pesanan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $transaction['pesanan']->commit();
                Yii::$app->session->setFlash('success', 'Submission successful.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['pesanan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['pesanan']->rollBack();
            }
        } else {
            foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;

            $render = true;
        }

        if ($render)
            return $this->render('form-checkin', [
                'model' => $model,
                'title' => 'Checkin #' . $model['pesanan']->id,
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionFormCheckout($id)
    {
        $render = false;

        $model['pesanan'] = isset($id) ? $this->findModelPesananActiveCheckout($id) : new Pesanan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pesanan']->load($post);

            $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

            try {
                $model['pesanan']->status = 'Checkout';
                $model['pesanan']->waktu_checkout = new \yii\db\Expression("now()");
                if (!$model['pesanan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $transaction['pesanan']->commit();
                Yii::$app->session->setFlash('success', 'Submission successful.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['pesanan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['pesanan']->rollBack();
            }
        } else {
            foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;

            $render = true;
        }

        if ($render)
            return $this->render('form-checkout', [
                'model' => $model,
                'title' => 'Checkout #' . $model['pesanan']->id,
            ]);
        else
            return $this->redirect(['index']);
    }
}
