<?php
namespace app_flems\controllers\supir;

use Yii;
use app_flems\models\Pesanan;
use app_flems\models\TransaksiSupir;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use Dompdf\Dompdf;

class TransaksiController extends Controller
{
    public static $permissions = [
        ['supir', 'Transaksi supir']
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [[
                    'index',
                ], 'supir'],
            ]),
        ];
    }

    protected function findModel($id)
    {
        if (($model = Pesanan::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatablesTransaksiSupir()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'ts.id',
                'DATE_FORMAT(ts.waktu, "%d %M %Y, %H:%i") AS waktu',
                'ts.keterangan',
                'FORMAT(ts.debit, 2) AS debit',
                'FORMAT(ts.kredit, 2) AS kredit',
                'FORMAT(ts.saldo, 2) AS saldo',
                'ts.id_pesanan',
            ])
            ->from('transaksi_supir ts')
            ->orderBy(['ts.id' => SORT_DESC])
            ->where(['ts.id_supir' => Yii::$app->user->identity->id])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDetailPesanan($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        return $this->render('detail-pesanan', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'title' => 'Transaksi Supir',
        ]);
    }
}