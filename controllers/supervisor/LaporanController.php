<?php
namespace app_flems\controllers\supervisor;

use Yii;
use app_flems\models\Pesanan;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class LaporanController extends Controller
{
    public static $permissions = [
        ['supervisor', 'Laporan Supervisor']
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [[
                    'datatables-permintaan',
                    'datatables-pemakaian',
                    'datatables-biaya',
                    'datatables-performa-supir',
                    'datatables-spk-supir',
                    'datatables-pengeluaran-supir',
                    'datatables-petty-cash',
                    'permintaan',
                    'pemakaian',
                    'biaya',
                    'performa-supir',
                    'spk-supir',
                    'pengeluaran-supir',
                    'petty-cash',
                ], 'supervisor'],
            ]),
        ];
    }

    protected function findModel($id)
    {
        if (($model = Pesanan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatablesPermintaan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',

                'p.tipe_penumpang',
                'CONCAT(m.nomor_polisi, " ", m.tipe) as mobil',
                'uk.unit_kerja',
                'p.tujuan_kota',
                'p.estimasi_jarak',
                'FORMAT((IFNULL(p.revisi_bbm, 0) + IFNULL(p.revisi_tol, 0) + IFNULL(p.revisi_parkir, 0) + IFNULL(p.revisi_dll, 0)), 2) AS biaya',

                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->join('LEFT JOIN', 'mobil m', 'p.id_mobil = m.id')
            ->join('LEFT JOIN', 'unit_kerja uk', 'p.id_pool = uk.id')

            ->where([
                'p.status' => ['Diminta', 'Disetujui Manager', 'Dialokasikan Dispatcher', 'Disetujui Supervisor', 'SPK Telah Siap', 'Checkin', 'Checkout', 'SPK Selesai'],
                'p.id_pool' => Yii::$app->user->identity->karyawan->id_pool,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDatatablesPemakaian()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',

                'p.tipe_penumpang',
                'CONCAT(m.nomor_polisi, " ", m.tipe) as mobil',
                'uk.unit_kerja',
                'p.tujuan_kota',
                'p.estimasi_jarak',
                'FORMAT((IFNULL(p.revisi_bbm, 0) + IFNULL(p.revisi_tol, 0) + IFNULL(p.revisi_parkir, 0) + IFNULL(p.revisi_dll, 0)), 2) AS biaya',

                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->join('LEFT JOIN', 'mobil m', 'p.id_mobil = m.id')
            ->join('LEFT JOIN', 'unit_kerja uk', 'p.id_pool = uk.id')

            ->where([
                'p.status' => ['Checkin', 'Checkout', 'SPK Selesai'],
                'p.id_pool' => Yii::$app->user->identity->karyawan->id_pool,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDatatablesBiaya()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',

                'p.tipe_penumpang',
                'CONCAT(m.nomor_polisi, " ", m.tipe) as mobil',
                'uk.unit_kerja',
                'p.tujuan_kota',
                'p.estimasi_jarak',
                'FORMAT((IFNULL(p.revisi_bbm, 0) + IFNULL(p.revisi_tol, 0) + IFNULL(p.revisi_parkir, 0) + IFNULL(p.revisi_dll, 0)), 2) AS biaya',

                'DATE_FORMAT(p.waktu_checkin, "%d %M %Y, %H:%i") AS waktu_checkin',
                'DATE_FORMAT(p.waktu_checkout, "%d %M %Y, %H:%i") AS waktu_checkout',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->join('LEFT JOIN', 'mobil m', 'p.id_mobil = m.id')
            ->join('LEFT JOIN', 'unit_kerja uk', 'p.id_pool = uk.id')

            ->where([
                'p.status' => ['SPK Selesai'],
                'p.id_pool' => Yii::$app->user->identity->karyawan->id_pool,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDatatablesPerformaSupir()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                's.id_user',
                // 'p.status',
                // 'u.name',
                'su.name AS driver_name',

                // 'p.tipe_penumpang',
                // 'CONCAT(m.nomor_polisi, " ", m.tipe) as mobil',
                'uk.unit_kerja',
                // 'p.tujuan_kota',
                // 'p.estimasi_jarak',
                // 'FORMAT((IFNULL(p.revisi_bbm, 0) + IFNULL(p.revisi_tol, 0) + IFNULL(p.revisi_parkir, 0) + IFNULL(p.revisi_dll, 0)), 2) AS biaya',

                // 'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                // 'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                // 'FORMAT(p.anggaran, 2) AS anggaran',
                // 'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',

                'pp.jumlah_spk'
            ])
            ->from('supir s')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            // ->join('LEFT JOIN', 'pesanan p', 's.id_user = p.id_supir')
            // ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            // ->join('LEFT JOIN', 'mobil m', 'p.id_mobil = m.id')
            ->join('LEFT JOIN', 'unit_kerja uk', 's.id_pool = uk.id')
            ->join('LEFT JOIN', '(
                    select id_supir, count(*) as jumlah_spk
                    from pesanan
                    where status = "Checkin" or status = "Checkout" or status = "SPK Selesai"
                    group by id_supir
                ) pp', 'pp.id_supir = s.id_user')

            ->where([
                // 'p.status' => ['Checkin', 'Checkout', 'SPK Selesai'],
                's.id_pool' => Yii::$app->user->identity->karyawan->id_pool,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDatatablesSpkSupir()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',

                'p.tipe_penumpang',
                'CONCAT(m.nomor_polisi, " ", m.tipe) as mobil',
                'uk.unit_kerja',
                'p.tujuan_kota',
                'p.estimasi_jarak',
                'FORMAT((IFNULL(p.revisi_bbm, 0) + IFNULL(p.revisi_tol, 0) + IFNULL(p.revisi_parkir, 0) + IFNULL(p.revisi_dll, 0)), 2) AS biaya',

                'DATE_FORMAT(p.waktu_checkin, "%d %M %Y, %H:%i") AS waktu_checkin',
                'DATE_FORMAT(p.waktu_checkout, "%d %M %Y, %H:%i") AS waktu_checkout',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->join('LEFT JOIN', 'mobil m', 'p.id_mobil = m.id')
            ->join('LEFT JOIN', 'unit_kerja uk', 'p.id_pool = uk.id')

            ->where([
                'p.status' => ['SPK Selesai'],
                'p.id_pool' => Yii::$app->user->identity->karyawan->id_pool,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDatatablesPengeluaranSupir()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                's.id_user',
                // 'p.status',
                // 'u.name',
                'su.name AS driver_name',

                // 'p.tipe_penumpang',
                // 'CONCAT(m.nomor_polisi, " ", m.tipe) as mobil',
                'uk.unit_kerja',
                // 'p.tujuan_kota',
                // 'p.estimasi_jarak',
                // 'FORMAT((IFNULL(p.revisi_bbm, 0) + IFNULL(p.revisi_tol, 0) + IFNULL(p.revisi_parkir, 0) + IFNULL(p.revisi_dll, 0)), 2) AS biaya',

                // 'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                // 'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                // 'FORMAT(p.anggaran, 2) AS anggaran',
                // 'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',

                'FORMAT(ts.saldo, 2) AS saldo',
            ])
            ->from('supir s')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            // ->join('LEFT JOIN', 'pesanan p', 's.id_user = p.id_supir')
            // ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            // ->join('LEFT JOIN', 'mobil m', 'p.id_mobil = m.id')
            ->join('LEFT JOIN', 'unit_kerja uk', 's.id_pool = uk.id')
            ->join('LEFT JOIN', 'transaksi_supir ts', 'ts.id_supir = s.id_user')
            ->join('JOIN', '(
                    select id_supir, max(id) as id
                    from transaksi_supir
                    group by id_supir
                ) tsp', 'tsp.id_supir = ts.id_supir AND tsp.id = ts.id')

            ->where([
                // 'p.status' => ['Checkin', 'Checkout', 'SPK Selesai'],
                's.id_pool' => Yii::$app->user->identity->karyawan->id_pool,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDatatablesPettyCash()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'k.id_user',
                // 'p.status',
                // 'u.name',
                'u.name AS karyawan_name',

                // 'p.tipe_penumpang',
                // 'CONCAT(m.nomor_polisi, " ", m.tipe) as mobil',
                'uk.unit_kerja',
                // 'p.tujuan_kota',
                // 'p.estimasi_jarak',
                // 'FORMAT((IFNULL(p.revisi_bbm, 0) + IFNULL(p.revisi_tol, 0) + IFNULL(p.revisi_parkir, 0) + IFNULL(p.revisi_dll, 0)), 2) AS biaya',

                // 'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                // 'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                // 'FORMAT(p.anggaran, 2) AS anggaran',
                // 'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',

                'FORMAT(ta.saldo, 2) AS saldo',
            ])
            ->from('karyawan k')
            ->join('LEFT JOIN', 'user u', 'k.id_user = u.id')
            // ->join('LEFT JOIN', 'pesanan p', 'k.id_user = p.id_supir')
            // ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            // ->join('LEFT JOIN', 'mobil m', 'p.id_mobil = m.id')
            ->join('LEFT JOIN', 'unit_kerja uk', 'k.id_pool = uk.id')
            ->join('JOIN', 'transaksi_admin ta', 'k.id_user = ta.id_admin')
            ->join('JOIN', '(
                    select id_admin, max(id) as id
                    from transaksi_admin
                    group by id_admin
                ) tap', 'tap.id = ta.id')

            ->where([
                // 'p.status' => ['Checkin', 'Checkout', 'SPK Selesai'],
                'k.id_pool' => Yii::$app->user->identity->karyawan->id_pool,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionPermintaan()
    {
        return $this->render('list-permintaan', [
            'title' => 'Laporan Permintaan Kendaraan',
        ]);
    }

    public function actionPemakaian()
    {
        return $this->render('list-pemakaian', [
            'title' => 'Laporan Pemakaian Kendaraan',
        ]);
    }

    public function actionBiaya()
    {
        return $this->render('list-biaya', [
            'title' => 'Laporan Biaya Pemakaian Kendaraan',
        ]);
    }

    public function actionPerformaSupir()
    {
        return $this->render('list-performa-supir', [
            'title' => 'Laporan Performa Supir',
        ]);
    }

    public function actionSpkSupir()
    {
        return $this->render('list-spk-supir', [
            'title' => 'Laporan SPK Supir',
        ]);
    }

    public function actionPengeluaranSupir()
    {
        return $this->render('list-pengeluaran-supir', [
            'title' => 'Laporan Pengeluaran Supir',
        ]);
    }

    public function actionPettyCash()
    {
        return $this->render('list-petty-cash', [
            'title' => 'Laporan Petty Cash Admin Pool',
        ]);
    }
}
