<?php
namespace app_flems\controllers\supervisor;

use Yii;
use app_flems\models\User;
use app_flems\models\Supir;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * SupirController implements highly advanced CRUD actions for Supir model.
 */
class SupirController extends Controller
{
    public static $permissions = [
        ['supir', 'Hak akses Supervisor'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'create', 'update', 'delete'], 'supir'],
            ]),
        ];
    }

    /**
     * Finds the Supir model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Supir the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Supir::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelUser($id)
    {
        if (($model = User::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'u.id',
                'u.username',
                'u.name',
                'u.email',
                'u.phone',
                's.nik',
                's.tipe',
                'CONCAT(m.merk, \' - \', m.tipe) AS mobil',
                'uk.unit_kerja AS unit_kerja',
                'suk.sub_unit_kerja AS sub_unit_kerja',
                'ukp.unit_kerja AS pool',
                'v.vendor',
                's.nomor_ktp',
                's.tipe_sim',
                's.nomor_sim',
                's.alamat',
                's.tempat_lahir',
                's.tanggal_lahir',
                's.jenis_kelamin',
                's.pendidikan_terakhir',
                's.golongan_darah',
                's.status',
                's.agama',
                's.kewarganegaraan',
                's.pelatihan',
                's.nomor_kontrak',
                's.tanggal_kontrak',
            ])
            ->from('supir s')
            ->join('LEFT JOIN', 'user u', 'u.id = s.id_user')
            ->join('LEFT JOIN', 'sub_unit_kerja suk', 'suk.id = s.id_sub_unit_kerja')
            ->join('LEFT JOIN', 'unit_kerja uk', 'uk.id = suk.id_unit_kerja')
            ->join('LEFT JOIN', 'unit_kerja ukp', 'ukp.id = s.id_pool')
            ->join('LEFT JOIN', 'mobil m', 'm.id = s.id_mobil')
            ->join('LEFT JOIN', 'vendor v', 'v.id = s.id_vendor')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Supir::getDb());
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'Daftar Supir',
            ]);
        }
        
        // view single data
        $model['supir'] = $this->findModel($id);
        $model['user'] = $this->findModelUser($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail Supir ' . $model['user']->name,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCreate()
    {
        $error = true;

        $model['supir'] = isset($id) ? $this->findModel($id) : new Supir();
        $model['user'] = isset($id) ? $this->findModelUser($id) : new User();

        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name; 

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['supir']->load($post);
            $model['user']->load($post);

            // $model['assignments'] = Yii::$app->request->post('assignments', []);
            $model['assignments'] = ['supir'];

            $transaction['supir'] = Supir::getDb()->beginTransaction();

            try {
                if ($model['user']->isNewRecord) {
                    $model['user']->status = 1;
                    $model['user']->setPassword($model['user']->password);
                    $model['user']->generateAuthKey();
                }
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                $model['supir']->id_user = $model['user']->id;
                if (!$model['supir']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }

                Yii::$app->authManager->revokeAll($model['user']->id);
                
                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }
                
                $error = false;

                $transaction['supir']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['supir']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Supir',
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $error = true;

        $model['supir'] = isset($id) ? $this->findModel($id) : new Supir();
        $model['user'] = isset($id) ? $this->findModelUser($id) : new User();

        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name; 

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['supir']->load($post);
            $model['user']->load($post);

            // $model['assignments'] = Yii::$app->request->post('assignments', []);
            $model['assignments'] = ['supir'];

            $transaction['supir'] = Supir::getDb()->beginTransaction();

            try {
                if ($model['user']->isNewRecord) {
                    $model['user']->status = 1;
                    $model['user']->setPassword($model['user']->password);
                    $model['user']->generateAuthKey();
                }
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                $model['supir']->id_user = $model['user']->id;
                if (!$model['supir']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }

                Yii::$app->authManager->revokeAll($model['user']->id);
                
                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }
                
                $error = false;

                $transaction['supir']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['supir']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-update', [
                'model' => $model,
                'title' => 'Update Supir ' . $model['user']->name,
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Supir model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
