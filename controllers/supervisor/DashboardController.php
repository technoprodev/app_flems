<?php
namespace app_flems\controllers\supervisor;

use Yii;
use app_flems\models\Pesanan;
use app_flems\models\TransaksiAdmin;
use app_flems\models\TransaksiAdminPengajuan;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class DashboardController extends Controller
{
    public static $permissions = [
        ['supervisor', 'Dashboard Supervisor']
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [[
                    'index',
                    'datatables-butuh-persetujuan',
                    'approve',
                    'reject',
                    'detail-butuh-persetujuan',
                    'datatables-dalam-proses',
                    'detail-dalam-proses',
                    'datatables-selesai',
                    'detail-selesai',
                    'datatables-revisi-anggaran',
                    'form-revisi-anggaran',
                    'detail-revisi-anggaran',
                    'datatables-transaksi-admin-pengajuan',
                    'approve-transaksi-admin-pengajuan',
                    'reject-transaksi-admin-pengajuan',
                ], 'supervisor'],
            ]),
        ];
    }

    protected function findModel($id)
    {
        if (($model = Pesanan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPesananApprove($id)
    {
        if (($model = Pesanan::find()->where(['id' => $id, 'status' => 'Dialokasikan Dispatcher', 'id_pool' => Yii::$app->user->identity->karyawan->id_pool])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPesananRevisiAnggaran($id)
    {
        if (($model = Pesanan::find()->where(['id' => $id, 'status_realisasi' => ['Diminta'], 'id_pool' => Yii::$app->user->identity->karyawan->id_pool])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelAdminTransaksiPengajuan($id)
    {
        if (($model = TransaksiAdminPengajuan::find()->where(['id' => $id, 'status_pengajuan' => 'Diminta', 'id_pool' => Yii::$app->user->identity->karyawan->id_pool])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelTransaksiAdminNew($idAdmin, $tipe)
    {
        $lastModel = TransaksiAdmin::find()->where(['id_admin' => $idAdmin, 'tipe' => $tipe])->orderBy(['id' => SORT_DESC])->limit(1)->one();
        
        $model = new TransaksiAdmin();
        $model->id_admin = $idAdmin;
        $model->tipe = $tipe;
        // $model->waktu = new \yii\db\Expression('NOW()');
        
        if ($lastModel) {
            $model->saldo = $lastModel->saldo;
        } else {
            $model->saldo = 0;
        }
        return $model;
    }

    public function actionIndex()
    {
        return $this->render('list-index', [
            'title' => 'Pesanan',
        ]);
    }

    public function actionDatatablesButuhPersetujuan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where('p.status = "Dialokasikan Dispatcher"')
            ->andWhere(['p.id_pool' => Yii::$app->user->identity->karyawan->id_pool])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionApprove($id)
    {
        $render = false;

        $model['pesanan'] = isset($id) ? $this->findModelPesananApprove($id) : new Pesanan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pesanan']->load($post);

            $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

            try {
                if ($model['pesanan']->status = 'Dialokasikan Dispatcher') {
                    $model['pesanan']->status = 'Disetujui Supervisor';
                    $model['pesanan']->otp_checkin = rand(1001, 9998);
                    $model['pesanan']->otp_checkout = rand(1001, 9998);
                    $model['pesanan']->disetujui_supervisor = Yii::$app->user->identity->id;
                    if (!$model['pesanan']->save()) {
                        throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                    }
                } else {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $transaction['pesanan']->commit();
                Yii::$app->session->setFlash('success', 'Submission successful.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['pesanan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['pesanan']->rollBack();
            }
        } else {
            foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;

            $render = true;
        }

        if ($render)
            return $this->render('form-approve', [
                'model' => $model,
                'title' => '#' . $model['pesanan']->id . ' Butuh Persetujuan',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionReject($id)
    {
        $error = false;

        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

        try {
            if ($model['pesanan']->status = 'Dialokasikan Dispatcher') {
                $model['pesanan']->status = 'Ditolak Supervisor';
                $model['pesanan']->ditolak_supervisor = Yii::$app->user->identity->id;
                if (!$model['pesanan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
            } else {
                throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
            }
            
            $transaction['pesanan']->commit();
            Yii::$app->session->setFlash('success', 'Submission successful.');
        } catch (\Exception $e) {
            $error = true;
            $transaction['pesanan']->rollBack();
        } catch (\Throwable $e) {
            $error = true;
            $transaction['pesanan']->rollBack();
        }

        if ($error)
            throw new \yii\web\HttpException(400, 'We are sorry, but we cannot proceed your action. Please try again.');
        else
            return $this->redirect(['index']);
    }

    public function actionDetailButuhPersetujuan($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();
        
        return $this->render('detail-butuh_persetujuan', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionDatatablesDalamProses()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'IF(p.status = 5, CONCAT(p.status, " (",
                    IF(p.supir_konfirmasi = 1, "Supir sudah konfirm", "Supir belum konfirm"),
                    ")"), p.status) as status',
                'u.name',
                'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where(['p.status' => ['Disetujui Supervisor','SPK Telah Siap','Checkin','Checkout']])
            ->andWhere(['p.disetujui_supervisor' => Yii::$app->user->identity->id])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDetailDalamProses($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();
        
        return $this->render('detail-dalam_proses', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionDatatablesSelesai()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where(['p.status' => ['Ditolak Supervisor','SPK Selesai']])
            ->andWhere(['or',
                ['p.disetujui_supervisor' => Yii::$app->user->identity->id],
                ['p.ditolak_supervisor' => Yii::$app->user->identity->id],
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDetailSelesai($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();
        
        return $this->render('detail-selesai', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionDatatablesRevisiAnggaran()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status_realisasi',
                'p.status',
                'u.name',
                'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where('p.status_realisasi = "Diminta"')
            ->andWhere(['p.id_pool' => Yii::$app->user->identity->karyawan->id_pool])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionFormRevisiAnggaran($id)
    {
        $render = false;

        $model['pesanan'] = isset($id) ? $this->findModelPesananRevisiAnggaran($id) : new Pesanan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pesanan']->load($post);

            $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

            try {
                if ($model['pesanan']->status_realisasi == 'Disetujui Supervisor') {
                    $model['pesanan']->realisasi_disetujui_supervisor = Yii::$app->user->identity->id;
                    // $model['pesanan']->anggaran = $model['pesanan']->revisi_bbm + $model['pesanan']->revisi_tol + $model['pesanan']->revisi_parkir ;
                } else if ($model['pesanan']->status_realisasi == 'Ditolak Supervisor') {
                    $model['pesanan']->realisasi_ditolak_supervisor = Yii::$app->user->identity->id;
                }
                if (!$model['pesanan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $transaction['pesanan']->commit();
                Yii::$app->session->setFlash('success', 'Submission successful.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['pesanan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['pesanan']->rollBack();
            }
        } else {
            foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;

            $render = true;
        }

        if ($render)
            return $this->render('form-revisi_anggaran', [
                'model' => $model,
                'title' => '#' . $model['pesanan']->id . ' Butuh Tambahan Aggaran',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionDetailRevisiAnggaran($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        return $this->render('detail-revisi_anggaran', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionDatatablesTransaksiAdminPengajuan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'tap.id',
                'u.name AS admin_yang_mengajukan',
                'FORMAT(tap.total_yang_diajukan, 2) AS total_yang_diajukan',
                'DATE_FORMAT(tap.waktu_pengajuan, "%d %M %Y, %H:%i") AS waktu_pengajuan',
            ])
            ->from('transaksi_admin_pengajuan tap')
            ->join('LEFT JOIN', 'user u', 'u.id = tap.admin_yang_mengajukan')
            ->orderBy(['tap.id' => SORT_DESC])
            ->where(['tap.id_pool' => Yii::$app->user->identity->karyawan->id_pool])
            ->andWhere(['tap.status_pengajuan' => 'Diminta'])
            ->andWhere(['tap.tipe' => 'Kas'])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionApproveTransaksiAdminPengajuan($id)
    {
        $error = true;

        $model['transaksi_admin_pengajuan'] = isset($id) ? $this->findModelAdminTransaksiPengajuan($id) : new TransaksiAdminPengajuan();

        $transaction['transaksi_admin_pengajuan'] = TransaksiAdminPengajuan::getDb()->beginTransaction();

        try {
            if ($model['transaksi_admin_pengajuan']->status_pengajuan == 'Diminta') {
                $model['transaksi_admin'] = $this->findModelTransaksiAdminNew($model['transaksi_admin_pengajuan']->admin_yang_mengajukan, 'Kas');
                $model['transaksi_admin']->debit = $model['transaksi_admin_pengajuan']->total_yang_diajukan;
                $model['transaksi_admin']->saldo += $model['transaksi_admin']->debit;
                $model['transaksi_admin']->keterangan = 'Tambahan Saldo';
                $model['transaksi_admin']->disetujui_supervisor = Yii::$app->user->identity->id;
                if (!$model['transaksi_admin']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }

                $model['transaksi_admin_pengajuan']->status_pengajuan = 'Disetujui';
                $model['transaksi_admin_pengajuan']->supervisor_yang_menolak = Yii::$app->user->identity->id;
                if (!$model['transaksi_admin_pengajuan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada data. Harap hubungi admin pusat agar segera diperbaiki.');
                }
            } else {
                throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
            }
                
            $error = false;

            $transaction['transaksi_admin_pengajuan']->commit();
            Yii::$app->session->setFlash('info', 'Pengajuan berhasil disetujui.');
        } catch (\Throwable $e) {
            $error = true;
            $transaction['transaksi_admin_pengajuan']->rollBack();
            if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
        }

        return $this->redirect(['index']);
    }

    public function actionRejectTransaksiAdminPengajuan($id)
    {
        $error = true;

        $model['transaksi_admin_pengajuan'] = isset($id) ? $this->findModelAdminTransaksiPengajuan($id) : new TransaksiAdminPengajuan();

        $transaction['transaksi_admin_pengajuan'] = TransaksiAdminPengajuan::getDb()->beginTransaction();

        try {
            if ($model['transaksi_admin_pengajuan']->status_pengajuan == 'Diminta') {
                $model['transaksi_admin_pengajuan']->status_pengajuan = 'Ditolak';
                $model['transaksi_admin_pengajuan']->supervisor_yang_menolak = Yii::$app->user->identity->id;
                if (!$model['transaksi_admin_pengajuan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada data. Harap hubungi admin pusat agar segera diperbaiki.');
                }
            } else {
                throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
            }
                
            $error = false;

            $transaction['transaksi_admin_pengajuan']->commit();
            Yii::$app->session->setFlash('info', 'Pengajuan berhasil ditolak.');
        } catch (\Throwable $e) {
            $error = true;
            $transaction['transaksi_admin_pengajuan']->rollBack();
            if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
        }

        return $this->redirect(['index']);
    }
}
