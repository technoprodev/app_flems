<?php
namespace app_flems\controllers\manager;

use Yii;
use app_flems\models\Pesanan;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class DashboardController extends Controller
{
    public static $permissions = [
        ['manager', 'Hak akses manager']
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [[
                    'index',
                    'datatables-butuh-persetujuan',
                    'approve',
                    'reject',
                    'detail-butuh-persetujuan',
                    'datatables-dalam-proses',
                    'detail-dalam-proses',
                    'datatables-selesai',
                    'detail-selesai',
                ], 'manager'],
            ]),
        ];
    }

    protected function findModel($id)
    {
        if (($model = Pesanan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionIndex()
    {
        return $this->render('list-index', [
            'title' => 'Pesanan',
        ]);
    }

    public function actionDatatablesButuhPersetujuan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                // 'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                // 'FORMAT(p.anggaran, 2) AS anggaran',
                // 'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            // ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            // ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where('p.status = "Diminta"')
            ->andWhere(['p.disetujui_manager' => Yii::$app->user->identity->id])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionApprove($id)
    {
        $error = false;

        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

        try {
            if ($model['pesanan']->status == 'Diminta') {
                $model['pesanan']->status = 'Disetujui Manager';
                $model['pesanan']->disetujui_manager = Yii::$app->user->identity->id;
                if (!$model['pesanan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada submit data. Harap perbaiki sebelum submit ulang.');
                }
            } else {
                throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada submit data. Harap perbaiki sebelum submit ulang.');
            }
            
            $transaction['pesanan']->commit();
            Yii::$app->session->setFlash('success', 'Submission successful.');
        } catch (\Throwable $e) {
            $error = true;
            $transaction['pesanan']->rollBack();
            if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
        }

        if ($error)
            throw new \yii\web\HttpException(400, 'We are sorry, but we cannot proceed your action. Please try again.');
        else
            return $this->redirect(['index']);
    }

    public function actionReject($id)
    {
        $error = false;

        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

        try {
            if ($model['pesanan']->status == 'Diminta') {
                $model['pesanan']->status = 'Ditolak Manager';
                $model['pesanan']->ditolak_manager = Yii::$app->user->identity->id;
                if (!$model['pesanan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
            } else {
                throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
            }
            
            $transaction['pesanan']->commit();
            Yii::$app->session->setFlash('success', 'Submission successful.');
        } catch (\Exception $e) {
            $error = true;
            $transaction['pesanan']->rollBack();
        } catch (\Throwable $e) {
            $error = true;
            $transaction['pesanan']->rollBack();
        }

        if ($error)
            throw new \yii\web\HttpException(400, 'We are sorry, but we cannot proceed your action. Please try again.');
        else
            return $this->redirect(['index']);
    }

    public function actionDetailButuhPersetujuan($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();
        
        return $this->render('detail-butuh_persetujuan', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionDatatablesDalamProses()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where(['p.status' => ['Disetujui Manager','Dialokasikan Dispatcher','Disetujui Supervisor','SPK Telah Siap','Checkin','Checkout']])
            ->andWhere(['p.disetujui_manager' => Yii::$app->user->identity->id])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDetailDalamProses($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();
        
        return $this->render('detail-dalam_proses', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionDatatablesSelesai()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.status',
                'u.name',
                'su.name AS driver_name',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'FORMAT(p.anggaran, 2) AS anggaran',
                'IF(p.spj IS NULL, "", IF(p.spj = 1, "Luar Kota", "Dalam Kota")) as spj',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where(['p.status' => ['Ditolak Manager','Ditolak Supervisor','SPK Selesai']])
            ->andWhere(['p.disetujui_manager' => Yii::$app->user->identity->id])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDetailSelesai($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();
        
        return $this->render('detail-selesai', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }
}
