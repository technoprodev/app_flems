<?php
namespace app_flems\controllers\karyawan;

use Yii;
use app_flems\models\Pesanan;
use app_flems\models\PesananPenumpang;
use app_flems\models\PesananTujuan;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class DashboardController extends Controller
{
    public static $permissions = [
        ['karyawan', 'Hak akses karyawan']
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [[
                    'index',
                ], 'karyawan'],
            ]),
        ];
    }

    protected function findModel($id)
    {
        if (($model = Pesanan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPesananPenumpang($id)
    {
        if (($model = PesananPenumpang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPesananTujuan($id)
    {
        if (($model = PesananTujuan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatablesDalamProses()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.tipe',
                'p.tipe_penumpang',
                'p.status',
                'p.tujuan',
                'p.otp_checkin',
                'p.otp_checkout',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'u.name',
                'su.name AS supir',
                'su.phone AS hp_supir',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where(['p.status' => ['Diminta','Disetujui Manager','Dialokasikan Dispatcher','Disetujui Supervisor','SPK Telah Siap','Checkin','Checkout']])
            ->andWhere(['p.id_penumpang' => Yii::$app->user->identity->id])
            ->orderBy(['p.id' => SORT_DESC])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionDatatablesSelesai()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.tipe',
                'p.tipe_penumpang',
                'p.status',
                'p.tujuan',
                'p.otp_checkin',
                'p.otp_checkout',
                'DATE_FORMAT(p.waktu_keberangkatan, "%d %M %Y, %H:%i") AS waktu_keberangkatan',
                'DATE_FORMAT(p.waktu_kembali, "%d %M %Y, %H:%i") AS waktu_kembali',
                'u.name',
                'su.name AS supir',
                'su.phone AS hp_supir',
            ])
            ->from('pesanan p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id_penumpang')
            ->join('LEFT JOIN', 'supir s', 's.id_user = p.id_supir')
            ->join('LEFT JOIN', 'user su', 's.id_user = su.id')
            ->where(['p.status' => ['Ditolak Manager','Ditolak Supervisor','SPK Selesai','Dibatalkan Penumpang','Dibatalkan Otomatis']])
            ->andWhere(['p.id_penumpang' => Yii::$app->user->identity->id])
            ->orderBy(['p.id' => SORT_DESC])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pesanan::getDb());
    }

    public function actionIndex()
    {
        $error = true;

        $model['pesanan'] = new Pesanan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pesanan']->load($post);
            if (isset($post['PesananTujuan'])) {
                foreach ($post['PesananTujuan'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $pesananTujuan = $this->findModelPesananTujuan($value['id']);
                        $pesananTujuan->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $pesananTujuan = $this->findModelPesananTujuan(($value['id']*-1));
                        $pesananTujuan->isDeleted = true;
                    } else {
                        $pesananTujuan = new PesananTujuan();
                        $pesananTujuan->setAttributes($value);
                    }
                    $model['pesanan_tujuan'][] = $pesananTujuan;
                }
            }
            if (isset($post['PesananPenumpang'])) {
                foreach ($post['PesananPenumpang'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $pesananPenumpang = $this->findModelPesananPenumpang($value['id']);
                        $pesananPenumpang->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $pesananPenumpang = $this->findModelPesananPenumpang(($value['id']*-1));
                        $pesananPenumpang->isDeleted = true;
                    } else {
                        $pesananPenumpang = new PesananPenumpang();
                        $pesananPenumpang->setAttributes($value);
                    }
                    $model['pesanan_penumpang'][] = $pesananPenumpang;
                }
            }

            $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

            try {
                if ($model['pesanan']->isNewRecord) {
                    $model['pesanan']->createPesanan();
                }
                if (!$model['pesanan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }

                $error = false;

                if (isset($model['pesanan_tujuan']) and is_array($model['pesanan_tujuan'])) {
                    foreach ($model['pesanan_tujuan'] as $key => $pesananTujuan) {
                        $pesananTujuan->id_pesanan = $model['pesanan']->id;
                        if (!$pesananTujuan->isDeleted && !$pesananTujuan->validate()) $error = true;                        
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                    }
                
                    foreach ($model['pesanan_tujuan'] as $key => $pesananTujuan) {
                        if ($pesananTujuan->isDeleted) {
                            if (!$pesananTujuan->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$pesananTujuan->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['pesanan_penumpang']) and is_array($model['pesanan_penumpang'])) {
                    foreach ($model['pesanan_penumpang'] as $key => $pesananPenumpang) {
                        $pesananPenumpang->id_pesanan = $model['pesanan']->id;
                        if (!$pesananPenumpang->isDeleted && !$pesananPenumpang->validate()) $error = true;                        
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                    }
                
                    foreach ($model['pesanan_penumpang'] as $key => $pesananPenumpang) {
                        if ($pesananPenumpang->isDeleted) {
                            if (!$pesananPenumpang->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$pesananPenumpang->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $transaction['pesanan']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disubmit.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pesanan']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;
        }

        if ($error)
            return $this->render('form-index', [
                'model' => $model,
                'title' => 'Beranda Karyawan',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionDetail($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();
        
        return $this->render('detail', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }

    public function actionBatalkanPesanan($id)
    {
        $error = false;

        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

        try {
            if ($model['pesanan']->status == 'Diminta' || $model['pesanan']->status == 'Disetujui Manager' || $model['pesanan']->status == 'Dialokasikan Dispatcher' || $model['pesanan']->status == 'Disetujui Supervisor') {
                $model['pesanan']->status = 'Dibatalkan Penumpang';
                $model['pesanan']->waktu_dibatalkan_penumpang = new \yii\db\Expression("now()");
                if (!$model['pesanan']->save()) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada submit data. Harap perbaiki sebelum submit ulang.');
                }
            } else {
                throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada submit data. Harap perbaiki sebelum submit ulang.');
            }
            
            $transaction['pesanan']->commit();
            Yii::$app->session->setFlash('success', 'Submission successful.');
        } catch (\Throwable $e) {
            $error = true;
            $transaction['pesanan']->rollBack();
            if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
        }

        if ($error)
            throw new \yii\web\HttpException(400, 'We are sorry, but we cannot proceed your action. Please try again.');
        else
            return $this->redirect(['index']);
    }
}
