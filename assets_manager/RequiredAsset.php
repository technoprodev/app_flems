<?php
namespace app_flems\assets_manager;

use yii\web\AssetBundle;

class RequiredAsset extends AssetBundle
{
	public $sourcePath = '@app_flems/assets';

    public $css = [];

    public $js = [];

    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}